#![feature(portable_simd)]

use criterion::{black_box, Criterion, criterion_group, criterion_main};

use positronium_maths::{Matrix, Quaternion, simd::SimdQuaternion, Matrix2f, Matrix3f, Matrix4f, Vector4, Vector3, simd::SimdVector4, simd::SimdMatrix};

fn matrix_2x2_inverse(c: &mut Criterion) {
    c.bench_function("mat2 inverse", |b| b.iter(|| Matrix2f::from_iter_column_wise((1..).map(|i| black_box(i * i) as f32)).invert_unchecked()));
    c.bench_function("mat2 inverse checked", |b| b.iter(|| Matrix2f::from_iter_column_wise((1..).map(|i| black_box(i * i) as f32)).invert().unwrap()));
}

fn matrix_3x3_inverse(c: &mut Criterion) {
    c.bench_function("mat3 inverse", |b| b.iter(|| Matrix3f::from_iter_column_wise((1..).map(|i| black_box(i * i) as f32)).invert_unchecked()));
    c.bench_function("mat3 inverse checked", |b| b.iter(|| Matrix3f::from_iter_column_wise((1..).map(|i| black_box(i * i) as f32)).invert().unwrap()));
}

fn matrix_4x4_inverse(c: &mut Criterion) {
    c.bench_function("mat4 inverse", |b| b.iter(|| Matrix4f::from_iter_column_wise((1..).map(|i| black_box(i * i * i) as f32)).invert_unchecked()));
    c.bench_function("mat4 inverse checked", |b| b.iter(|| Matrix4f::from_iter_column_wise((1..).map(|i| black_box(i * i * i) as f32)).invert().unwrap()));
}

fn matrix_5x5_inverse(c: &mut Criterion) {
    c.bench_function("mat5 inverse", |b| b.iter(|| Matrix::<f32, 5, 5>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i) as f32)).invert_unchecked()));
    c.bench_function("mat5 inverse checked", |b| b.iter(|| Matrix::<f32, 5, 5>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i) as f32)).invert().unwrap()));
}

fn matrix_8x8_inverse(c: &mut Criterion) {
    c.bench_function("mat8 inverse", |b| b.iter(|| Matrix::<f32, 8, 8>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i * i * i) as f32)).invert_unchecked()));
    c.bench_function("mat8 inverse checked", |b| b.iter(|| Matrix::<f32, 8, 8>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i * i * i) as f32)).invert().unwrap()));
}

fn matrix_10x10_inverse(c: &mut Criterion) {
    c.bench_function("mat10 inverse", |b| b.iter(|| Matrix::<f32, 10, 10>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i * i * i) as f32)).invert_unchecked()));
    c.bench_function("mat10 inverse checked", |b| b.iter(|| Matrix::<f32, 10, 10>::from_iter_column_wise((1..).map(|i| black_box(i * i * i * i * i * i) as f32)).invert().unwrap()));
}

// criterion_group!(benches, matrix_2x2_inverse, matrix_3x3_inverse, matrix_4x4_inverse, matrix_5x5_inverse, matrix_8x8_inverse, matrix_10x10_inverse);
// criterion_main!(benches, compare);
// criterion_main!(compare);


fn component_arithmetic(c: &mut Criterion) {
    {
        let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("sf: 1000 adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("sf: 1000 subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("sf: 1000 muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec * vec);
                }
            })
        });

        c.bench_function("sf: 1000 divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec / vec);
                }
            })
        });
    }
    {
        let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm: 1000 adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("psm: 1000 subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("psm: 1000 muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec & vec);
                }
            })
        });

        c.bench_function("psm: 1000 divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec | vec);
                }
            })
        });
    }
    {
        let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm simd: 1000 adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec & vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec | vec);
                }
            })
        });
    }
}

fn misc(c: &mut Criterion) {
    {
        let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
        let vec1 = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("sf: 1000 abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.abs());
                }
            })
        });

        c.bench_function("sf: 1000 hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.hsum());
                }
            })
        });

        c.bench_function("sf: 1000 mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(spaceform::base::Vector::min(vec1, vec2));
                }
            })
        });

        c.bench_function("sf: 1000 maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(spaceform::base::Vector::max(vec1, vec2));
                }
            })
        });
    }
    {
        let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
        let vec1 = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm: 1000 abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.comp_abs());
                }
            })
        });

        c.bench_function("psm: 1000 hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.comp_sum());
                }
            })
        });

        c.bench_function("psm: 1000 mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.comp_min(vec2));
                }
            })
        });

        c.bench_function("psm: 1000 maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.comp_max(vec2));
                }
            })
        });
    }
    {
        let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
        let vec1 = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm simd: 1000 abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.comp_abs());
                }
            })
        });

        c.bench_function("psm simd: 1000 hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec.comp_sum());
                }
            })
        });

        c.bench_function("psm simd: 1000 mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.comp_min(vec2));
                }
            })
        });

        c.bench_function("psm simd: 1000 maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.comp_max(vec2));
                }
            })
        });
    }
}

fn products(c: &mut Criterion) {
    {
        let vec1 = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("sf: 1000 dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(spaceform::base::Vector::dot(vec1, vec2));
                }
            })
        });

        c.bench_function("sf: 1000 crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(spaceform::base::Vector::cross(vec1, vec2));
                }
            })
        });
    }
    {
        let vec1 = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm: 1000 dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1 * vec2);
                }
            })
        });

        c.bench_function("psm: 1000 crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.space() ^ vec2.space());
                }
            })
        });
    }
    {
        let vec1 = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
        let vec2 = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));

        c.bench_function("psm simd: 1000 dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1 * vec2);
                }
            })
        });

        c.bench_function("psm simd: 1000 crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec1.cross_space(vec2));
                }
            })
        });
    }
}

fn mul(c: &mut Criterion) {
    {
        let vec = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
        let mat = black_box(spaceform::base::Matrix::rows([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("sf: 1000 matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("sf: 1000 vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(vec * mat);
                }
            })
        });
    }
    {
        let vec = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
        let mat = black_box(Matrix::columns([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("psm: 1000 matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("psm: 1000 vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat * vec);
                }
            })
        });
    }
    {
        let vec = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
        let mat = black_box(SimdMatrix::columns([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("psm simd: 1000 matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("psm simd: 1000 vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat * vec);
                }
            })
        });
    }
}

fn others(c: &mut Criterion) {
    {
        let mat = black_box(spaceform::base::Matrix::rows([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("sf: 1000 transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("sf: 1000 inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.inverse());
                }
            })
        });

        c.bench_function("sf: 1000 dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.det());
                }
            })
        });
    }
    {
        let mat = black_box(Matrix::columns([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("psm: 1000 transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("psm: 1000 inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.invert_unchecked());
                }
            })
        });

        c.bench_function("psm: 1000 dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.determinant());
                }
            })
        });

        let big_mat = black_box(Matrix::<f32, 8, 8>::splat(3.14));

        c.bench_function("psm: 1000 big dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(big_mat.determinant());
                }
            })
        });
    }
    {
        let mat = black_box(SimdMatrix::columns([
            [1f32, 2f32, 3f32, 4f32],
            [5f32, 3f32, 7f32, 8f32],
            [9f32, 10f32, -1f32, 12f32],
            [13f32, 14f32, 15f32, 4f32],
        ]));

        c.bench_function("psm simd: 1000 transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("psm simd: 1000 inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.invert_unchecked());
                }
            })
        });

        c.bench_function("psm simd: 1000 dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(mat.determinant());
                }
            })
        });

        let big_mat = black_box(SimdMatrix::<f32, 8, 8>::splat(3.14));

        c.bench_function("psm simd: 1000 big dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(big_mat.determinant());
                }
            })
        });
    }
}

fn quats(c: &mut Criterion) {
    {
        let quat = spaceform::base::Quaternion::new(1f32, 2f32, 3f32, 4f32);

        c.bench_function("sf: 1000 quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(quat * quat);
                }
            })
        });
    }
    {
        let quat = Quaternion::new(4f32, Vector3::new(1f32, 2f32, 3f32));

        c.bench_function("psm: 1000 quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(quat * quat);
                }
            })
        });
    }
    {
        let quat = SimdQuaternion::new(4f32, Vector3::new(1f32, 2f32, 3f32));

        c.bench_function("psm simd: 1000 quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    black_box(quat * quat);
                }
            })
        });
    }
}

fn component_arithmetic_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("sf: 1000 load subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("sf: 1000 load muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec * vec);
                }
            })
        });

        c.bench_function("sf: 1000 load divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec / vec);
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("psm: 1000 load subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("psm: 1000 load muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec & vec);
                }
            })
        });

        c.bench_function("psm: 1000 load divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec | vec);
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load adds", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec + vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 load subs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec - vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 load muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec & vec);
                }
            })
        });

        c.bench_function("psm simd: 1000 load divs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec | vec);
                }
            })
        });
    }
}

fn misc_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.abs());
                }
            })
        });

        c.bench_function("sf: 1000 load hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.hsum());
                }
            })
        });

        c.bench_function("sf: 1000 load mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(spaceform::base::Vector::min(vec, vec));
                }
            })
        });

        c.bench_function("sf: 1000 load maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(spaceform::base::Vector::max(vec, vec));
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_abs());
                }
            })
        });

        c.bench_function("psm: 1000 load hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_sum());
                }
            })
        });

        c.bench_function("psm: 1000 load mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_min(vec));
                }
            })
        });

        c.bench_function("psm: 1000 load maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_max(vec));
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load abs", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_abs());
                }
            })
        });

        c.bench_function("psm simd: 1000 load hsums", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_sum());
                }
            })
        });

        c.bench_function("psm simd: 1000 load mins", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_min(vec));
                }
            })
        });

        c.bench_function("psm simd: 1000 load maxes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec.comp_max(vec));
                }
            })
        });
    }
}

fn products_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(spaceform::base::Vector::dot(vec1, vec2));
                }
            })
        });

        c.bench_function("sf: 1000 load crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(spaceform::base::Vector::new(1f32, 2f32, 3f32, 4f32));
                    black_box(spaceform::base::Vector::cross(vec1, vec2));
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec1 * vec2);
                }
            })
        });

        c.bench_function("psm: 1000 load crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(Vector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec1.space() ^ vec2.space());
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load dots", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec1 * vec2);
                }
            })
        });

        c.bench_function("psm simd: 1000 load crosses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec1 = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
                    let vec2 = black_box(SimdVector4::new(1f32, 2f32, 3f32, 4f32));
                    black_box(vec1.cross_space(vec2));
                }
            })
        });
    }
}

fn mul_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(spaceform::base::Matrix::rows([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("sf: 1000 load vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(spaceform::base::Vector::new(-1f32, 2f32, -3f32, 4f32));
                    let mat = black_box(spaceform::base::Matrix::rows([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(vec * mat);
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(Matrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("psm: 1000 load vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(Vector4::new(-1f32, 2f32, -3f32, 4f32));
                    let mat = black_box(Matrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat * vec);
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(SimdMatrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat * mat);
                }
            })
        });

        c.bench_function("psm simd: 1000 load vector matrix muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let vec = black_box(SimdVector4::new(-1f32, 2f32, -3f32, 4f32));
                    let mat = black_box(SimdMatrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat * vec);
                }
            })
        });
    }
}

fn others_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(spaceform::base::Matrix::rows([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("sf: 1000 load inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(spaceform::base::Matrix::rows([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.inverse());
                }
            })
        });

        c.bench_function("sf: 1000 load dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(spaceform::base::Matrix::rows([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.det());
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(Matrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("psm: 1000 load inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(Matrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.invert_unchecked());
                }
            })
        });

        c.bench_function("psm: 1000 load dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(Matrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.determinant());
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load transposes", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(SimdMatrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.transpose());
                }
            })
        });

        c.bench_function("psm simd: 1000 load inverses", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(SimdMatrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.invert_unchecked());
                }
            })
        });

        c.bench_function("psm simd: 1000 load dets", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let mat = black_box(SimdMatrix::columns([
                        [1f32, 2f32, 3f32, 4f32],
                        [5f32, 3f32, 7f32, 8f32],
                        [9f32, 10f32, -1f32, 12f32],
                        [13f32, 14f32, 15f32, 4f32],
                    ]));
                    black_box(mat.determinant());
                }
            })
        });
    }
}

fn quats_load(c: &mut Criterion) {
    {
        c.bench_function("sf: 1000 load quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let quat = spaceform::base::Quaternion::new(1f32, 2f32, 3f32, 4f32);
                    black_box(quat * quat);
                }
            })
        });
    }
    {
        c.bench_function("psm: 1000 load quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let quat = Quaternion::new(4f32, Vector3::new(1f32, 2f32, 3f32));
                    black_box(quat * quat);
                }
            })
        });
    }
    {
        c.bench_function("psm simd: 1000 load quat muls", |b| {
            b.iter(|| {
                for _ in 0..1000 {
                    let quat = SimdQuaternion::new(4f32, Vector3::new(1f32, 2f32, 3f32));
                    black_box(quat * quat);
                }
            })
        });
    }
}

criterion_group!(
	vector,
	component_arithmetic,
	misc,
	products,
	component_arithmetic_load,
	misc_load,
	products_load
);
criterion_group!(matrix, mul, others, mul_load, others_load);
criterion_group!(quaternion, quats, quats_load);
criterion_main!(vector, matrix, quaternion);