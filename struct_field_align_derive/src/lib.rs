use std::collections::HashSet;

use proc_macro2::TokenStream;
use proc_macro_crate::{crate_name, FoundCrate};
use quote::{format_ident, quote, TokenStreamExt, ToTokens};
use syn::{AttrStyle, Field, Fields, ItemStruct, parse_macro_input, parse_quote};
use syn::punctuated::Pair;
use syn::spanned::Spanned;

///
/// The macro is intended for the purpose of aligning the fields in structs that will uploaded
/// to the GPU with alignment requirements like that in Vulkan.
///
/// En example usage:
///
/// ```
/// use positronium_maths::*;
///
/// #[field_align(Self, new = new, init = Init, offset = offset)]
/// #[derive(Default, Debug)]
/// pub(crate) struct Test {
///     #[align(vec3)]
///     pub a: Vector3f,
///     #[align(2)]
///     pub b: Vector2f,
///     #[align(vec4)]
///     pub c: Vector4f,
/// }
/// ```
///
/// Here the 4 attributes `Self`, `new = new`, `init = Init` and `offset = offset` are the 3 options.
///
/// Firstly, note that it adds #[repr(C)] to the struct, so that parameter or and offset can
/// actually be guaranteed.
///
/// The `Self` option means that the alignment of the struct will be set to the largest
/// alignment of all the fields. This is how structs normally behave, but this takes into
/// account the increase alignment from the #[align(_)] field attributes.
///
/// The `new = <constructor name>` attribute, will if specified generate a constructor
/// with the name <constructor name>. The purpose of this is so that you don't have to
/// worry about manually initialising the padding fields.
/// Also note that `new` is the default name so `new` is equivalent to `new = new`.
///
/// The `init = <init suffix>` attribute, will if specified a parallel initialise struct with name
/// <original name><init suffix> that has all the original fields of the main struct but no
/// alignment magic. The purpose of this is so that you can initialise it just like normal,
/// the with a call to .init() consume the Init struct to give you the one with proper alignment.
/// Also note that `Init` is the default name, so `init` is equivalent to `init = Init`.
/// Also note, that the publicity of this struct will match the main one.
///
/// The `offset = <offset suffix>` attribute with add const static methods to get
/// the offset of a field in the struct once all alignment has been applied,
/// this has the field `pub const fn <field_name>_<offset_suffix>() -> usize`.
/// Also note that `offset` is the default name so `offset` is equivalent to `offset = offset`
///
/// Then for the when using this macro add a #[align(_)] attribute to any field in the struct,
/// this will result in if needed padding being added before the the field so that relative to the
/// start of the struct the field will have an offset that is aligned to `n` times the fields
/// normal alignment.
/// When using like #[align(n)], e.g. #[align(4)] : n = n, e.g n = 4, so the number is
/// directly the multiplier applied to the fields normal alignment.
/// When using the `vec` or `mat` prefix, you can only specify 1 <= n <= 4, so as to correspond
/// to glsl and has the mapping of
///     n = 1 -> 1
///     n = 2 -> 2
///     n = 3 -> 4
///     n = 4 -> 4
/// which lines up with the offset rules for vec1, vec2, vec3, vec4, mat1, mat2, mat3, mat4.
///
///
///
/// The generated output for this particular example is:
///
/// ```
/// use positronium_maths::*;
///
/// #[repr(C)]
/// #[derive(Default, Debug)]
/// pub(crate) struct Test {
///     __padding__0: Align<16>,
///     pub a: Vector3f,
///     __padding__1: [u8; 4],
///     pub b: Vector2f,
///     __padding__3: [u8; 8],
///     pub c: Vector4f,
/// }
///
/// impl Test {
///     pub const fn a_offset() -> usize {
///         0
///     }
///     pub const fn b_offset() -> usize {
///         16
///     }
///     pub const fn c_offset() -> usize {
///         32
///     }
/// }
///
/// impl Test {
///     pub fn new(a: Vector3f, b: Vector2f, c: Vector3f) -> Self {
///         Test {
///             __padding__0: Default::default(),
///             a,
///             __padding__1: Default::default(),
///             b,
///             __padding__3: Default::default(),
///             c,
///         }
///     }
/// }
///
/// pub(crate) struct TestInit {
///     pub a: Vector3f,
///     pub b: Vector2f,
///     pub c: Vector4f,
/// }
///
/// impl TestInit {
///     pub fn init(self) -> Test {
///         Test {
///             __padding__0: Default::default(),
///             a: self.a,
///             __padding__1: Default::default(),
///             b: self.b,
///             __padding__3: Default::default(),
///             c: self.c,
///         }
///     }
/// }
///
/// ```
///
///
#[proc_macro_attribute]
pub fn field_align(attr: proc_macro::TokenStream, item: proc_macro::TokenStream) -> proc_macro::TokenStream {
    let input = parse_macro_input!(item as ItemStruct);

    let mut pad_self = false;
    let mut constructor = None;
    let mut init_suffix = None;
    let mut offset_suffix = None;

    {
        // dbg!(&attr);

        let mut attr = attr.into_iter();

        while let Some(t) = attr.next() {
            let s = t.to_string();

            let mut fetch_assign = |name: &str, default: &str| {
                if let Some(e) = attr.next() {
                    match e.to_string().as_str() {
                        "=" => {
                            if let Some(n) = attr.next() {
                                if let Some(l) = attr.next() {
                                    if &l.to_string() != "," {
                                        panic!("Extra ident after `{}` assignment", name);
                                    }
                                }
                                n.to_string()
                            } else {
                                panic!("Missing ident for `{}` assignment", name);
                            }
                        }
                        "," => default.to_string(),
                        _ => panic!("Invalid formatting after `{}`", name)
                    }
                } else {
                    default.to_string()
                }
            };

            match s.as_str() {
                "Self" => {
                    pad_self = true;
                    if let Some(l) = attr.next() {
                        if &l.to_string() != "," {
                            panic!("Extra ident after `Self`");
                        }
                    }
                }
                "new" => constructor = Some(fetch_assign("new", "new")),
                "init" => init_suffix = Some(fetch_assign("init", "Init")),
                "offset" => offset_suffix = Some(fetch_assign("offset", "offset")),
                s => panic!("Supported attributes are Self, new = <new name> and init = <init suffix>, found: {}", s)
            }
        }
    }

    let mut r = TokenStream::new();

    let repr_c = parse_quote!(#[repr(C)]);

    let mut field_alignment_factors = Vec::new();
    let mut field_attrib_ignore = Vec::new();
    let mut field_types = Vec::new();
    let mut field_names = Vec::new();

    match &input.fields {
        Fields::Named(fields) => {
            let mut process_field = |f: &Field| {
                // f.to_tokens(r)

                field_types.push(f.ty.clone());
                field_names.push(f.ident.clone().unwrap());

                let mut align_factor = None;
                let mut to_ignore = HashSet::new();

                for (i, a) in f.attrs.iter().enumerate() {
                    if a.path.leading_colon.is_none() {
                        if a.path.segments.len() == 1 {
                            if let Some(seg) = a.path.segments.first() {
                                if &seg.ident.to_string() == "align" {
                                    let s = a.tokens.to_string();
                                    assert!(s.starts_with('('));
                                    assert!(s.ends_with(')'));

                                    let v = &s[1..s.len() - 1];

                                    let n = v.parse::<usize>().unwrap_or_else(|_| {
                                        let m = (&v[3..]).parse::<usize>().expect(&format!("Invalid #[align(_)], only mat/vec are valid prefixes: {:?}", a.span()));

                                        match &v[0..3] {
                                            "vec" | "mat" => {
                                                match m {
                                                    1 => 1,
                                                    2 => 2,
                                                    3 | 4 => 4,
                                                    _ => panic!("Invalid #[align(_)], only matX/vecX from 1 to 4 are valid: {:?}", a.span())
                                                }
                                            }
                                            _ => panic!("Invalid #[align(_)], only mat/vec are valid prefixes: {:?}", a.span())
                                        }
                                    });

                                    // dbg!(n);

                                    align_factor = Some(n);
                                    to_ignore.insert(i);
                                }
                            }
                        }
                    }
                }

                if let Some(n) = align_factor {
                    field_alignment_factors.push(n);
                } else {
                    field_alignment_factors.push(1);
                }

                field_attrib_ignore.push(to_ignore);
            };

            for f in fields.named.pairs() {
                match f {
                    Pair::Punctuated(a, _) => {
                        process_field(a);
                    }
                    Pair::End(a) => {
                        process_field(a);
                    }
                }
            }
        }
        Fields::Unnamed(_) => {
            panic!("field_align expects a name struct");
            // fields.to_tokens(&mut r);
            // input.generics.where_clause.to_tokens(&mut r);
            // input.semi_token.to_tokens(&mut r);
        }
        Fields::Unit => {
            panic!("field_align expects a name struct");
            // input.generics.where_clause.to_tokens(&mut r);
            // input.semi_token.to_tokens(&mut r);
        }
    }

    r.append_all(input.attrs.iter().filter(|a| {
        match a.style {
            AttrStyle::Outer => true,
            AttrStyle::Inner(_) => false,
        }
    }).chain(core::iter::once(&repr_c)));
    input.vis.to_tokens(&mut r);
    input.struct_token.to_tokens(&mut r);
    input.ident.to_tokens(&mut r);
    input.generics.to_tokens(&mut r);
    match &input.fields {
        Fields::Named(fields) => {
            input.generics.where_clause.to_tokens(&mut r);
            // fields.to_tokens(&mut r);

            fields.brace_token.surround(&mut r, |r| {
                // fields.named.to_tokens(r);

                let process_field = |f: &Field, i: usize, r: &mut TokenStream| {
                    // f.to_tokens(r)

                    let this_align_mul = field_alignment_factors[i];

                    if i == 0 && pad_self {
                        let krate = if let Ok(krate) = crate_name("struct_field_align") {
                            match krate {
                                FoundCrate::Itself => format_ident!("struct_field_align"),
                                FoundCrate::Name(o) => format_ident!("{}", o),
                            }
                        } else {
                            let krate = crate_name("positronium_maths")
                                .expect("Failed to find `struct_field_align` or `positronium_maths` in Cargo.toml");
                            match krate {
                                FoundCrate::Itself => format_ident!("positronium_maths"),
                                FoundCrate::Name(o) => format_ident!("{}", o),
                            }
                        };

                        let these_field_alignment_factors = field_alignment_factors.clone();
                        let these_field_types = field_types.clone();

                        let tokens = quote! {
                            #[doc(hidden)]
                            __padding__0: #krate::Align<{
                                macro_rules! max {
                                    ($v0:expr, $($v:expr),+) => {
                                        if $v0 > max!( $($v),+ ) {
                                            $v0
                                        } else {
                                            max!( $($v),+ )
                                        }
                                    };
                                    ($v0:expr) => {
                                        $v0
                                    }
                                };

                                max!( #( #these_field_alignment_factors * ::std::mem::align_of::<# these_field_types>() ),* )
                            }>,
                        };

                        r.append_all(tokens);
                    } else if this_align_mul > 1 && i > 0 {
                        let padding_name = format_ident!("__padding__{}", i);

                        let this_field_type = field_types[i].clone();

                        let temp_names = (0..i).map(|i| format_ident!("F{}", i)).collect::<Vec<_>>();
                        let prev_temp_names = std::iter::once(format_ident!("BASE")).chain(temp_names.iter().cloned()).take(temp_names.len()).collect::<Vec<_>>();
                        let these_field_alignment_factors = field_alignment_factors.iter().take(i).copied().collect::<Vec<_>>();
                        let these_field_types = field_types.iter().take(i).cloned().collect::<Vec<_>>();

                        let last_temp_name = temp_names.last().unwrap().clone();

                        let tokens = quote! {
                            #[doc(hidden)]
                            #padding_name: [u8; {
                                pub const fn some_or_panic(v: Option<usize>) -> usize {
                                    match v {
                                        Some(v) => v,
                                        // None => panic!("Should not happen")
                                        None => loop {} // For now, since can guarantee I'm using it right?
                                    }
                                }
                                pub const fn ok_or_panic(v: ::core::result::Result<::std::alloc::Layout, ::std::alloc::LayoutError>) -> ::std::alloc::Layout {
                                    match v {
                                        Ok(v) => v,
                                        // Err(_) => panic!("Should not happen")
                                        Err(_) => loop {} // For now, since can guarantee I'm using it right?
                                    }
                                }
                                pub const fn my_padding_needed_for(this: &::std::alloc::Layout, align: usize) -> usize {
                                    let len = this.size();

                                    let len_rounded_up = len.wrapping_add(align).wrapping_sub(1) & !align.wrapping_sub(1);
                                    len_rounded_up.wrapping_sub(len)
                                }
                                pub const fn my_extend(this: &::std::alloc::Layout, next: ::std::alloc::Layout) -> (::std::alloc::Layout, usize) {
                                    let new_align = if this.align() > next.align() {
                                        this.align()
                                    } else {
                                        next.align()
                                    };
                                    let pad = my_padding_needed_for(this, next.align());

                                    let offset = some_or_panic(this.size().checked_add(pad));
                                    let new_size = some_or_panic(offset.checked_add(next.size()));

                                    let layout = ok_or_panic(::std::alloc::Layout::from_size_align(new_size, new_align));
                                    (layout, offset)
                                }

                                const BASE: (::std::alloc::Layout, usize) = (ok_or_panic(::std::alloc::Layout::from_size_align(0, 1)), 0);

                                // const F0: (Layout, usize) = ok_or_panic(BASE.extend(ok_or_panic(Layout::from_size_align(size_of::<Vector3f>(), 4 * align_of::<Vector3f>()))));
                                //const F0: (Layout, usize) = my_extend(&BASE, ok_or_panic(Layout::from_size_align(size_of::<Vector3f>(), 4 * align_of::<Vector3f>())));

                                #(const #temp_names : (::std::alloc::Layout, usize) = my_extend(& #prev_temp_names.0 , ok_or_panic(::std::alloc::Layout::from_size_align(::std::mem::size_of::< #these_field_types >(), #these_field_alignment_factors * ::std::mem::align_of::< #these_field_types >()))); )*


                                my_padding_needed_for(& #last_temp_name.0, #this_align_mul * ::std::mem::align_of::< #this_field_type >())
                            }],
                        };

                        r.append_all(tokens);
                    }

                    r.append_all(f.attrs.iter().enumerate()
                        .filter(|(j, _)| !field_attrib_ignore[i].contains(&j))
                        .map(|(_, a)| a.clone()));

                    f.vis.to_tokens(r);
                    if let Some(ident) = &f.ident {
                        ident.to_tokens(r);
                        // TokensOrDefault(&f.colon_token).to_tokens(r);
                        if let Some(c) = &f.colon_token {
                            c.to_tokens(r);
                        }
                    }
                    f.ty.to_tokens(r);
                };

                for (i, f) in fields.named.pairs().enumerate() {
                    match f {
                        Pair::Punctuated(a, b) => {
                            // a.to_tokens(r);
                            process_field(a, i, r);
                            b.to_tokens(r);
                        }
                        Pair::End(a) => {
                            // a.to_tokens(r)
                            process_field(a, i, r);
                        }
                    }
                }
            });
        }
        Fields::Unnamed(_) => {
            panic!("field_align expects a name struct");
            // fields.to_tokens(&mut r);
            // input.generics.where_clause.to_tokens(&mut r);
            // input.semi_token.to_tokens(&mut r);
        }
        Fields::Unit => {
            panic!("field_align expects a name struct");
            // input.generics.where_clause.to_tokens(&mut r);
            // input.semi_token.to_tokens(&mut r);
        }
    }

    if let Some(offset_suffix) = offset_suffix {
        let mut offsets = TokenStream::new();

        match &input.fields {
            Fields::Named(fields) => {
                let process_field = |i: usize, offsets: &mut TokenStream| {
                    // f.to_tokens(r)

                    let this_align_mul = field_alignment_factors[i];

                    if i == 0 {
                        let this_name = format_ident!("{}_{}", field_names[0], offset_suffix);

                        let offset = quote! {
                            pub const fn #this_name() -> usize {
                               0
                            }
                        };

                        offsets.append_all(offset);
                    } else if i > 0 {
                        let this_field_type = field_types[i].clone();

                        let this_name = format_ident!("{}_{}", field_names[i], offset_suffix);
                        let temp_names = (0..i).map(|i| format_ident!("F{}", i)).collect::<Vec<_>>();
                        let prev_temp_names = std::iter::once(format_ident!("BASE")).chain(temp_names.iter().cloned()).take(temp_names.len()).collect::<Vec<_>>();
                        let these_field_alignment_factors = field_alignment_factors.iter().take(i).copied().collect::<Vec<_>>();
                        let these_field_types = field_types.iter().take(i).cloned().collect::<Vec<_>>();


                        let last_temp_name = temp_names.last().unwrap().clone();

                        let offset = quote! {
                            pub const fn #this_name() -> usize
                            {
                                pub const fn some_or_panic(v: Option<usize>) -> usize {
                                    match v {
                                        Some(v) => v,
                                        // None => panic!("Should not happen")
                                        None => loop {} // For now, since can guarantee I'm using it right?
                                    }
                                }
                                pub const fn ok_or_panic(v: ::core::result::Result<::std::alloc::Layout, ::std::alloc::LayoutError>) -> ::std::alloc::Layout {
                                    match v {
                                        Ok(v) => v,
                                        // Err(_) => panic!("Should not happen")
                                        Err(_) => loop {} // For now, since can guarantee I'm using it right?
                                    }
                                }
                                pub const fn my_padding_needed_for(this: &::std::alloc::Layout, align: usize) -> usize {
                                    let len = this.size();

                                    let len_rounded_up = len.wrapping_add(align).wrapping_sub(1) & !align.wrapping_sub(1);
                                    len_rounded_up.wrapping_sub(len)
                                }
                                pub const fn my_extend(this: &::std::alloc::Layout, next: ::std::alloc::Layout) -> (::std::alloc::Layout, usize) {
                                    let new_align = if this.align() > next.align() {
                                        this.align()
                                    } else {
                                        next.align()
                                    };
                                    let pad = my_padding_needed_for(this, next.align());

                                    let offset = some_or_panic(this.size().checked_add(pad));
                                    let new_size = some_or_panic(offset.checked_add(next.size()));

                                    let layout = ok_or_panic(::std::alloc::Layout::from_size_align(new_size, new_align));
                                    (layout, offset)
                                }

                                const BASE: (::std::alloc::Layout, usize) = (ok_or_panic(::std::alloc::Layout::from_size_align(0, 1)), 0);

                                // const F0: (Layout, usize) = ok_or_panic(BASE.extend(ok_or_panic(Layout::from_size_align(size_of::<Vector3f>(), 4 * align_of::<Vector3f>()))));
                                //const F0: (Layout, usize) = my_extend(&BASE, ok_or_panic(Layout::from_size_align(size_of::<Vector3f>(), 4 * align_of::<Vector3f>())));

                                #(const #temp_names : (::std::alloc::Layout, usize) = my_extend(& #prev_temp_names.0, ok_or_panic(::std::alloc::Layout::from_size_align(::std::mem::size_of::< #these_field_types >(), #these_field_alignment_factors * ::std::mem::align_of::< #these_field_types >()))); )*


                                const PAD: usize = my_padding_needed_for(& #last_temp_name.0, #this_align_mul * ::std::mem::align_of::< #this_field_type >());

                                PAD + #last_temp_name.0.size()
                            }
                        };

                        offsets.append_all(offset);
                    }
                };

                for (i, _) in fields.named.pairs().enumerate() {
                    process_field(i, &mut offsets);
                }
            }
            Fields::Unnamed(_) => {
                panic!("field_align expects a name struct");
                // fields.to_tokens(&mut r);
                // input.generics.where_clause.to_tokens(&mut r);
                // input.semi_token.to_tokens(&mut r);
            }
            Fields::Unit => {
                panic!("field_align expects a name struct");
                // input.generics.where_clause.to_tokens(&mut r);
                // input.semi_token.to_tokens(&mut r);
            }
        }

        let name = input.ident.clone();

        let tokens = quote! {
            impl #name {
                #offsets
            }
        };

        r.append_all(tokens);
    }

    if let Some(constructor) = constructor {
        let constructor_name = format_ident!("{}", constructor);

        let mut field_inits = TokenStream::new();

        for i in 0..field_names.len() {
            let this_align_mul = field_alignment_factors[i];

            if i == 0 && pad_self {
                let tokens = quote! {
                    __padding__0: ::core::default::Default::default(),
                };

                field_inits.append_all(tokens);
            } else if this_align_mul > 1 && i > 0 {
                let padding_name = format_ident!("__padding__{}", i);

                let tokens = quote! {
                    #padding_name: ::core::default::Default::default(),
                };

                field_inits.append_all(tokens);
            }

            let field_name = field_names[i].clone();

            let tokens = quote! {
                #field_name,
            };

            field_inits.append_all(tokens);
        }

        let name = input.ident.clone();

        let tokens = quote! {
            impl #name {
                pub fn #constructor_name ( #(#field_names: #field_types),* ) -> Self {
                    #name {
                        #field_inits
                    }
                }
            }
        };

        r.append_all(tokens);
    }

    if let Some(init_suffix) = init_suffix {
        let name = input.ident.clone();
        let new_name = format_ident!("{}{}", input.ident, init_suffix);

        let vis = input.vis.clone();

        let mut field_inits = TokenStream::new();

        for i in 0..field_names.len() {
            let this_align_mul = field_alignment_factors[i];

            if i == 0 && pad_self {
                let tokens = quote! {
                    __padding__0: ::core::default::Default::default(),
                };

                field_inits.append_all(tokens);
            } else if this_align_mul > 1 && i > 0 {
                let padding_name = format_ident!("__padding__{}", i);

                let tokens = quote! {
                    #padding_name: ::core::default::Default::default(),
                };

                field_inits.append_all(tokens);
            }

            let field_name = field_names[i].clone();

            let tokens = quote! {
                #field_name: self.#field_name,
            };

            field_inits.append_all(tokens);
        }

        let tokens = quote! {
            #vis struct #new_name {
                #(pub #field_names: #field_types,)*
            }

            impl #new_name {
                pub fn init(self) -> #name {
                    #name {
                        #field_inits
                    }
                }
            }
        };

        r.append_all(tokens);
    }

    proc_macro::TokenStream::from(r)
}