use std::fmt::{Debug, Display};
use std::iter::FusedIterator;
use std::ops::{AddAssign, Deref, DerefMut, DivAssign, Index, IndexMut, MulAssign, SubAssign};

use crate::{Matrix, Scalar, Vector};

pub struct MatrixSlice<T, const R: usize, const C: usize> {
    pub(super) values: [T],
}

// ref to is memory equivalent to MatrixSlice
pub struct MatrixColumnSlice<T, const R: usize, const C: usize> {
    pub(super) _values: [T],
}

// ref to is memory equivalent to MatrixSlice
pub struct MatrixRowSlice<T, const R: usize, const C: usize> {
    pub(super) _values: [T],
}

// Row/Column specialisation

impl<T: Scalar, const R: usize, const C: usize> MatrixColumnSlice<T, R, C> {
    pub fn vector<const N: usize>(&self) -> Vector<T, N> {
        self.into()
    }

    pub fn set_vector<const N: usize>(&mut self, vector: Vector<T, N>) {
        self.set(Matrix::column_vectors([vector]));
    }

    pub fn column_matrix<const N: usize>(&self) -> Matrix<T, N, 1> {
        self.into()
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize> From<&MatrixColumnSlice<T, R, C>> for Vector<T, N> {
    fn from(m: &MatrixColumnSlice<T, R, C>) -> Self {
        m.iter_column_wise().copied().collect()
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize> From<&MatrixColumnSlice<T, R, C>> for Matrix<T, N, 1> {
    fn from(m: &MatrixColumnSlice<T, R, C>) -> Self {
        Matrix::column_vectors([m.into()])
    }
}

impl<T: Scalar, const R: usize, const C: usize> Deref for MatrixColumnSlice<T, R, C> {
    type Target = MatrixSlice<T, R, C>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: Scalar, const R: usize, const C: usize> DerefMut for MatrixColumnSlice<T, R, C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: Scalar, const R: usize, const C: usize> MatrixRowSlice<T, R, C> {
    pub fn vector<const N: usize>(&self) -> Vector<T, N> {
        self.into()
    }

    pub fn set_vector<const N: usize>(&mut self, vector: Vector<T, N>) {
        self.set(Matrix::row_vectors([vector]));
    }

    pub fn row_matrix<const N: usize>(&self) -> Matrix<T, 1, N> {
        self.into()
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize> From<&MatrixRowSlice<T, R, C>> for Vector<T, N> {
    fn from(m: &MatrixRowSlice<T, R, C>) -> Self {
        m.iter_column_wise().copied().collect()
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize> From<&MatrixRowSlice<T, R, C>> for Matrix<T, 1, N> {
    fn from(m: &MatrixRowSlice<T, R, C>) -> Self {
        Matrix::row_vectors([m.into()])
    }
}

impl<T: Scalar, const R: usize, const C: usize> Deref for MatrixRowSlice<T, R, C> {
    type Target = MatrixSlice<T, R, C>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: Scalar, const R: usize, const C: usize> DerefMut for MatrixRowSlice<T, R, C> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

// Associated  functions

impl<T: Scalar, const R: usize, const C: usize> MatrixSlice<T, R, C> {
    pub fn size(&self) -> (usize, usize) {
        let l = self.values.len();

        if l <= R {
            let columns = 1;
            let rows = l;

            (rows, columns)
        } else {
            let missing_elements = R * C - l;

            let columns_missing = (missing_elements) / R;
            let columns = C - columns_missing;

            let rows_missing = R * columns - l;
            let rows = R - rows_missing;

            (rows, columns)
        }
    }

    #[track_caller]
    pub fn set<const N: usize, const M: usize>(&mut self, value: Matrix<T, N, M>) {
        assert_eq!(self.size(), (N, M));

        for (old, new) in self.iter_column_wise_mut().zip(value.into_iter_column_wise()) {
            *old = new;
        }
    }

    #[track_caller]
    pub fn set_slice<const N: usize, const M: usize>(&mut self, slice: &MatrixSlice<T, N, M>) {
        assert_eq!(self.size(), slice.size());

        for (old, new) in self.iter_column_wise_mut().zip(slice.iter_column_wise().copied()) {
            *old = new;
        }
    }

    #[track_caller]
    pub fn set_with<F, const N: usize, const M: usize>(&mut self, f: F) where F: FnOnce(&Self) -> Matrix<T, N, M>, T: Copy {
        assert_eq!(self.size(), (N, M));

        let result = f(self);

        for (old, new) in self.iter_column_wise_mut().zip(result.into_iter_column_wise()) {
            *old = new;
        }
    }

    pub fn matrix<const N: usize, const M: usize>(&self) -> Matrix<T, N, M> {
        self.into()
    }

    pub fn iter_column_wise(&self) -> Iter<T, R, C> {
        let (r_size, c_size) = self.size();
        Iter {
            slice: self,
            r_size,
            c_size,
            row: 0,
            column: 0,
        }
    }

    pub fn iter_column_wise_mut(&mut self) -> IterMut<T, R, C> {
        let (r_size, c_size) = self.size();
        IterMut {
            slice: self,
            r_size,
            c_size,
            row: 0,
            column: 0,
        }
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize, const M: usize> From<&MatrixSlice<T, N, M>> for Matrix<T, R, C> {
    #[track_caller]
    fn from(slice: &MatrixSlice<T, N, M>) -> Self {
        assert_eq!(slice.size(), (R, C), "The size of the source and destination of the conversion do not match, source: {:?} != dest: {:?}", slice.size(), (R, C));

        Matrix::from_iter_column_wise(slice.iter_column_wise().copied())
    }
}

// Iterators

pub struct Iter<'slice, T: Scalar, const R: usize, const C: usize> {
    slice: &'slice MatrixSlice<T, R, C>,
    r_size: usize,
    c_size: usize,
    row: usize,
    column: usize,
}

impl<'slice, T: Scalar, const R: usize, const C: usize> Iterator for Iter<'slice, T, R, C> {
    type Item = &'slice T;

    fn next(&mut self) -> Option<&'slice T> {
        (self.row < self.r_size && self.column < self.c_size).then(|| {
            let v = &self.slice[self.row][self.column];

            self.row += 1;
            if self.row == self.r_size {
                self.row = 0;
                self.column += 1;
            }

            v
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = (self.c_size - self.column - 1) * self.r_size + (self.r_size - self.row);
        (size, Some(size))
    }
}

impl<T: Scalar, const R: usize, const C: usize> ExactSizeIterator for Iter<'_, T, R, C> {}

impl<T: Scalar, const R: usize, const C: usize> FusedIterator for Iter<'_, T, R, C> {}

pub struct IterMut<'slice, T: Scalar, const R: usize, const C: usize> {
    slice: &'slice mut MatrixSlice<T, R, C>,
    r_size: usize,
    c_size: usize,
    row: usize,
    column: usize,
}

impl<'slice, T: Scalar, const R: usize, const C: usize> Iterator for IterMut<'slice, T, R, C> {
    type Item = &'slice mut T;

    fn next(&mut self) -> Option<&'slice mut T> {
        (self.row < self.r_size && self.column < self.c_size).then(|| {
            let v = &mut self.slice[self.row][self.column];
            let v = unsafe { &mut *(v as *mut T) }; // For some reason compiler complains about not being able to infer lifetime, despite doing things the same as in Vec IterMut for example, so cheat like they do

            self.row += 1;
            if self.row == self.r_size {
                self.row = 0;
                self.column += 1;
            }

            v
        })
    }

    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = (self.c_size - self.column - 1) * self.r_size + (self.r_size - self.row);
        (size, Some(size))
    }
}

impl<T: Scalar, const R: usize, const C: usize> ExactSizeIterator for IterMut<'_, T, R, C> {}

impl<T: Scalar, const R: usize, const C: usize> FusedIterator for IterMut<'_, T, R, C> {}

// Add

impl<T: Scalar, const R: usize, const C: usize, const N: usize, const M: usize> AddAssign<&MatrixSlice<T, M, N>> for MatrixSlice<T, R, C> {
    #[track_caller]
    fn add_assign(&mut self, rhs: &MatrixSlice<T, M, N>) {
        assert_eq!(self.size(), rhs.size(), "Can not add matrices of different sizes");

        for (l, r) in self.iter_column_wise_mut().zip(rhs.iter_column_wise()) {
            *l += *r;
        }
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize, const M: usize> AddAssign<Matrix<T, N, M>> for MatrixSlice<T, R, C> {
    #[track_caller]
    fn add_assign(&mut self, rhs: Matrix<T, N, M>) {
        assert_eq!(self.size(), (N, M), "Can not add matrices of different sizes");

        for (l, r) in self.iter_column_wise_mut().zip(rhs.into_iter_column_wise()) {
            *l += r;
        }
    }
}

// Subtract

impl<T: Scalar, const R: usize, const C: usize, const N: usize, const M: usize> SubAssign<&MatrixSlice<T, M, N>> for MatrixSlice<T, R, C> {
    #[track_caller]
    fn sub_assign(&mut self, rhs: &MatrixSlice<T, M, N>) {
        assert_eq!(self.size(), rhs.size(), "Can not sub matrices of different sizes");

        for (l, r) in self.iter_column_wise_mut().zip(rhs.iter_column_wise()) {
            *l -= *r;
        }
    }
}

impl<T: Scalar, const R: usize, const C: usize, const N: usize, const M: usize> SubAssign<Matrix<T, N, M>> for MatrixSlice<T, R, C> {
    #[track_caller]
    fn sub_assign(&mut self, rhs: Matrix<T, N, M>) {
        assert_eq!(self.size(), (N, M), "Can not sub matrices of different sizes");

        for (l, r) in self.iter_column_wise_mut().zip(rhs.into_iter_column_wise()) {
            *l -= r;
        }
    }
}

// Multiply

impl<T: Scalar, const R: usize, const C: usize> MulAssign<T> for MatrixSlice<T, R, C> {
    fn mul_assign(&mut self, rhs: T) {
        for v in self.iter_column_wise_mut() {
            *v *= rhs;
        }
    }
}

// Divide

impl<T: Scalar, const R: usize, const C: usize> DivAssign<T> for MatrixSlice<T, R, C> {
    fn div_assign(&mut self, rhs: T) {
        for v in self.iter_column_wise_mut() {
            *v /= rhs;
        }
    }
}

// Indexing

pub struct MatrixSlicePartialIndex<T, const R: usize, const C: usize> {
    values: [T],
}

impl<T: Scalar, const R: usize, const C: usize> Index<usize> for MatrixSlice<T, R, C> {
    type Output = MatrixSlicePartialIndex<T, R, C>;

    #[track_caller]
    fn index(&self, r: usize) -> &MatrixSlicePartialIndex<T, R, C> {
        let (rows, _columns) = self.size();
        assert!(r < rows);

        let new_len = self.values.len() - (rows - 1);

        unsafe { core::mem::transmute(core::slice::from_raw_parts(&self.values[r] as *const T, new_len)) }
    }
}

impl<T: Scalar, const R: usize, const C: usize> Index<usize> for MatrixSlicePartialIndex<T, R, C> {
    type Output = T;

    #[track_caller]
    fn index(&self, c: usize) -> &T {
        let len = self.values.len();
        let columns = ((len - 1) / R) + 1; // len = 1 + (columns - 1) * R

        assert!(c < columns);

        &self.values[c * R]
    }
}

impl<T: Scalar, const R: usize, const C: usize> IndexMut<usize> for MatrixSlice<T, R, C> {
    #[track_caller]
    fn index_mut(&mut self, r: usize) -> &mut MatrixSlicePartialIndex<T, R, C> {
        let (rows, _columns) = self.size();
        assert!(r < rows);

        let new_len = self.values.len() - (rows - 1);

        unsafe { core::mem::transmute(core::slice::from_raw_parts_mut(&mut self.values[r] as *mut T, new_len)) }
    }
}

impl<T: Scalar, const R: usize, const C: usize> IndexMut<usize> for MatrixSlicePartialIndex<T, R, C> {
    #[track_caller]
    fn index_mut(&mut self, c: usize) -> &mut T {
        let len = self.values.len();
        let columns = ((len - 1) / R) + 1; // len = 1 + (columns - 1) * R

        assert!(c < columns);

        &mut self.values[c * R]
    }
}

// Printing

macro_rules! matrix_slice_printing {
    ($variant:ident, $tr:ident, $format:literal) => {
        impl<T: Scalar, const R: usize, const C: usize> $tr for $variant<T, R, C> where T: $tr {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                let (rows, columns) = self.size();

                write!(f, "\n")?;

                if rows != R {
                    write!(f, "[")?;

                    if columns != C {
                        write!(f, " \\   ")?;
                    }

                    for c in 0..columns {
                        let w = (0..rows).map(|r| format!($format, self[r][c]).len()).max().unwrap();

                        if c != 0 {
                            write!(f, "  ")?;
                        }

                        write!(f, "{}:{}", " ".repeat((w - 1) / 2), " ".repeat(w / 2))?;
                    }

                    if columns != C {
                        write!(f, "   / ")?;
                    }

                    writeln!(f, "]")?;
                }

                for r in 0..rows {
                    write!(f, "[")?;

                    if columns != C {
                        write!(f, "...  ")?;
                    }

                    for c in 0..columns {
                        let w = (0..rows).map(|r| format!($format, self[r][c]).len()).max().unwrap();

                        if c != 0 {
                            write!(f, "  ")?;
                        }
                        let s = format!($format, self[r][c]);
                        write!(f, "{}{}", s, " ".repeat(w - s.len()))?;
                    }

                    if columns != C {
                        write!(f, "  ...")?;
                    }

                    writeln!(f, "]")?;
                }

                if rows != R {
                    write!(f, "[")?;

                    if columns != C {
                        write!(f, " /   ")?;
                    }

                    for c in 0..columns {
                        let w = (0..rows).map(|r| format!($format, self[r][c]).len()).max().unwrap();

                        if c != 0 {
                            write!(f, "  ")?;
                        }

                        write!(f, "{}:{}", " ".repeat((w - 1) / 2), " ".repeat(w / 2))?;
                    }

                    if columns != C {
                        write!(f, "   \\ ")?;
                    }

                    writeln!(f, "]")?;
                }

                Ok(())
            }
        }

    };

}

matrix_slice_printing!(MatrixSlice, Debug, "{:?}");
matrix_slice_printing!(MatrixSlice, Display, "{}");