use crate::{ConstTest, Matrix, MatrixSlice, FloatScalar, Scalar, SignedScalar, True, Vector};
use crate::array::ConstArray;

impl<T: Copy, const R: usize, const C: usize> Matrix<T, R, C> {
    pub fn transpose(self) -> Matrix<T, C, R> {
        Matrix::from_fn(|r, c| self[c][r])
    }
}

impl<T: Scalar, const N: usize> Matrix<T, N, N> {
    pub fn transpose_assign(&mut self) {
        for r in 0..N {
            for c in 0..r {
                let i = r + N * c;
                let j = c + N * r;

                self.as_slice_column_wise_mut().swap(i, j);
            }
        }
    }

    pub fn trace(&self) -> T {
        let mut sum = T::ZERO;
        for i in 0..N {
            sum += self[i][i];
        }
        sum
    }

    pub fn powi(self, n: isize) -> Option<Matrix<T, N, N>> where T: FloatScalar, Self: Invert {
        let a = n.unsigned_abs();
        if n >= 0 {
            Some(self.powu(a))
        } else {
            self.invert().map(|m| m.powu(a))
        }
    }

    pub fn powi_unchecked(self, n: isize) -> Matrix<T, N, N> where T: FloatScalar, Self: Invert {
        let a = n.unsigned_abs();
        if n >= 0 {
            self.powu(a)
        } else {
            self.invert_unchecked().powu(a)
        }
    }

    // Uses successive squaring to process in O(log2(n)) matrix multiplications
    pub fn powu(self, mut n: usize) -> Matrix<T, N, N> {
        let mut current_total = Matrix::<T, N, N>::identity();
        let mut current_ss = self;

        while n > 0 {
            if n & 1 == 1 {
                current_total = current_total * current_ss;
            }
            current_ss = current_ss * current_ss;
            n >>= 1;
        }

        current_total
    }
}

impl<T: Scalar, const R: usize, const C: usize> MatrixSlice<T, R, C> {
    #[track_caller]
    pub fn transpose_assign(&mut self) {
        let (rows, columns) = self.size();
        assert_eq!(rows, columns, "Can only transpose a square matrix slice");
        let size = rows;

        for r in 0..size {
            for c in 0..r {
                let i = r + R * c;
                let j = c + R * r;

                self.values.swap(i, j);
            }
        }
    }
}

pub(crate) use determinant::Determinant;

pub mod determinant {
    use super::*;

    pub trait Determinant<T: Scalar> {
        fn determinant(&self) -> T;
    }
}

// 0 size case to prevent issues with cofactor calculation
// Should be unreachable in normal cases
impl<T: SignedScalar> Determinant<T> for Matrix<T, 0, 0> {
    #[inline(always)]
    fn determinant(&self) -> T {
        unreachable!("The determinant for a 0x0 matrix makes no sense")
    }
}

// Base case
impl<T: SignedScalar> Determinant<T> for Matrix<T, 1, 1> {
    #[inline(always)]
    fn determinant(&self) -> T {
        self[0][0]
    }
}

impl<T: SignedScalar, const N: usize> Matrix<T, N, N> {
    const fn minor_step<'a, const M: usize>(&'a self, prev: &'a impl Fn([usize; M]) -> T) -> impl Fn([usize; M + 1]) -> T + 'a where [(); M + 1]: {
        |rows: [usize; M + 1]| {
            let mut result = T::ZERO;
            let mut c = 0;
            let mut indexer = crate::array::OffsetIndexArray::<1, M>::ARRAY;

            for i in 0..M + 1 {
                if i % 2 == 0 {
                    result += self[rows[c]][N - (M + 1)] * prev(crate::array::shuffle(&rows, &indexer));
                } else {
                    result -= self[rows[c]][N - (M + 1)] * prev(crate::array::shuffle(&rows, &indexer));
                }
                if i != M {
                    std::mem::swap(&mut c, &mut indexer[i]);
                }
            }
            result
        }
    }
}

macro_rules! matrix_det {
    ($size:literal, $($step:literal),+) => {
        impl<T: SignedScalar> Determinant<T> for Matrix<T, $size, $size> {
            // #[inline(always)]
            fn determinant(&self) -> T {
                let m = |rows: [usize; 1]| {
                    self[rows[0]][$size - 1]
                };

                $(
                    let m = self.minor_step(&m);
                    let _ = $step;
                )*

                m(crate::array::IndexArray::<$size>::ARRAY)
            }
        }

        matrix_det!($($step),+);
    };
    ($size:literal) => {}
}

// TODO: Might be able to remove need for macro once const generic expressions are improved
// Note: Not using 16 as upper limit as compilation starts taking a long time
matrix_det!(10, 9, 8, 7, 6, 5, 4, 3, 2, 1);

impl<T: SignedScalar, const N: usize> Matrix<T, N, N> where Self: Determinant<T> {
    #[inline(always)]
    pub fn determinant(&self) -> T {
        <Self as Determinant<T>>::determinant(self)
    }
}

pub(crate) use cofactor::Cofactor;

pub mod cofactor {
    pub trait Cofactor {
        fn cofactor(&self) -> Self;
    }
}

impl<T: SignedScalar, const N: usize> Cofactor for Matrix<T, N, N> where ConstTest<{ N > 0 }>: True, [(); N - 1]:, Matrix<T, { N - 1 }, { N - 1 }>: Determinant<T> {
    // #[inline(always)]
    fn cofactor(&self) -> Self {
        if N == 1 {
            Self::identity()
        } else {
            Self::from_fn(|r, c| {
                // let checker_board = T::from((-1i8).pow((r + c) as u32)).unwrap();
                let checker_board = if (r + c) % 2 == 0 { T::ONE } else { T::NEGATIVE_ONE };
                checker_board * self.cut_row_column(r, c).determinant()
            })
        }
    }
}

impl<T: SignedScalar, const N: usize> Matrix<T, N, N> where Self: Cofactor {
    #[inline(always)]
    pub fn cofactor(&self) -> Self {
        <Self as Cofactor>::cofactor(self)
    }

    pub fn cofactor_and_determinant(&self) -> (Self, T) {
        let cofactor = self.cofactor();
        let det = self.column_vector(0) * cofactor.column_vector(0);
        (cofactor, det)
    }
}

pub(crate) use invert::Invert;

pub mod invert {
    use crate::Cofactor;

    pub trait Invert: Sized + Cofactor {
        fn invert(&self) -> Option<Self>;
        fn invert_unchecked(&self) -> Self;
    }
}

impl<T: FloatScalar, const N: usize> Invert for Matrix<T, N, N> where Self: Cofactor {
    // #[inline(always)]
    fn invert(&self) -> Option<Self> {
        let (cofactor, det) = self.cofactor_and_determinant();
        (det != T::ZERO).then(|| cofactor.transpose() / det)
    }

    // #[inline(always)]
    fn invert_unchecked(&self) -> Self {
        let (cofactor, det) = self.cofactor_and_determinant();
        cofactor.transpose() / det
    }
}

impl<T: FloatScalar, const N: usize> Matrix<T, N, N> where Self: Invert {
    #[inline(always)]
    pub fn invert(&self) -> Option<Self> {
        <Self as Invert>::invert(self)
    }

    #[inline(always)]
    pub fn invert_unchecked(&self) -> Self {
        <Self as Invert>::invert_unchecked(self)
    }
}

impl<T: FloatScalar, const R: usize, const C: usize> Matrix<T, R, C> {
    pub fn augmented_gauss_jordan(mut self, mut vector: Vector<T, R>) -> (Self, Vector<T, R>) {
        // dbg!(&self);
        let mut pivot_r = 0;
        let mut pivot_c = 0;

        while pivot_r < R && pivot_c < C {
            let max_r = (pivot_r..R).max_by(|a, b| self[*a][pivot_c].abs().partial_cmp(&self[*b][pivot_c].abs()).unwrap()).unwrap();
            if self[max_r][pivot_c] == T::ZERO {
                pivot_c += 1;
            } else {
                self.swap_rows_assign(pivot_r, max_r);
                vector.swap_assign(pivot_r, max_r);
                for under_r in pivot_r + 1..R {
                    let f = self[under_r][pivot_c] / self[pivot_r][pivot_c];

                    self[under_r][pivot_c] = T::ZERO;
                    for c in pivot_c + 1..C {
                        self[under_r][c] = self[under_r][c] - self[pivot_r][c] * f;
                    }

                    vector[under_r] = vector[under_r] - vector[pivot_r] * f;
                }
                pivot_r += 1;
                pivot_c += 1;
            }
        }
        // dbg!(&self);

        for c in (0..C).rev() {
            if let Some(last_r) = (0..R).rev().find(|r| self[*r][c] != T::ZERO) {
                for r in 0..last_r {
                    let f = self[r][c] / self[last_r][c];
                    self[r][c] = T::ZERO; // Not _needed_, just for returning self

                    vector[r] = vector[r] - vector[last_r] * f;
                }

                vector[last_r] = vector[last_r] / self[last_r][c];
                self[last_r][c] = T::ONE; // Not _needed_, just for returning self
            }
        }
        // dbg!(&self);

        (self, vector)
    }

    pub fn super_augmented_gauss_jordan<const M: usize>(mut self, mut matrix: Matrix<T, R, M>) -> (Self, Matrix<T, R, M>) {
        // dbg!(&self);
        let mut pivot_r = 0;
        let mut pivot_c = 0;

        while pivot_r < R && pivot_c < C {
            let max_r = (pivot_r..R).max_by(|a, b| self[*a][pivot_c].abs().partial_cmp(&self[*b][pivot_c].abs()).unwrap()).unwrap();
            if self[max_r][pivot_c] == T::ZERO {
                pivot_c += 1;
            } else {
                self.swap_rows_assign(pivot_r, max_r);
                matrix.swap_rows_assign(pivot_r, max_r);
                for under_r in pivot_r + 1..R {
                    let f = self[under_r][pivot_c] / self[pivot_r][pivot_c];

                    self[under_r][pivot_c] = T::ZERO;
                    for c in pivot_c + 1..C {
                        self[under_r][c] = self[under_r][c] - self[pivot_r][c] * f;
                    }

                    for k in 0..M {
                        matrix[under_r][k] = matrix[under_r][k] - matrix[pivot_r][k] * f;
                    }
                }
                pivot_r += 1;
                pivot_c += 1;
            }
        }
        // dbg!(&self);

        for c in (0..C).rev() {
            if let Some(last_r) = (0..R).rev().find(|r| self[*r][c] != T::ZERO) {
                for r in 0..last_r {
                    let f = self[r][c] / self[last_r][c];
                    self[r][c] = T::ZERO; // Not _needed_, just for returning self

                    for k in 0..M {
                        matrix[r][k] = matrix[r][k] - matrix[last_r][k] * f;
                    }
                }

                for k in 0..M {
                    matrix[last_r][k] = matrix[last_r][k] / self[last_r][c];
                }
                self[last_r][c] = T::ONE; // Not _needed_, just for returning self
            }
        }
        // dbg!(&self);

        (self, matrix)
    }
}

impl<T: FloatScalar, const N: usize> Matrix<T, N, N> {
    /// Note: This is less reliable for near zero determinate matrices
    pub fn gauss_jordan_invert(self) -> Option<Self> {
        let (identity, result) = self.super_augmented_gauss_jordan(Self::identity());

        for r in 0..N {
            if identity[r][..].iter_column_wise().copied().all(|v| v == T::ZERO) {
                return None;
            }
        }
        for c in 0..N {
            if identity[..][c].iter_column_wise().copied().all(|v| v == T::ZERO) {
                return None;
            }
        }

        Some(result)
    }

    pub fn gauss_jordan_invert_unchecked(self) -> Self {
        self.super_augmented_gauss_jordan(Self::identity()).1
    }
}

#[cfg(test)]
mod mat_calc_tests {
    use rand::Rng;

    use crate::*;

    #[test]
    fn det_f32_2_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix2f::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_3_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix3f::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_4_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix4f::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_5_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f32, 5, 5>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_6_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f32, 6, 6>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_7_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f32, 7, 7>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f32_8_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f32, 8, 8>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_2_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix2d::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_3_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix3d::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_4_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix4d::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_5_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f64, 5, 5>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_6_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f64, 6, 6>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_7_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f64, 7, 7>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn det_f64_8_test() {
        let mut rng = rand::thread_rng();

        let mut sum = 0.0;

        for _ in 0..1_000_000 {
            let m = Matrix::<f64, 8, 8>::from_fn(|_, _| rng.gen());
            sum += m.determinant();
        }

        dbg!(sum);
    }

    #[test]
    fn invert_2x2() {
        let mut rng = rand::thread_rng();

        for _ in 0..100_000 {
            let mut m = Matrix2d::from_fn(|_, _| rng.gen());

            if rng.gen::<f32>() < 0.1 {
                m[1][0] = (m[0][0] * m[1][1]) / m[0][1];
            }

            // dbg!(m);
            let i = m.invert();
            // dbg!(i);
            let d = m.determinant();

            if d == 0.0 {
                assert_eq!(i, None);
            } else {
                assert!(i.is_some());

                let n = i.unwrap() * m;
                let close_enough = n.into_iter_column_wise().zip(Matrix2d::identity().into_iter_column_wise()).all(|(l, r)| (l - r).abs() < f32::EPSILON as f64);
                assert!(close_enough, "{}, {}, {}", m, i.unwrap(), n);
            }
        }
    }

    #[test]
    fn invert_3x3() {
        let mut rng = rand::thread_rng();

        for _ in 0..100_000 {
            let m = Matrix3d::from_fn(|_, _| rng.gen());

            // if rng.gen::<f32>() < 0.1 {
            //     m[1][0] = (m[0][0] * m[1][1]) / m[0][1];
            // }

            // dbg!(m);
            let i = m.invert();
            // dbg!(i);
            let d = m.determinant();

            if d == 0.0 {
                assert_eq!(i, None);
            } else {
                assert!(i.is_some());

                let n = i.unwrap() * m;
                let close_enough = n.into_iter_column_wise().zip(Matrix3d::identity().into_iter_column_wise()).all(|(l, r)| (l - r).abs() < f32::EPSILON as f64);
                assert!(close_enough, "{}", n);
            }
        }
    }

    #[test]
    fn invert_4x4() {
        let mut rng = rand::thread_rng();

        for _ in 0..100_000 {
            let m = Matrix4d::from_fn(|_, _| rng.gen());

            // if rng.gen::<f32>() < 0.1 {
            //     m[1][0] = (m[0][0] * m[1][1]) / m[0][1];
            // }

            // dbg!(m);
            let i = m.invert();
            // dbg!(i);
            let d = m.determinant();

            if d == 0.0 {
                assert_eq!(i, None);
            } else {
                assert!(i.is_some());

                let n = i.unwrap() * m;
                let close_enough = n.into_iter_column_wise().zip(Matrix4d::identity().into_iter_column_wise()).all(|(l, r)| (l - r).abs() < f32::EPSILON as f64);
                assert!(close_enough, "{}", n);
            }
        }
    }

    #[test]
    fn invert_10x10() {
        let mut rng = rand::thread_rng();

        for _ in 0..100 {
            let m = Matrix::<f64, 10, 10>::from_fn(|_, _| rng.gen());

            // if rng.gen::<f32>() < 0.1 {
            //     m[1][0] = (m[0][0] * m[1][1]) / m[0][1];
            // }

            // dbg!(m);
            let i = m.invert();
            // dbg!(i);
            let d = m.determinant();

            if d == 0.0 {
                assert_eq!(i, None);
            } else {
                assert!(i.is_some());

                let n = i.unwrap() * m;
                let close_enough = n.into_iter_column_wise().zip(Matrix::<f64, 10, 10>::identity().into_iter_column_wise()).all(|(l, r)| (l - r).abs() < f32::EPSILON as f64);
                assert!(close_enough, "{}", n);
            }
        }
    }

    #[test]
    fn gj_invert_10x10() {
        let mut rng = rand::thread_rng();

        for _ in 0..100 {
            let m = Matrix::<f64, 10, 10>::from_fn(|_, _| rng.gen());

            // if rng.gen::<f32>() < 0.1 {
            //     m[1][0] = (m[0][0] * m[1][1]) / m[0][1];
            // }

            // dbg!(m);
            let i = m.gauss_jordan_invert();
            // dbg!(i);
            let d = m.determinant();

            if d == 0.0 {
                assert_eq!(i, None);
            } else {
                assert!(i.is_some());

                let n = i.unwrap() * m;
                let close_enough = n.into_iter_column_wise().zip(Matrix::<f64, 10, 10>::identity().into_iter_column_wise()).all(|(l, r)| (l - r).abs() < f32::EPSILON as f64);
                assert!(close_enough, "{}", n);
            }
        }
    }

    #[test]
    fn gauss_jordan_with_vector() {
        let m: Matrix<_, 4, 4> = matrix![
            1.0,  2.0,  4.0,  8.0,
            2.0,  0.0,  8.0,  1.0,
            4.0,  8.0,  1.0,  2.0,
            8.0,  1.0,  2.0,  4.0,
        ];

        let v = vector![5.0, 3.0, 8.0, 7.0];

        let result = m.augmented_gauss_jordan(v).1;

        dbg!(result);
        dbg!(m);
        dbg!(m * v);
        dbg!(m * result);
        assert_eq!(m * result, v);
    }

    #[test]
    fn gauss_jordan_with_matrix() {
        let m: Matrix<_, 3, 3> = matrix![
            1.0, 0.0, 1.0,
            1.0, 0.0, 2.0,
            0.0, 1.0, 0.0
        ];

        let result = m.super_augmented_gauss_jordan(Matrix::<_, 3, 3>::identity()).1;

        dbg!(result);
        dbg!(m);
        dbg!(m * result);
        assert_eq!(m * result, Matrix::<_, 3, 3>::identity());
    }
}