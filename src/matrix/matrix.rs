use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::{Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign};

use paste::paste;

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

use crate::{CastAs, ConstTest, MatrixColumnSlice, MatrixRowSlice, MatrixSlice, NotNan, Scalar, SignedScalar, True, Vector, ZeroInit};
use crate::bounds::RangeBounds;
use crate::utility::array;

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Matrix<T: 'static, const R: usize, const C: usize> {
    pub(crate) columns: [Vector<T, R>; C],
}

impl<T, const R: usize, const C: usize> Default for Matrix<T, R, C> where T: Default {
    fn default() -> Self {
        Matrix::from_fn(|_, _| T::default())
    }
}

/// Safety: Is repr(c) and single field is Zeroable
unsafe impl<T: bytemuck::Zeroable, const R: usize, const C: usize> bytemuck::Zeroable for Matrix<T, R, C> {}

/// Safety: Is repr(c) and single field is Pod
unsafe impl<T: bytemuck::Pod, const R: usize, const C: usize> bytemuck::Pod for Matrix<T, R, C> {}

/// Safety: Is repr(c) and single field is NoUninit
unsafe impl<T: bytemuck::NoUninit, const R: usize, const C: usize> bytemuck::NoUninit for Matrix<NotNan<T>, R, C> {}

impl<T: ZeroInit + Copy, const R: usize, const C: usize> ZeroInit for Matrix<T, R, C> {
    const ZERO: Self = Matrix::column_vectors([Vector::zero(); C]);
}

impl<T, const R: usize, const C: usize> Matrix<T, R, C> {
    pub const fn zero() -> Self where T: ZeroInit + Copy {
        Matrix::ZERO
    }

    pub const fn splat(value: T) -> Self where T: Copy {
        Matrix::column_vectors([Vector::splat(value); C])
    }

    #[inline(always)]
    pub const fn column_vectors(columns: [Vector<T, R>; C]) -> Self {
        Matrix {
            columns,
        }
    }

    pub fn row_vectors(rows: [Vector<T, C>; R]) -> Self where T: Copy {
        Matrix::column_vectors(rows).transpose()
    }

    pub fn columns(columns: [[T; R]; C]) -> Self {
        Matrix::column_vectors(array::map(columns, Vector::array))
    }

    pub fn rows(rows: [[T; C]; R]) -> Self where T: Copy {
        Matrix::columns(rows).transpose()
    }

    #[inline(always)]
    pub fn array_column_wise(array: [T; R * C]) -> Self {
        let ptr = &array as *const _ as *const Self;
        // Safe due to #[repr(C)] and layout guarantees of arrays
        let result = unsafe { ptr.read() };
        // forget is not needed since Scalar: Copy, so no Drop, but might help compiler optimized this out
        std::mem::forget(array);
        result
    }

    pub fn array_row_wise(array: [T; C * R]) -> Self where T: Copy {
        Matrix::<T, C, R>::array_column_wise(array).transpose()
    }

    pub fn slice_column_wise(values: &[T]) -> Self where T: Copy {
        Matrix::from_fn(|r, c| values[r + R * c])
    }

    pub fn slice_row_wise(values: &[T]) -> Self where T: Copy {
        Matrix::from_fn(|r, c| values[c + C * r])
    }

    pub fn from_fn<F>(mut f: F) -> Self where F: FnMut(usize, usize) -> T {
        Matrix::column_vectors(array::from_fn(|c| Vector::from_fn(|r| f(r, c))))
    }

    pub fn try_from_fn<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize, usize) -> Result<T, E> {
        Ok(Matrix::column_vectors(array::try_from_fn(|c| Vector::try_from_fn(|r| f(r, c)))?))
    }

    pub fn from_fn_columns<F>(mut f: F) -> Self where F: FnMut(usize) -> [T; R] {
        Matrix::columns(array::from_fn(|c| f(c)))
    }

    pub fn try_from_fn_columns<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<[T; R], E> {
        Ok(Matrix::columns(array::try_from_fn(|c| f(c))?))
    }

    pub fn from_fn_column_vectors<F>(mut f: F) -> Self where F: FnMut(usize) -> Vector<T, R> {
        Matrix::column_vectors(array::from_fn(|c| f(c)))
    }

    pub fn try_from_fn_column_vectors<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<Vector<T, R>, E> {
        Ok(Matrix::column_vectors(array::try_from_fn(|c| f(c))?))
    }

    pub fn from_iter_column_wise<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let mut iter = iter.into_iter();
        Matrix::column_vectors(array::from_fn(|_| Vector::from_fn(|_| iter.next().expect("Insufficient Elements"))))
    }

    pub fn from_iter_row_wise<I: IntoIterator<Item=T>>(iter: I) -> Self where T: Copy {
        Matrix::from_iter_column_wise(iter).transpose()
    }

    #[track_caller]
    pub fn cut_row(&self, row: usize) -> Matrix<T, { R - 1 }, C> where ConstTest<{ R > 0 }>: True, T: Copy {
        assert!(row < R, "Can not cut row outside range [0, R)");

        Matrix::from_fn(|r, c| self[if r < row { r } else { r + 1 }][c])
    }

    #[track_caller]
    pub fn cut_column(&self, column: usize) -> Matrix<T, R, { C - 1 }> where T: Copy {
        assert!(column < C, "Can not cut column outside range [0, C)");

        Matrix::from_fn(|r, c| self[r][if c < column { c } else { c + 1 }])
    }

    #[track_caller]
    pub fn cut_row_column(&self, row: usize, column: usize) -> Matrix<T, { R - 1 }, { C - 1 }> where ConstTest<{ R > 0 }>: True, ConstTest<{ C > 0 }>: True, T: Copy {
        assert!(row < R, "Can not cut row outside range [0, R)");
        assert!(column < C, "Can not cut column outside range [0, C)");

        Matrix::from_fn(|r, c| self[if r < row { r } else { r + 1 }][if c < column { c } else { c + 1 }])
    }

    pub fn push_column(self, column: Vector<T, R>) -> Matrix<T, R, { C + 1 }> {
        Matrix::column_vectors(array::join(self.columns, [column]))
    }

    pub fn push_row(self, row: Vector<T, C>) -> Matrix<T, { R + 1 }, C> {
        Matrix::column_vectors(array::map_zip(self.columns, row.into_array(), |c, v| c.push(v)))
    }

    #[must_use]
    pub fn swap_rows(mut self, a: usize, b: usize) -> Self {
        for c in 0..C {
            self.columns[c].swap_assign(a, b);
        }

        self
    }

    pub fn swap_rows_assign(&mut self, a: usize, b: usize) {
        for c in 0..C {
            self.columns[c].swap_assign(a, b);
        }
    }

    #[must_use]
    pub fn swap_columns(mut self, a: usize, b: usize) -> Self {
        self.columns.swap(a, b);

        self
    }

    pub fn swap_columns_assign(&mut self, a: usize, b: usize) {
        self.columns.swap(a, b);
    }

    pub fn map<V: Scalar, F: FnMut(T) -> V>(self, mut map: F) -> Matrix<V, R, C> {
        Matrix::column_vectors(array::map(self.columns, |c| c.map(|v| map(v))))
    }

    pub fn try_map<V: Scalar, E, F: FnMut(T) -> Result<V, E>>(self, mut map: F) -> Result<Matrix<V, R, C>, E> {
        Ok(Matrix::column_vectors(array::try_map(self.columns, |c| c.try_map(|v| map(v)))?))
    }

    pub fn zip_map<U: Scalar, V: Scalar, F: FnMut(T, V) -> U>(self, other: Matrix<V, R, C>, mut map: F) -> Matrix<U, R, C> {
        Matrix::from_iter_column_wise(self.into_iter_column_wise().zip(other.into_iter_column_wise()).map(|(u, v)| map(u, v)))
    }

    pub fn cast_as<V: Scalar>(self) -> Matrix<V, R, C> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }

    pub fn comp_into<V: Scalar>(self) -> Matrix<V, R, C> where T: Into<V> {
        self.map(|v| v.into())
    }

    pub fn comp_try_into<V: Scalar, E>(self) -> Result<Matrix<V, R, C>, E> where T: TryInto<V, Error = E> {
        self.try_map(|v| v.try_into())
    }

    pub fn as_columns(&self) -> &[[T; R]; C] {
        unsafe { std::mem::transmute(&self.columns) }
    }

    pub fn as_columns_mut(&mut self) -> &mut [[T; R]; C] {
        unsafe { std::mem::transmute(&mut self.columns) }
    }

    pub fn as_column_vectors(&self) -> &[Vector<T, R>; C] {
        &self.columns
    }

    pub fn as_column_vectors_mut(&mut self) -> &mut [Vector<T, R>; C] {
        &mut self.columns
    }

    pub fn as_array_column_wise(&self) -> &[T; R * C] {
        // Safe due to #[repr(C)] and layout guarantees of arrays
        unsafe { std::mem::transmute(self) }
    }

    pub fn as_array_column_wise_mut(&mut self) -> &mut [T; R * C] {
        // Safe due to #[repr(C)] and layout guarantees of arrays
        unsafe { std::mem::transmute(self) }
    }

    pub fn as_slice_column_wise(&self) -> &[T] {
        unsafe { core::slice::from_raw_parts(&self[0][0] as *const T, R * C) }
    }

    pub fn as_slice_column_wise_mut(&mut self) -> &mut [T] {
        unsafe { core::slice::from_raw_parts_mut(&mut self[0][0] as *mut T, R * C) }
    }

    pub fn into_arrays(self) -> [[T; R]; C] {
        array::map(self.columns, Vector::into_array)
    }

    pub fn into_array_column_wise(self) -> [T; R * C] {
        let ptr = &self as *const _ as *const [T; R * C];
        // Safe due to #[repr(C)] and layout guarantees of arrays
        let result = unsafe { ptr.read() };
        // forget is not needed since Scalar: Copy, so no Drop, but might help compiler optimized this out
        std::mem::forget(self);
        result
    }

    pub fn column_vector(&self, column: usize) -> Vector<T, R> where T: Copy {
        self.columns[column]
    }

    pub fn row_vector(&self, row: usize) -> Vector<T, C> where T: Copy {
        Vector::from_fn(|c| self[row][c])
    }

    pub fn into_column_vectors(self) -> [Vector<T, R>; C] {
        self.columns
    }

    pub fn into_row_vectors(self) -> [Vector<T, C>; R] where T: Copy {
        self.transpose().columns
    }

    pub fn into_iter_column_wise(self) -> impl Iterator<Item = T> {
        self.columns.into_iter().flat_map(|column| column.into_iter())
    }

    pub fn iter_column_wise(&self) -> impl Iterator<Item = &T> {
        self.columns.iter().flat_map(|column| column.iter())
    }

    pub fn iter_column_wise_mut(&mut self) -> impl Iterator<Item = &mut T> {
        self.columns.iter_mut().flat_map(|column| column.iter_mut())
    }
}

impl<T: Scalar> Matrix<T, 3, 3> {
    pub fn with_w(self, w: T) -> Matrix<T, 4, 4> {
        self.push_row(Vector::<T, 3>::zero())
            .push_column(Vector::<T, 3>::zero().with_w(w))
    }
}

impl<T: Scalar, const R: usize, const C: usize> Matrix<T, R, C> {
    /// Can also use & operator
    pub fn comp_mul(self, rhs: Self) -> Self {
        self & rhs
    }

    /// Can also use | operator
    pub fn comp_div(self, rhs: Self) -> Self {
        self | rhs
    }
}

impl<T: Scalar, const N: usize> Matrix<T, N, N> {
    pub fn identity() -> Self {
        let mut identity = Self::ZERO;
        for i in 0..N {
            identity[i][i] = T::ONE;
        }
        identity
    }

    pub fn diagonal_vector(v: Vector<T, N>) -> Self {
        Self::diagonal(v.into_array())
    }

    pub fn diagonal(v: [T; N]) -> Self {
        Self::diagonal_iter(v)
    }

    pub fn diagonal_iter<I>(iter: I) -> Self where I: IntoIterator<Item=T> {
        let mut result = Self::ZERO;
        let mut iter = iter.into_iter();
        for i in 0..N {
            result[i][i] = iter.next().expect("Insufficient Elements");
        }
        result
    }
}

impl<T: Scalar, const R: usize> Matrix<T, R, 1> {
    pub fn into_vector(self) -> Vector<T, R> {
        self.columns[0]
    }
}

impl<T: Scalar, const R: usize, const C: usize> From<[[T; R]; C]> for Matrix<T, R, C> {
    fn from(columns: [[T; R]; C]) -> Self {
        Matrix::columns(columns)
    }
}

impl<T: Scalar, const R: usize, const C: usize> From<[Vector<T, R>; C]> for Matrix<T, R, C> {
    fn from(columns: [Vector<T, R>; C]) -> Self {
        Matrix::column_vectors(columns)
    }
}

// Addition

impl<T: Scalar, const R: usize, const C: usize> Add for Matrix<T, R, C> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        Matrix::from_fn(|r, c| self[r][c] + rhs[r][c])
    }
}

impl<T: Scalar, const R: usize, const C: usize> AddAssign for Matrix<T, R, C> {
    fn add_assign(&mut self, rhs: Self) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] += rhs[r][c];
            }
        }
    }
}

// Subtraction

impl<T: Scalar, const R: usize, const C: usize> Sub for Matrix<T, R, C> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        Matrix::from_fn(|r, c| self[r][c] - rhs[r][c])
    }
}

impl<T: Scalar, const R: usize, const C: usize> SubAssign for Matrix<T, R, C> {
    fn sub_assign(&mut self, rhs: Self) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] -= rhs[r][c];
            }
        }
    }
}

// Multiplication

impl<T: Scalar, const R: usize, const K: usize, const C: usize> Mul<Matrix<T, K, C>> for Matrix<T, R, K> {
    type Output = Matrix<T, R, C>;

    fn mul(self, rhs: Matrix<T, K, C>) -> Self::Output {
        let mut result = Matrix::ZERO;
        for c in 0..C {
            result.as_column_vectors_mut()[c] = self * rhs.column_vector(c)
        }
        result
    }
}

impl<T: Scalar, const N: usize> MulAssign<Matrix<T, N, N>> for Matrix<T, N, N> {
    fn mul_assign(&mut self, rhs: Matrix<T, N, N>) {
        *self = *self * rhs;
    }
}

impl<T: Scalar, const R: usize, const C: usize> Mul<Vector<T, C>> for Matrix<T, R, C> {
    type Output = Vector<T, R>;

    fn mul(self, rhs: Vector<T, C>) -> Self::Output {
        let mut result = Vector::ZERO;
        for c in 0..C {
            result += self.as_column_vectors()[c] * rhs[c];
        }
        result
    }
}

impl<T: Scalar, const R: usize, const C: usize> Mul<Matrix<T, R, C>> for Vector<T, R> {
    type Output = Vector<T, C>;

    fn mul(self, rhs: Matrix<T, R, C>) -> Self::Output {
        Vector::from_fn(|c| self * rhs.columns[c])
    }
}

impl<T: Scalar, const R: usize, const C: usize> Mul<T> for Matrix<T, R, C> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Matrix::from_fn(|r, c| self[r][c] * rhs)
    }
}

impl<T: Scalar, const R: usize, const C: usize> MulAssign<T> for Matrix<T, R, C> {
    fn mul_assign(&mut self, rhs: T) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] *= rhs;
            }
        }
    }
}

macro_rules! matrix_pre_scale {
    ($($t:ty),+) => {
        $(impl<const R: usize, const C: usize> Mul<Matrix<$t, R, C>> for $t {
            type Output = Matrix<$t, R, C>;

            fn mul(self, rhs: Matrix<$t, R, C>) -> Matrix<$t, R, C> {
                Matrix::splat(self) & rhs
            }
        })
        *
    };
}

matrix_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64, NotNan<f32>, NotNan<f64>);

impl<T: Scalar, const R: usize, const C: usize> BitAnd<Matrix<T, R, C>> for Matrix<T, R, C> {
    type Output = Matrix<T, R, C>;

    fn bitand(self, rhs: Matrix<T, R, C>) -> Self::Output {
        self.zip_map(rhs, |l, r| l * r)
    }
}

impl<T: Scalar, const R: usize, const C: usize> BitAndAssign<Matrix<T, R, C>> for Matrix<T, R, C> {
    fn bitand_assign(&mut self, rhs: Matrix<T, R, C>) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] *= rhs[r][c];
            }
        }
    }
}

// Division

impl<T: Scalar, const R: usize, const C: usize> Div<T> for Matrix<T, R, C> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Matrix::from_fn(|r, c| self[r][c] / rhs)
    }
}

impl<T: Scalar, const R: usize, const C: usize> DivAssign<T> for Matrix<T, R, C> {
    fn div_assign(&mut self, rhs: T) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] /= rhs;
            }
        }
    }
}

impl<T: Scalar, const R: usize, const C: usize> BitOr<Matrix<T, R, C>> for Matrix<T, R, C> {
    type Output = Matrix<T, R, C>;

    fn bitor(self, rhs: Matrix<T, R, C>) -> Self::Output {
        self.zip_map(rhs, |l, r| l / r)
    }
}

impl<T: Scalar, const R: usize, const C: usize> BitOrAssign<Matrix<T, R, C>> for Matrix<T, R, C> {
    fn bitor_assign(&mut self, rhs: Matrix<T, R, C>) {
        for c in 0..C {
            for r in 0..R {
                self[r][c] /= rhs[r][c];
            }
        }
    }
}

macro_rules! matrix_pre_recip {
    ($($t:ty),+) => {
        $(impl<const R: usize, const C: usize> BitOr<Matrix<$t, R, C>> for $t {
            type Output = Matrix<$t, R, C>;

            fn bitor(self, rhs: Matrix<$t, R, C>) -> Matrix<$t, R, C> {
                Matrix::splat(self) | rhs
            }
        })
        *
    };
}

matrix_pre_recip!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64, NotNan<f32>, NotNan<f64>);

// Negation

impl<T: SignedScalar, const R: usize, const C: usize> Neg for Matrix<T, R, C> {
    type Output = Self;

    fn neg(self) -> Self {
        Matrix::from_fn(|r, c| -self[r][c])
    }
}

// Indexing

pub struct MatrixPartialIndex<T, const R: usize, const C: usize> {
    values: [T],
}

impl<T, const R: usize, const C: usize> MatrixPartialIndex<T, R, C> {
    fn as_slice(&self) -> &MatrixPartialSlice<T, R, C> {
        unsafe { std::mem::transmute(self) }
    }

    fn as_slice_mut(&mut self) -> &mut MatrixPartialSlice<T, R, C> {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T, const R: usize, const C: usize> Index<usize> for Matrix<T, R, C> {
    type Output = MatrixPartialIndex<T, R, C>;

    fn index(&self, r: usize) -> &MatrixPartialIndex<T, R, C> {
        unsafe { core::mem::transmute(core::slice::from_raw_parts(&self.columns[0][r] as *const T, (C - 1) * R + 1)) }
    }
}

impl<T, const R: usize, const C: usize> Index<usize> for MatrixPartialIndex<T, R, C> {
    type Output = T;

    fn index(&self, c: usize) -> &T {
        &self.values[c * R]
    }
}

impl<T, const R: usize, const C: usize> IndexMut<usize> for Matrix<T, R, C> {
    fn index_mut(&mut self, r: usize) -> &mut MatrixPartialIndex<T, R, C> {
        unsafe { core::mem::transmute(core::slice::from_raw_parts_mut(&mut self.columns[0][r] as *mut T, (C - 1) * R + 1)) }
    }
}

impl<T, const R: usize, const C: usize> IndexMut<usize> for MatrixPartialIndex<T, R, C> {
    fn index_mut(&mut self, c: usize) -> &mut T {
        &mut self.values[c * R]
    }
}

impl<T, I, const R: usize, const C: usize> Index<I> for MatrixPartialIndex<T, R, C> where I: RangeBounds<usize> {
    type Output = MatrixRowSlice<T, R, C>;

    fn index(&self, index: I) -> &MatrixRowSlice<T, R, C> {
        unsafe { std::mem::transmute(&self.as_slice()[index]) }
    }
}

impl<T, I, const R: usize, const C: usize> IndexMut<I> for MatrixPartialIndex<T, R, C> where I: RangeBounds<usize> {
    fn index_mut(&mut self, index: I) -> &mut MatrixRowSlice<T, R, C> {
        unsafe { std::mem::transmute(&mut self.as_slice_mut()[index]) }
    }
}

#[cfg(test)]
mod partial_index_tests {
    use super::*;

    #[test]
    fn test() {
        let m = Matrix4d::from_iter_column_wise((0..16).map(|i| i as f64));

        let _a = &m[0];
        let _b = &m[1];

        assert_eq!(0.0, m[0][0]);
        assert_eq!(1.0, m[1][0]);
        assert_eq!(2.0, m[2][0]);
        assert_eq!(3.0, m[3][0]);
        assert_eq!(4.0, m[0][1]);
        assert_eq!(8.0, m[0][2]);
        assert_eq!(12.0, m[0][3]);
        assert_eq!(13.0, m[1][3]);
        assert_eq!(14.0, m[2][3]);
        assert_eq!(15.0, m[3][3]);

        let mut n = Matrix4d::from_iter_column_wise((0..16).map(|i| i as f64));

        n[3][2] = -0.5;
        n[0][1] = 7.5;

        assert_eq!(-0.5, n[3][2]);
        assert_eq!(7.5, n[0][1]);

        let t = &m[1][1..4];
        let _t: Matrix<_, 1, 3> = t.row_matrix();
        let _t: Vector<_, 3> = m[1][1..4].vector();

        let t = &m[1..4][1];
        let _t: Matrix<_, 3, 1> = t.column_matrix();
        let _t: Vector<_, 3> = m[1..4][1].vector();
    }
}

// Slicing

pub struct MatrixPartialSlice<T, const R: usize, const C: usize> {
    values: [T],
}

impl<T, I, const R: usize, const C: usize> Index<I> for Matrix<T, R, C> where I: RangeBounds<usize> {
    type Output = MatrixPartialSlice<T, R, C>;

    #[track_caller]
    fn index(&self, index: I) -> &MatrixPartialSlice<T, R, C> {
        let start_row = index.start();
        let end_row = index.end(R);

        assert!(start_row < end_row, "Row range must be > 0");
        assert!(end_row <= R, "Row index out of bounds");

        let row_len = end_row - start_row;

        let total_len = (C - 1) * R + row_len;

        unsafe { core::mem::transmute(core::slice::from_raw_parts(&self.columns[0][start_row] as *const T, total_len)) }
    }
}

impl<T, I, const R: usize, const C: usize> Index<I> for MatrixPartialSlice<T, R, C> where I: RangeBounds<usize> {
    type Output = MatrixSlice<T, R, C>;

    #[track_caller]
    fn index(&self, index: I) -> &MatrixSlice<T, R, C> {
        let start_col = index.start();
        let end_col = index.end(C);

        assert!(start_col < end_col, "Column range must be > 0");
        assert!(end_col <= C, "Column index out of bounds");

        let col_len = end_col - start_col;

        let total_len = self.values.len() - (C - col_len) * R;

        unsafe { core::mem::transmute(core::slice::from_raw_parts(&self.values[start_col * R] as *const T, total_len)) }
    }
}

impl<T, I, const R: usize, const C: usize> IndexMut<I> for Matrix<T, R, C> where I: RangeBounds<usize> {
    #[track_caller]
    fn index_mut(&mut self, index: I) -> &mut MatrixPartialSlice<T, R, C> {
        let start_row = index.start();
        let end_row = index.end(R);

        assert!(start_row < end_row, "Row range must be > 0");
        assert!(end_row <= R, "Row index out of bounds");

        let row_len = end_row - start_row;

        let total_len = (C - 1) * R + row_len;

        unsafe { core::mem::transmute(core::slice::from_raw_parts_mut(&mut self.columns[0][start_row] as *mut T, total_len)) }
    }
}

impl<T, I, const R: usize, const C: usize> IndexMut<I> for MatrixPartialSlice<T, R, C> where I: RangeBounds<usize> {
    #[track_caller]
    fn index_mut(&mut self, index: I) -> &mut MatrixSlice<T, R, C> {
        let start_col = index.start();
        let end_col = index.end(C);

        assert!(start_col < end_col, "Column range must be > 0");
        assert!(end_col <= C, "Column index out of bounds");

        let col_len = end_col - start_col;

        let total_len = self.values.len() - (C - col_len) * R;

        unsafe { core::mem::transmute(core::slice::from_raw_parts_mut(&mut self.values[start_col * R] as *mut T, total_len)) }
    }
}

impl<T, const R: usize, const C: usize> Index<usize> for MatrixPartialSlice<T, R, C> {
    type Output = MatrixColumnSlice<T, R, C>;

    fn index(&self, c: usize) -> &MatrixColumnSlice<T, R, C> {
        unsafe { std::mem::transmute(&self[c..=c]) }
    }
}

impl<T, const R: usize, const C: usize> IndexMut<usize> for MatrixPartialSlice<T, R, C> {
    fn index_mut(&mut self, c: usize) -> &mut MatrixColumnSlice<T, R, C> {
        unsafe { std::mem::transmute(&mut self[c..=c]) }
    }
}

macro_rules! matrix_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            pub type [<$nick_base $size>]<T> = Matrix<T, $size, $size>;

            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

matrix_nicks!(Matrix, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
matrix_nicks!(Matrix, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
matrix_nicks!(Matrix, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
matrix_nicks!(Matrix, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);

macro_rules! matrix_nicks_rect {
    ($nick_base:ident, $row:literal, $col:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),* ; $($cols:literal),+ ) => {
        paste! {

            impl<T> Matrix<T, $row, $col> {
                pub fn new($( [< column_ $cols >]: Vector<T, $row> ),+) -> Self {
                    Self::column_vectors( [ $( [< column_ $cols >] ),+ ] )
                }
            }

            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col>]<T> = Matrix<T, $row, $col>;

            $(

            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col $suffix>] = [<$nick_base $row x $col>]<$var>;)
            *

            $(
            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col $exp_var>] = [<$nick_base $row x $col>]<$exp_var>;)
            *
        }
    };
}

matrix_nicks_rect!(Matrix, 1, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0);
matrix_nicks_rect!(Matrix, 2, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0);
matrix_nicks_rect!(Matrix, 3, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0);
matrix_nicks_rect!(Matrix, 4, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0);
matrix_nicks_rect!(Matrix, 1, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1);
matrix_nicks_rect!(Matrix, 2, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1);
matrix_nicks_rect!(Matrix, 3, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1);
matrix_nicks_rect!(Matrix, 4, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1);
matrix_nicks_rect!(Matrix, 1, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2);
matrix_nicks_rect!(Matrix, 2, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2);
matrix_nicks_rect!(Matrix, 3, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2);
matrix_nicks_rect!(Matrix, 4, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2);
matrix_nicks_rect!(Matrix, 1, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2, 3);
matrix_nicks_rect!(Matrix, 2, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2, 3);
matrix_nicks_rect!(Matrix, 3, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2, 3);
matrix_nicks_rect!(Matrix, 4, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, f32, f64 ; 0, 1, 2, 3);

impl<T, const R: usize, const C: usize> Debug for Matrix<T, R, C> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\n")?;

        let column_widths: [_; C] = array::from_fn(|c| (0..R).map(|r| format!("{:?}", self[r][c]).len()).max().unwrap());

        for r in 0..R {
            write!(f, "[")?;
            for c in 0..C {
                if c != 0 {
                    write!(f, "  ")?;
                }
                let s = format!("{:?}", self[r][c]);
                write!(f, "{}{}", s, " ".repeat(column_widths[c] - s.len()))?;
            }
            writeln!(f, "]")?;
        }

        Ok(())
    }
}

impl<T, const R: usize, const C: usize> Display for Matrix<T, R, C> where T: Display {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "\n")?;

        let column_widths: [_; C] = array::from_fn(|c| (0..R).map(|r| format!("{}", self[r][c]).len()).max().unwrap());

        for r in 0..R {
            write!(f, "[")?;
            for c in 0..C {
                if c != 0 {
                    write!(f, "  ")?;
                }
                let s = format!("{}", self[r][c]);
                write!(f, "{}{}", s, " ".repeat(column_widths[c] - s.len()))?;
            }
            writeln!(f, "]")?;
        }

        Ok(())
    }
}