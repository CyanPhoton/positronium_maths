use std::fmt::Display;

use paste::paste;

use crate::{Matrix, Matrix3, Matrix4, Radians, FloatScalar, Scalar, SignedScalar, UnitVector, UnitVector3, Vector, Vector2, Vector3, Vector4};
use crate::utility::array;

impl<T: Scalar, const N: usize> Matrix<T, N, N> {
    pub fn scale(factor: T) -> Self {
        Self::identity() * factor
    }

    pub fn scale_comp_vector(factors: Vector<T, N>) -> Self {
        Self::diagonal_vector(factors)
    }

    pub fn scale_comp(factors: [T; N]) -> Self {
        Self::diagonal(factors)
    }
}

impl<T: FloatScalar, const N: usize> Matrix<T, N, N> {
    pub fn rotate_plane<R>(radians: R, plane: (usize, usize)) -> Self where R: Into<Radians<T>> {
        let radians = radians.into();
        let (sin, cos) = radians.sin_cos();

        Self::columns(array::from_fn(|c| {
            if c == plane.0 {
                array::from_fn(|r| if r == plane.0 {
                    cos
                } else if r == plane.1 {
                    sin
                } else {
                    T::ZERO
                })
            } else if c == plane.1 {
                array::from_fn(|r| if r == plane.0 {
                    -sin
                } else if r == plane.1 {
                    cos
                } else {
                    T::ZERO
                })
            } else {
                array::from_fn(|r| if r == c { T::ONE } else { T::ZERO })
            }
        }))
    }
}

macro_rules! rotate_specialise {
    ($size:literal, $x_plane:literal, $y_plane:literal $(,$suffix:ident)+) => {
        paste! {
            impl<T: FloatScalar> Matrix<T, $size, $size> {
                $(
                pub fn [< rotate _ $suffix >]<R>(radians: R) -> Self where R: Into<Radians<T>> {
                    Self::rotate_plane(radians, ($x_plane, $y_plane))
                }
                )+
            }
        }
    };
    ($size:literal, $x_plane:literal, $y_plane:literal) => {
        impl<T: FloatScalar> Matrix<T, $size, $size> {
            pub fn rotate<R>(radians: R) -> Self where R: Into<Radians<T>> {
                Self::rotate_plane(radians, ($x_plane, $y_plane))
            }
        }
    };
}

rotate_specialise!(2, 0, 1);
rotate_specialise!(3, 0, 1, xy, z);
rotate_specialise!(3, 1, 2, yz, x);
rotate_specialise!(3, 2, 0, zx, y);
rotate_specialise!(4, 0, 1, xy, z);
rotate_specialise!(4, 1, 2, yz, x);
rotate_specialise!(4, 2, 0, zx, y);

impl<T: FloatScalar> Matrix3<T> {
    // https://en.wikipedia.org/wiki/Rotation_matrix#General_rotations
    pub fn rotate_axis<R>(angle_radians: R, axis: UnitVector<T, 3>) -> Matrix3<T> where R: Into<Radians<T>> {
        let axis = axis.into_vector();
        let angle_radians = angle_radians.into();
        let (s, c) = angle_radians.sin_cos();

        let u_x = Matrix3::column_vectors(array::from_fn(|c| axis ^ Vector::<T, 3>::unit_n(c)));
        let cross_matrix = Matrix::column_vectors([axis]) * Matrix::row_vectors([axis]);

        Self::identity() * c + u_x * s + cross_matrix * (T::ONE - c)
    }
}

impl<T: Scalar> Matrix3<T> {
    pub fn to_homogeneous(self, ww: T) -> Matrix4<T> {
        let [c0, c1, c2] = self.into_column_vectors().map(|v| v.push(T::ZERO));

        let mut c3 = Vector::zero();
        c3[3] = ww;

        Matrix4::new(c0, c1, c2, c3)
    }

    #[cfg(feature = "simd")]
    pub fn to_homogeneous_simd(self, ww: T) -> crate::simd::SimdMatrix<T, 4, 4> where T: crate::SimdScalar {
        let [c0, c1, c2] = self.into_column_vectors().map(|v| v.with_w_simd(T::ZERO));

        let mut c3 = crate::simd::SimdVector::zero();
        c3[3] = ww;

        crate::simd::SimdMatrix::<T, 4, 4>::new(c0, c1, c2, c3)
    }
}

impl<T: Scalar> Vector3<T> {
    pub fn to_homogeneous(self, w: T) -> Vector4<T> {
        self.push(w)
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum ControlAxis {
    Vertical,
    Horizontal,
    MinAxis,
    MaxAxis,
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum ForwardDirection {
    PositiveZ,
    NegativeZ
}

impl ForwardDirection {
    pub const OPEN_GL: Self = Self::NegativeZ;
    pub const VULKAN: Self = Self::PositiveZ;

    pub fn sign<T: SignedScalar>(self) -> T {
        match self {
            ForwardDirection::PositiveZ => T::ONE,
            ForwardDirection::NegativeZ => T::NEGATIVE_ONE,
        }
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum RightDirection {
    PositiveX,
    NegativeX
}

impl RightDirection {
    pub const OPEN_GL: Self = Self::PositiveX;
    pub const VULKAN: Self = Self::PositiveX;

    pub fn sign<T: SignedScalar>(self) -> T {
        match self {
            RightDirection::PositiveX => T::ONE,
            RightDirection::NegativeX => T::NEGATIVE_ONE,
        }
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum UpDirection {
    PositiveY,
    NegativeY
}

impl UpDirection {
    pub const OPEN_GL: Self = Self::PositiveY;
    pub const VULKAN: Self = Self::NegativeY;

    pub fn sign<T: SignedScalar>(self) -> T {
        match self {
            UpDirection::PositiveY => T::ONE,
            UpDirection::NegativeY => T::NEGATIVE_ONE,
        }
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct NdcConfig {
    pub right_dir: RightDirection,
    pub up_dir: UpDirection,
    pub forward_dir: ForwardDirection,
}

impl NdcConfig {
    pub const OPEN_GL: Self = NdcConfig {
        right_dir: RightDirection::OPEN_GL,
        up_dir: UpDirection::OPEN_GL,
        forward_dir: ForwardDirection::OPEN_GL,
    };
    pub const VULKAN: Self = NdcConfig {
        right_dir: RightDirection::VULKAN,
        up_dir: UpDirection::VULKAN,
        forward_dir: ForwardDirection::VULKAN,
    };

    pub fn sign_vector<T: SignedScalar>(self) -> Vector3<T> {
        Vector3::new(
            self.right_dir.sign(),
            self.up_dir.sign(),
            self.forward_dir.sign()
        )
    }
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub enum DepthDomain<T> {
    Finite { near: T, far: T },
    Infinite { near: T },
}

#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Debug)]
pub struct DepthRange<T> {
    pub near: T,
    pub far: T,
}

impl<T: FloatScalar> DepthRange<T> {
    pub const OPEN_GL: Self = DepthRange { near: T::ONE, far: T::NEGATIVE_ONE };
    pub const VULKAN: Self = DepthRange { near: T::ZERO, far: T::ONE };
}

impl<T: Scalar> Matrix4<T> {
    pub fn space_scale(factor: T) -> Self {
        Matrix3::scale(factor).to_homogeneous(T::ONE)
    }

    pub fn space_scale_comp_vector(factors: Vector3<T>) -> Self {
        Matrix3::scale_comp_vector(factors).to_homogeneous(T::ONE)
    }

    pub fn space_scale_comp(factors: [T; 3]) -> Self {
        Matrix3::scale_comp(factors).to_homogeneous(T::ONE)
    }

    pub fn space_translate(translation: Vector3<T>) -> Self {
        Self::new(Vector4::unit_x(), Vector4::unit_y(), Vector4::unit_z(), translation.push(T::ONE))
    }

    pub fn space_rotate_axis<R>(angle_radians: R, axis: UnitVector3<T>) -> Self where T: FloatScalar, R: Into<Radians<T>> {
        Matrix3::rotate_axis(angle_radians, axis).to_homogeneous(T::ONE)
    }

    pub fn space_look_at(eye: Vector3<T>, target: Vector3<T>, up: UnitVector3<T>, ndc: NdcConfig) -> Self where T: FloatScalar + Display {
        let signs = ndc.sign_vector::<T>();

        let dir = (target - eye).unit();
        let new_up = up.into_vector().project_to_plane(dir).unit().into_vector();
        let dir = dir.into_vector();
        let side = dir ^ new_up;

        let offset = Self::space_translate(-target); // Moves target to origin
        let rotate = Matrix3::row_vectors([side * signs.x, new_up * signs.y, dir * signs.z]).to_homogeneous(T::ONE); // Rotates about origin
        let translate = Self::space_translate((Vector3::unit_z() * signs.z) * (target - eye).norm()); // Moves camera backwards

        translate * rotate * offset
    }

    pub fn projection<R>(camera_fov: R, camera_size: T, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar, R: Into<Radians<T>> {
        let two = T::ONE + T::ONE;

        let fov = camera_fov.into();
        let fov_factor = (fov / two).tan();
        let half_size = camera_size.div(two);

        let (k_x, k_y) = match control_axis {
            ControlAxis::Vertical => {
                // k_y = k_0, k_x = k_1 = a^-1 = w_0 / w_1 = w_y / w_x = (W / H)^-1
                (aspect_ratio.recip(), T::ONE)
            }
            ControlAxis::Horizontal => {
                // k_x = k_0, k_y = k_1 = a^-1 = w_0 / w_1 = w_x / w_y = (W / H)
                (T::ONE, aspect_ratio)
            }
            ControlAxis::MinAxis => if aspect_ratio >= T::ONE {
                // (W / H) >= 1 => H <= W, so use Vertical Axis Rule
                (aspect_ratio.recip(), T::ONE)
            } else {
                // (W / H) < 1 => W < H, so use Horizontal Axis Rule
                (T::ONE, aspect_ratio)
            },
            ControlAxis::MaxAxis => if aspect_ratio >= T::ONE {
                // (W / H) >= 1 => W >= H, so use Horizontal Axis Rule
                (T::ONE, aspect_ratio)
            } else {
                // (W / H) < 1 => H > W, so use Vertical Axis Rule
                (aspect_ratio.recip(), T::ONE)
            }
        };

        let depth_factors = match depth_domain {
            DepthDomain::Finite { near, far } => {
                let denom = far - near;
                Matrix::rows([
                    [depth_range.far * far - depth_range.near * near, depth_range.far - depth_range.near],
                    [near * far * (depth_range.near - depth_range.far), depth_range.near * far - depth_range.far * near],
                ]) / denom
            }
            DepthDomain::Infinite { near } => {
                Matrix::rows([
                    [depth_range.far, T::ZERO],
                    [near * (depth_range.near - depth_range.far), depth_range.near]
                ])
            }
        };

        let s: T = forward_dir.sign::<T>();
        let z_components = depth_factors * Vector2::new(fov_factor, half_size);

        Matrix::rows([
            [k_x, T::ZERO, T::ZERO, T::ZERO],
            [T::ZERO, k_y, T::ZERO, T::ZERO],
            [T::ZERO, T::ZERO, z_components.x, s * z_components.y],
            [T::ZERO, T::ZERO, s * fov_factor, half_size],
        ])
    }

    pub fn perspective_projection<R>(camera_fov: R, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar, R: Into<Radians<T>> {
        Self::projection(camera_fov, T::ZERO, aspect_ratio, control_axis, depth_domain, forward_dir, depth_range)
    }

    pub fn orthogonal_projection(camera_size: T, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar {
        Self::projection(Radians::new(T::ZERO), camera_size, aspect_ratio, control_axis, depth_domain, forward_dir, depth_range)
    }
}

#[cfg(test)]
mod tests {
    use crate::*;

    #[test]
    fn it_works() {
        let v = Vector2d::unit_x();
        assert!((Vector2d::unit_y() - Matrix2d::rotate(std::f64::consts::PI / 2.0) * v).norm() < 1e-10);
        assert!((-Vector2d::unit_x() - Matrix2d::rotate(std::f64::consts::PI) * v).norm() < 1e-10);
        assert!((-Vector2d::unit_y() - Matrix2d::rotate(3.0 * std::f64::consts::PI / 2.0) * v).norm() < 1e-10);
        assert!((Vector2d::unit_x() - Matrix2d::rotate(2.0 * std::f64::consts::PI) * v).norm() < 1e-10);

        println!("Test 1 = {}", Matrix2d::rotate(Degrees::new(90.0)));
        // println!("Test 2 = {}", Matrix2d::rotate_plane(Degrees::new(90.0), (0, 1)));

        println!("Test 3 = {}", Matrix2d::rotate(Degrees::new(-90.0)));
        // println!("Test 4 = {}", Matrix2d::rotate_plane(Degrees::new(90.0), (1, 0)));

        println!("Test 5 = {}", Matrix3d::rotate_x(Degrees::new(90.0)));
        println!("Test 6 = {}", Matrix3d::rotate_y(Degrees::new(90.0)));
        println!("Test 7 = {}", Matrix3d::rotate_z(Degrees::new(90.0)));

        println!("Test 8 = {}", Matrix4d::rotate_x(Degrees::new(90.0)));
        println!("Test 9 = {}", Matrix4d::rotate_y(Degrees::new(90.0)));
        println!("Test 10 = {}", Matrix4d::rotate_z(Degrees::new(90.0)));

        println!("{} vs {}", Matrix3f::rotate_x(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_x()));
        assert_eq!(Matrix3f::rotate_x(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_x()));
        println!("{} vs {}", Matrix3f::rotate_y(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_y()));
        assert_eq!(Matrix3f::rotate_y(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_y()));
        println!("{} vs {}", Matrix3f::rotate_z(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_z()));
        assert_eq!(Matrix3f::rotate_z(1.0), Matrix3f::rotate_axis(1.0, UnitVector3::unit_z()));

        let e = Vector3f::new(-1.0, -2.0, 3.0);
        let look_at = Matrix4f::space_look_at(e, Vector3f::zero(), -UnitVector3f::unit_y(), NdcConfig::OPEN_GL);
        println!("OpenGL look at = {}", look_at);

        let look_at = Matrix4f::space_look_at(e, Vector3f::zero(), -UnitVector3f::unit_y(), NdcConfig::VULKAN);
        println!("Vulkan look at = {}", look_at);

        println!("{} vs {}", (look_at * Vector4f::zero_point()).space(), Vector3f::unit_z() * e.norm());
    }

    #[test]
    fn projection() {
        let aspect = 16.0 / 9.0;

        let m = Matrix4f::projection(Degrees::new(90.0), 0.0, aspect, ControlAxis::Vertical, DepthDomain::Finite { near: 0.01, far: 100.0 }, ForwardDirection::OPEN_GL, DepthRange::OPEN_GL);
        dbg!(m);
        dbg!(m * Vector4f::new(0.0, 0.0, -5.0, 1.0));
        dbg!((m * Vector4f::new(0.0, 0.0, -0.01, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -0.1, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -5.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -10.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -100.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(aspect, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(-aspect, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 1.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, -1.0, -1.0, 1.0)).perspective_division());

        let m = Matrix4f::projection(Degrees::new(90.0), 0.0, aspect, ControlAxis::Vertical, DepthDomain::Infinite { near: 0.01 }, ForwardDirection::OPEN_GL, DepthRange::OPEN_GL);
        dbg!(m);
        dbg!(m * Vector4f::new(0.0, 0.0, -5.0, 1.0));
        dbg!((m * Vector4f::new(0.0, 0.0, -0.01, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -0.1, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -5.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -10.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -100.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, -100000000.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(aspect, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(-aspect, 0.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 1.0, -1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, -1.0, -1.0, 1.0)).perspective_division());

        let m = Matrix4f::projection(Degrees::new(90.0), 0.0, aspect, ControlAxis::Vertical, DepthDomain::Finite { near: 0.01, far: 100.0 }, ForwardDirection::VULKAN, DepthRange::VULKAN);
        dbg!(m);
        dbg!(m * Vector4f::new(0.0, 0.0, 5.0, 1.0));
        dbg!((m * Vector4f::new(0.0, 0.0, 0.01, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.1, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 5.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 10.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 100.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(aspect, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(-aspect, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 1.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, -1.0, 1.0, 1.0)).perspective_division());

        let m = Matrix4f::projection(Degrees::new(90.0), 0.0, aspect, ControlAxis::Vertical, DepthDomain::Infinite { near: 0.01 }, ForwardDirection::VULKAN, DepthRange::VULKAN);
        dbg!(m);
        dbg!(m * Vector4f::new(0.0, 0.0, 5.0, 1.0));
        dbg!((m * Vector4f::new(0.0, 0.0, 0.01, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.02, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 0.1, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 5.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 10.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 100.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 0.0, 100000000.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(aspect, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(-aspect, 0.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, 1.0, 1.0, 1.0)).perspective_division());
        dbg!((m * Vector4f::new(0.0, -1.0, 1.0, 1.0)).perspective_division());
    }
}