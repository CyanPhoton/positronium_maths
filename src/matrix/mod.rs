pub use matrix::*;
pub use matrix_calculations::*;
pub use matrix_slice::*;
pub use transformations::*;

pub mod matrix;
pub mod matrix_ops;
pub mod matrix_slice;
pub mod matrix_calculations;
pub mod transformations;
pub mod decompose;

#[macro_export]
macro_rules! matrix {
    ($elem:expr; ($r:expr, $c:expr)) => (
        $crate::Matrix::<_, $r, $c>::array_row_wise([$elem; $r * $c])
    );
    ($($x:expr),+ $(,)?) => (
        $crate::Matrix::array_row_wise([$($x),+])
    );
}

#[cfg(test)]
mod tests {
    use crate::Vector;

    use super::*;

    #[test]
    fn it_works() {
        let mut a = Matrix::<_, 2, 2>::identity();
        println!("a = {:?}", a);
        a.columns[0][1] = 2.0;
        a.columns[1][1] = 3.0;
        println!("a = {:?}", a);
        a.transpose_assign();
        println!("a^T = {:?}", a);
        let i = a.invert().unwrap();
        println!("a = {:?}", a);
        println!("a^-1 = {:?}", i);

        println!("a * (a^-1) = {:?}", a * i);
        println!("(a^-1) * a = {:?}", i * a);

        println!("a * <7, 13> = {}", a * Vector::<_, 2>::new(7.0, 13.0));
        println!("{:?} * <7, 13> = {}", Matrix::columns([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]), Matrix::columns([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]]) * Vector::<_, 2>::new(7.0, 13.0));

        println!("0.5 * a * 2.0 = {:?}", 0.5 * a * 2.0);
        println!("(0.5 * a * 2.0) == a => {:?}", (0.5 * a * 2.0) == a);

        let k = Matrix::<f32, 7, 9>::array_column_wise(crate::utility::array::from_iter(core::iter::repeat(()).enumerate().map(|(i, _)| i as f32)).unwrap());

        let m = k.transpose();

        println!("k = {}", k);
        println!("m = {}", m);

        assert_eq!((1, 1), k[0..1][0..1].size());
        println!("k[0..1][0..1] = {}", &k[0..1][0..1]);
        assert_eq!((1, 1), k[1..2][1..2].size());
        println!("k[1..2][1..2] = {}", &k[1..2][1..2]);
        assert_eq!((1, 1), k[2..3][2..3].size());
        println!("k[2..3][2..3] = {}", &k[2..3][2..3]);
        assert_eq!((2, 1), k[0..2][0..1].size());
        println!("k[0..2][0..1] = {}", &k[0..2][0..1]);
        assert_eq!((2, 1), k[1..3][1..2].size());
        println!("k[1..3][1..2] = {}", &k[1..3][1..2]);
        assert_eq!((2, 1), k[2..4][2..3].size());
        println!("k[2..4][2..3] = {}", &k[2..4][2..3]);
        assert_eq!((1, 2), k[0..1][0..2].size());
        println!("k[0..1][0..2] = {}", &k[0..1][0..2]);
        assert_eq!((1, 2), k[1..2][1..3].size());
        println!("k[1..2][1..3] = {}", &k[1..2][1..3]);
        assert_eq!((1, 2), k[2..3][2..4].size());
        println!("k[2..3][2..4] = {}", &k[2..3][2..4]);
        assert_eq!((7, 2), k[..][2..4].size());
        println!("k[..][2..4] = {}", &k[..][2..4]);
        assert_eq!((2, 2), k[2..4][2..4].size());
        println!("k[2..4][2..4] = {}", &k[2..4][2..4]);
        assert_eq!((2, 9), k[2..4][..].size());
        println!("k[2..4][..] = {}", &k[2..4][..]);

        assert_eq!((1, 1), m[0..1][0..1].size());
        println!("m[0..1][0..1] = {}", &m[0..1][0..1]);
        assert_eq!((1, 1), m[1..2][1..2].size());
        println!("m[1..2][1..2] = {}", &m[1..2][1..2]);
        assert_eq!((1, 1), m[2..3][2..3].size());
        println!("m[2..3][2..3] = {}", &m[2..3][2..3]);
        assert_eq!((2, 1), m[0..2][0..1].size());
        println!("m[0..2][0..1] = {}", &m[0..2][0..1]);
        assert_eq!((2, 1), m[1..3][1..2].size());
        println!("m[1..3][1..2] = {}", &m[1..3][1..2]);
        assert_eq!((2, 1), m[2..4][2..3].size());
        println!("m[2..4][2..3] = {}", &m[2..4][2..3]);
        assert_eq!((1, 2), m[0..1][0..2].size());
        println!("m[0..1][0..2] = {}", &m[0..1][0..2]);
        assert_eq!((1, 2), m[1..2][1..3].size());
        println!("m[1..2][1..3] = {}", &m[1..2][1..3]);
        assert_eq!((1, 2), m[2..3][2..4].size());
        println!("m[2..3][2..4] = {}", &m[2..3][2..4]);
        assert_eq!((9, 2), m[..][2..4].size());
        println!("m[..][2..4] = {}", &m[..][2..4]);
        assert_eq!((2, 2), m[2..4][2..4].size());
        println!("m[2..4][2..4] = {}", &m[2..4][2..4]);
        assert_eq!((2, 7), m[2..4][..].size());
        println!("m[2..4][..] = {}", &m[2..4][..]);


        assert_eq!(Matrix1f::identity().determinant(), 1.0);
        assert_eq!(Matrix2f::identity().determinant(), 1.0);
        assert_eq!(Matrix3f::identity().determinant(), 1.0);
        assert_eq!(Matrix4f::identity().determinant(), 1.0);

        assert_eq!(Matrix1f::identity().transpose().determinant(), 1.0);
        assert_eq!(Matrix2f::identity().transpose().determinant(), 1.0);
        assert_eq!(Matrix3f::identity().transpose().determinant(), 1.0);
        assert_eq!(Matrix4f::identity().transpose().determinant(), 1.0);

        let m = Matrix::<_, 10, 10>::from_fn(|r, c| (if (r + c) % 3 == 0 { -1.0 } else { 1.0 }) * (r as f32 + 1.0).powf((if (r + c) % 2 == 0 { -1.0 } else { 1.0 }) * (c as f32 + 1.0)));

        macro_rules! det_test {
            ($size:literal) => {
                let p = m[0..$size][0..$size].matrix::<$size,$size>();
                println!("p{} = {}, det = {}", $size, p, p.determinant());
            };
        }

        det_test!(1);
        det_test!(2);
        det_test!(3);
        det_test!(4);
        det_test!(5);
        det_test!(6);
        det_test!(7);
        det_test!(8);
        det_test!(9);
        det_test!(10);

        // let u = Matrix2f64::from_rows([[2.0, 4.0], [8.0, 16.0]]);

        // test_a(Matrix::zero());
        // test_b(Matrix::zero());

        {
            let _a: Matrix2<_> = matrix![
                1, 2,
                3, 4
            ];

            let _b = matrix![0; (2, 2)];
        }
    }
}

#[inline(never)]
pub fn test_a(m: Matrix2f64) -> Matrix2f64 {
    m.invert()
        .unwrap()
}

#[inline(never)]
pub fn test_b(m: Matrix2f64) -> Matrix2f64 {
    let d = m.determinant();
    (d != 0.0).then(|| Matrix::columns([[m[1][1], -m[1][0]], [-m[0][1], m[0][0]]]) / d)
        .unwrap()
}

#[inline(never)]
pub fn test_c(m: Matrix2f64) -> Matrix2f64 {
    m.invert_unchecked()
}

#[inline(never)]
pub fn test_d(m: Matrix2f64) -> Matrix2f64 {
    Matrix::columns([[m[1][1], -m[1][0]], [-m[0][1], m[0][0]]]) / m.determinant()
}

#[inline(never)]
pub fn test_f(m: Matrix3f) -> Matrix3f {
    m.invert_unchecked()
}

#[inline(never)]
pub fn test_e(m: Matrix4f) -> Matrix4f {
    m.invert_unchecked()
}
