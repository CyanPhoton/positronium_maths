use crate::{Matrix3, Matrix4, FloatScalar, Vector3, vector, Scalar};
use crate::quaternion::Quaternion;

///
/// A 3x3 matrix broken down into it's geometric parts
///
/// Assumes there is no shear component
///
pub struct Decomposition3<T: Scalar> {
    pub rotation: Quaternion<T>,
    pub scale: Vector3<T>,
}

///
/// A 4x4 matrix broken down into it's geometric parts
///
/// Assumes there is no shear component
///
pub struct Decomposition4<T: Scalar> {
    pub rotation: Quaternion<T>,
    pub scale: Vector3<T>,
    pub translation: Vector3<T>,
}

impl<T: FloatScalar> Matrix3<T> {
    /// Decompose the matrix M as M = R * S, where S is a scale and R is a rotation.
    ///
    /// Assumes there is no shear.
    /// Returns None if any scale is 0.
    pub fn decompose_rotate_scale(self) -> Option<Decomposition3<T>> {
        let col_x = self.column_vector(0);
        let col_y = self.column_vector(1);
        let col_z = self.column_vector(2);

        let scale_x = col_x.norm();
        let scale_y = col_y.norm();
        let scale_z = col_z.norm();

        if scale_x == T::ZERO || scale_y == T::ZERO || scale_z == T::ZERO {
            // Can't cancel out a scale of 0, as it destroys information
            return None;
        }

        let scale = vector![scale_x, scale_y, scale_z];

        let rot_x = col_x / scale_x;
        let rot_y = col_y / scale_y;
        let rot_z = col_z / scale_z;

        let rotation = Matrix3::column_vectors([rot_x, rot_y, rot_z]).rotate_to_quaternion();

        Some(Decomposition3 {
            rotation,
            scale,
        })
    }

    /// Decompose the matrix M as M = S * R, where S is a scale and R is a rotation.
    ///
    /// Assumes there is no shear.
    /// Returns None if any scale is 0.
    pub fn decompose_scale_rotate(self) -> Option<Decomposition3<T>> {
        let row_x = self.row_vector(0);
        let row_y = self.row_vector(1);
        let row_z = self.row_vector(2);

        let scale_x = row_x.norm();
        let scale_y = row_y.norm();
        let scale_z = row_z.norm();

        if scale_x == T::ZERO || scale_y == T::ZERO || scale_z == T::ZERO {
            // Can't cancel out a scale of 0, as it destroys information
            return None;
        }

        let scale = vector![scale_x, scale_y, scale_z];

        let rot_x = row_x / scale_x;
        let rot_y = row_y / scale_y;
        let rot_z = row_z / scale_z;

        let rotation = Matrix3::row_vectors([rot_x, rot_y, rot_z]).rotate_to_quaternion();

        Some(Decomposition3 {
            rotation,
            scale,
        })
    }
}

impl<T: FloatScalar> Matrix4<T> {
    /// Decompose the matrix M as M = T * R * S, where S is a scale, R is a rotation and T is a translation.
    ///
    /// Assumes there is no shear.
    /// Returns None if any scale is 0.
    pub fn decompose_translate_rotate_scale(self) -> Option<Decomposition4<T>> {
        let translation = self[0..3][3].vector();
        self[0..3][0..3].matrix::<3, 3>().decompose_rotate_scale().map(|Decomposition3 { rotation, scale }| {
            Decomposition4 {
                rotation,
                scale,
                translation,
            }
        })
    }

    /// Decompose the matrix M as M = T * S * R, where S is a scale, R is a rotation and T is a translation.
    ///
    /// Assumes there is no shear.
    /// Returns None if any scale is 0.
    pub fn decompose_translate_scale_rotate(self) -> Option<Decomposition4<T>> {
        let translation = self[0..3][3].vector();
        self[0..3][0..3].matrix::<3, 3>().decompose_scale_rotate().map(|Decomposition3 { rotation, scale }| {
            Decomposition4 {
                rotation,
                scale,
                translation,
            }
        })
    }
}