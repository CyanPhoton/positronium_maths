use crate::{OrdScalar, Scalar, SignedScalar, Matrix};

impl<T: Scalar, const R: usize, const C: usize> Matrix<T, R, C> {
    pub fn comp_abs(self) -> Self where T: SignedScalar {
        self.map(|v| v.abs())
    }

    pub fn comp_sum(&self) -> T {
        let mut sum = T::ZERO;
        for c in 0..C {
            for r in 0..R {
                sum += self[r][c];
            }
        }
        sum
    }

    pub fn all<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter_column_wise().all(f)
    }

    pub fn zip_all<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Matrix<V, R, C>, mut f: F) -> bool {
        self.iter_column_wise().zip(other.iter_column_wise()).all(|(l, r)| f(l, r))
    }

    pub fn any<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter_column_wise().any(f)
    }

    pub fn zip_any<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Matrix<V, R, C>, mut f: F) -> bool {
        self.iter_column_wise().zip(other.iter_column_wise()).any(|(l, r)| f(l, r))
    }
}

impl<T: OrdScalar, const R: usize, const C: usize> Matrix<T, R, C> {
    pub fn comp_min(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.min(r))
    }

    pub fn comp_max(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.max(r))
    }

    pub fn comp_clamp(self, lower: Self, upper: Self) -> Self {
        Self::from_fn(|r, c| {
            self[r][c].clamp(lower[r][c], upper[r][c])
        })
    }

    pub fn comp_gt(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l > r)
    }

    pub fn comp_ge(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l >= r)
    }

    pub fn comp_lt(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l < r)
    }

    pub fn comp_le(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l <= r)
    }
}