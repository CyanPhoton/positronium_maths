use std::fmt::{Debug, Display, Formatter};
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

use crate::{Matrix, Radians, FloatScalar, Scalar, SignedScalar, Vector, ZeroInit, NotNan};

#[repr(C)]
#[derive(Default, Eq, PartialEq, Copy, Clone, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Complex<T> {
    pub real: T,
    pub imaginary: T,
}

/// Safety: Is repr(c) and both fields are Zeroable, and will have no padding
unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for Complex<T> {}

/// Safety: Is repr(c) and both fields are Pod, and will have no padding
unsafe impl<T: bytemuck::Pod> bytemuck::Pod for Complex<T> {}

/// Safety: Is repr(c) and both fields are Pod, and will have no padding
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for Complex<NotNan<T>> {}

impl<T: ZeroInit> ZeroInit for Complex<T> {
    const ZERO: Self = Complex {
        real: T::ZERO,
        imaginary: T::ZERO,
    };
}

impl<T> Complex<T> {
    pub const fn zero() -> Complex<T> where T: ZeroInit {
        Self::ZERO
    }

    pub const fn rect(real: T, imaginary: T) -> Complex<T> {
        Complex {
            real,
            imaginary,
        }
    }
}

impl<T: Scalar> Complex<T> {
    pub fn polar<R>(norm: T, arg: R) -> Complex<T> where T: FloatScalar, R: Into<Radians<T>> {
        let radians = arg.into();
        let (s, c) = radians.sin_cos();

        Complex {
            real: norm * s,
            imaginary: norm * c,
        }
    }

    pub const fn one() -> Complex<T> {
        Complex {
            real: T::ONE,
            imaginary: T::ZERO,
        }
    }

    pub const fn i() -> Complex<T> {
        Complex {
            real: T::ZERO,
            imaginary: T::ONE,
        }
    }

    pub const fn real(real: T) -> Complex<T> {
        Complex {
            real,
            imaginary: T::ZERO,
        }
    }

    pub const fn imaginary(imaginary: T) -> Complex<T> {
        Complex {
            real: T::ZERO,
            imaginary,
        }
    }

    pub fn norm_squared(self) -> T {
        (self.real * self.real) + (self.imaginary * self.imaginary)
    }

    pub fn norm(self) -> T where T: FloatScalar {
        self.norm_squared().sqrt()
    }

    pub fn arg(self) -> T where T: FloatScalar {
        self.imaginary.atan2(self.real)
    }

    pub fn unit(self) -> Complex<T> where T: FloatScalar {
        self / self.norm()
    }

    pub fn unit_assign(&mut self) where T: FloatScalar {
        *self = *self / self.norm()
    }

    pub fn conjugate(self) -> Complex<T> where T: SignedScalar {
        Complex {
            real: self.real,
            imaginary: -self.imaginary,
        }
    }

    pub fn conjugate_assign(&mut self) where T: SignedScalar {
        self.imaginary = -self.imaginary;
    }

    pub fn inverse(self) -> Complex<T> where T: FloatScalar {
        self.conjugate() / self.norm_squared()
    }

    pub fn inverse_assign(&mut self) where T: FloatScalar {
        *self = self.conjugate() / self.norm_squared();
    }

    pub fn to_transformation(self) -> Matrix<T, 2, 2> where T: FloatScalar {
        let arg = self.arg();
        let norm = self.norm();
        Matrix::<T, 2, 2>::rotate(arg) * Matrix::<T, 2, 2>::scale(norm)
    }

    pub fn to_rotation(self) -> Matrix<T, 2, 2> where T: FloatScalar {
        let arg = self.arg();
        Matrix::<T, 2, 2>::rotate(arg)
    }

    pub fn powf(self, t: T) -> Complex<T> where T: FloatScalar {
        let arg = self.arg();
        let norm = self.norm();

        Complex::polar(norm.powf(t), arg * t)
    }
}

impl<T> From<(T, T)> for Complex<T> {
    fn from(v: (T, T)) -> Self {
        Self::rect(v.0, v.1)
    }
}

impl<T> From<Complex<T>> for (T, T) {
    fn from(v: Complex<T>) -> Self {
        (v.real, v.imaginary)
    }
}

impl<T> From<Vector<T, 2>> for Complex<T> {
    fn from(v: Vector<T, 2>) -> Self {
        let v: (T, T) = v.into();
        v.into()
    }
}

impl<T> From<Complex<T>> for Vector<T, 2> {
    fn from(v: Complex<T>) -> Self {
        (v.real, v.imaginary).into()
    }
}

// Addition

impl<T: Scalar> Add for Complex<T> {
    type Output = Complex<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Complex {
            real: self.real + rhs.real,
            imaginary: self.imaginary + rhs.imaginary,
        }
    }
}

impl<T: Scalar> AddAssign for Complex<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.real += rhs.real;
        self.imaginary += rhs.imaginary;
    }
}

// Subtraction

impl<T: Scalar> Sub for Complex<T> {
    type Output = Complex<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Complex {
            real: self.real - rhs.real,
            imaginary: self.imaginary - rhs.imaginary,
        }
    }
}

impl<T: Scalar> SubAssign for Complex<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.real -= rhs.real;
        self.imaginary -= rhs.imaginary;
    }
}

// Complex x Complex multiplication

impl<T: Scalar> Mul<Complex<T>> for Complex<T> {
    type Output = Complex<T>;

    fn mul(self, rhs: Complex<T>) -> Self::Output {
        Complex {
            real: self.real * rhs.real - self.imaginary * rhs.imaginary,
            imaginary: self.real * rhs.imaginary + self.imaginary * rhs.real,
        }
    }
}

// Multiplication

impl<T: Scalar> Mul<T> for Complex<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Complex {
            real: self.real * rhs,
            imaginary: self.imaginary * rhs,
        }
    }
}

impl<T: Scalar> MulAssign<T> for Complex<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.real *= rhs;
        self.imaginary *= rhs;
    }
}

macro_rules! complex_pre_scale {
    ($($t:ty),+) => {
        $(impl Mul<Complex<$t>> for $t {
            type Output = Complex<$t>;

            fn mul(self, rhs: Complex<$t>) -> Complex<$t> {
                Complex {
                    real: self * rhs.real,
                    imaginary: self * rhs.imaginary
                }
            }
        })
        *
    };
}

complex_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64);

// Complex / Complex division

impl<T: FloatScalar> Div<Complex<T>> for Complex<T> {
    type Output = Complex<T>;

    fn div(self, rhs: Complex<T>) -> Self::Output {
        self * rhs.inverse()
    }
}

// Division

impl<T: Scalar> Div<T> for Complex<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Complex {
            real: self.real / rhs,
            imaginary: self.imaginary / rhs,
        }
    }
}

impl<T: Scalar> DivAssign<T> for Complex<T> {
    fn div_assign(&mut self, rhs: T) {
        self.real /= rhs;
        self.imaginary /= rhs;
    }
}

macro_rules! complex_pre_division {
    ($($t:ty),+) => {
        $(impl Div<Complex<$t>> for $t {
            type Output = Complex<$t>;

            fn div(self, rhs: Complex<$t>) -> Complex<$t> {
                Complex::real(self) / rhs
            }
        })
        *
    };
}

complex_pre_division!(f32, f64);

// Negation

impl<T: SignedScalar> Neg for Complex<T> {
    type Output = Complex<T>;

    fn neg(self) -> Self::Output {
        Complex {
            real: -self.real,
            imaginary: -self.imaginary,
        }
    }
}

// Printing

impl<T> Debug for Complex<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} + i{:?}", self.real, self.imaginary)
    }
}

impl<T> Display for Complex<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} + i{}", self.real, self.imaginary)
    }
}
