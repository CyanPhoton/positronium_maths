use std::fmt::{Debug, Display, Formatter};
use crate::{Matrix, Matrix4, FloatScalar, UnitVector, Vector, Vector2, Scalar};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Ray<T: 'static, const N: usize> {
    pub start: Vector<T, N>,
    pub direction: UnitVector<T, N>
}

/// Safety: Is repr(c) and two fields are NoUninit and without padding
unsafe impl<T: bytemuck::NoUninit, const N: usize> bytemuck::NoUninit for Ray<T, N> {}

impl<T, const N: usize> Ray<T, N> {
    pub fn new(start: Vector<T, N>, direction: UnitVector<T, N>) -> Self {
        Self {
            start,
            direction
        }
    }
}

impl<T: FloatScalar> Ray<T, 3> {
    pub fn transform(self, transform: Matrix<T, 4, 4>) -> Self {
        Self {
            start: (transform * self.start.to_homogeneous(T::ONE)).space(),
            direction: (transform * self.direction.into_vector().to_homogeneous(T::ZERO)).space().unit(),
        }
    }

    #[cfg(feature = "simd")]
    pub fn transform_simd(self, transform: crate::simd::SimdMatrix<T, 4, 4>) -> Self where T: crate::SimdScalar, std::simd::Simd<T, 4>: crate::Arithmetic {
        Self {
            start: (transform * self.start.with_w_simd(T::ONE)).space(),
            direction: (transform * self.direction.with_w_simd(T::ZERO)).space().unit(),
        }
    }

    pub fn view_ray_from_ndc(projection_matrix: &Matrix4<T>, ndc: Vector2<T>) -> Self {
        let k_x = projection_matrix[0][0];
        let k_y = projection_matrix[1][1];
        let k = Vector2::new(k_x, k_y);

        let c = projection_matrix[3][2];
        let d = projection_matrix[3][3];
        let s = c.signum();

        let scaled_ndc = (ndc * c) | k;

        let n_z = (T::ONE + scaled_ndc.norm_squared()).sqrt().recip() * s;
        let n_xy = scaled_ndc * n_z;

        let start = (ndc * d) | k;

        // (n_x, n_y, n_z) is guaranteed to always be unit length, hence no call to unit() is needed
        Ray::new(start.push(T::ZERO), UnitVector(n_xy.push(n_z)))
    }
}

impl<T: Scalar + Debug, const N: usize> Debug for Ray<T, N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.start == Vector::<T, N>::zero() {
            write!(f, "Ray(λ) = λ * {:?}", self.direction)
        } else {
            write!(f, "Ray(λ) = {:?} + λ * {:?}", self.start, self.direction)
        }
    }
}

impl<T: Scalar + Display, const N: usize> Display for Ray<T, N> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        if self.start == Vector::<T, N>::zero() {
            write!(f, "Ray(λ) = λ * {}", self.direction)
        } else {
            write!(f, "Ray(λ) = {} + λ * {}", self.start, self.direction)
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::*;
    use crate::ray::Ray;

    #[test]
    fn test() {
        let m = Matrix4f::projection(Degrees::new(90.0), 0.0, 1.0, ControlAxis::Vertical, DepthDomain::Infinite { near: 0.01 }, ForwardDirection::OPEN_GL, DepthRange::OPEN_GL);

        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(-1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, -1.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, 1.0)));

        let m = Matrix4f::projection(Degrees::new(0.0), 10.0, 1.0, ControlAxis::Vertical, DepthDomain::Infinite { near: 0.01 }, ForwardDirection::OPEN_GL, DepthRange::OPEN_GL);

        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(-1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, -1.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, 1.0)));

        let m = Matrix4f::projection(Degrees::new(90.0), 10.0, 1.0, ControlAxis::Vertical, DepthDomain::Infinite { near: 0.01 }, ForwardDirection::OPEN_GL, DepthRange::OPEN_GL);

        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(-1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(1.0, 0.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, -1.0)));
        dbg!(&Ray::view_ray_from_ndc(&m, Vector2::new(0.0, 1.0)));
    }
}