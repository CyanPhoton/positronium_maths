use std::cmp::Ordering;
use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};
use crate::{CastAs, FloatScalar, NotNan, Scalar, SignedScalar, ZeroInit};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

#[repr(transparent)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default, Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Radians<T>(T);

/// Safety: Is repr(transparent) and single field is Zeroable
unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for Radians<T> {}

/// Safety: Is repr(transparent) and single field is Pod
unsafe impl<T: bytemuck::Pod> bytemuck::Pod for Radians<T> {}

/// Safety: Is repr(transparent) and single field is NoUninit
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for Radians<NotNan<T>> {}

impl<T: ZeroInit> ZeroInit for Radians<T> {
    const ZERO: Self = Self(ZeroInit::ZERO);
}

#[repr(transparent)]
#[derive(Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Default, Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct Degrees<T>(T);

/// Safety: Is repr(transparent) and single field is Zeroable
unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for Degrees<T> {}

/// Safety: Is repr(transparent) and single field is Pod
unsafe impl<T: bytemuck::Pod> bytemuck::Pod for Degrees<T> {}

/// Safety: Is repr(transparent) and single field is NoUninit
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for Degrees<NotNan<T>> {}

impl<T: ZeroInit> ZeroInit for Degrees<T> {
    const ZERO: Self = Self(ZeroInit::ZERO);
}

impl<T> Radians<T> {
    pub const fn new(value: T) -> Self {
        Self(value)
    }

    pub fn value(self) -> T {
        self.0
    }

    pub fn map<V, F: FnOnce(T) -> V>(self, map: F) -> Radians<V> {
        Radians(map(self.0))
    }

    pub fn try_map<V, E, F: FnOnce(T) -> Result<V, E>>(self, map: F) -> Result<Radians<V>, E> {
        Ok(Radians(map(self.0)?))
    }

    pub fn zip_map<U, V, F: FnOnce(T, V) -> U>(self, other: Radians<V>, map: F) -> Radians<U> {
        Radians(map(self.0, other.0))
    }

    pub fn cast_as<V>(self) -> Radians<V> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }

    pub fn comp_into<V>(self) -> Radians<V> where T: Into<V> {
        self.map(|v| v.into())
    }

    pub fn comp_try_into<V, E>(self) -> Result<Radians<V>, E> where T: TryInto<V, Error = E> {
        self.try_map(|v| v.try_into())
    }
}

impl<T: FloatScalar> Radians<T> {
    pub fn to_degrees(self) -> Degrees<T> {
        Degrees(self.0.to_degrees())
    }

    // +- 2pi * k, so that ranges in [0, 2pi)
    pub fn normalise(self) -> Self {
        Radians(self.0.rem_euclid(T::PI + T::PI))
    }
}

impl<T> Degrees<T> {
    pub const fn new(value: T) -> Self {
        Self(value)
    }

    pub fn value(self) -> T {
        self.0
    }

    pub fn map<V, F: FnOnce(T) -> V>(self, map: F) -> Degrees<V> {
        Degrees(map(self.0))
    }

    pub fn try_map<V, E, F: FnOnce(T) -> Result<V, E>>(self, map: F) -> Result<Degrees<V>, E> {
        Ok(Degrees(map(self.0)?))
    }

    pub fn zip_map<U, V, F: FnOnce(T, V) -> U>(self, other: Degrees<V>, map: F) -> Degrees<U> {
        Degrees(map(self.0, other.0))
    }

    pub fn cast_as<V>(self) -> Degrees<V> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }
}

impl<T: Scalar> Degrees<T> {
    // +- 360 * k, so that ranges in [0, 360)
    pub fn normalise(self) -> Self where T: From<u16> {
        Degrees(self.0.rem_euclid(T::from(360)))
    }
}

impl<T: FloatScalar> Degrees<T> {
    pub fn to_radians(self) -> Radians<T> {
        Radians(self.0.to_radians())
    }
}

// Addition

impl<T: Scalar, O> Add<O> for Degrees<T> where O: Into<Degrees<T>> {
    type Output = Self;

    fn add(self, rhs: O) -> Self {
        Degrees(self.0 + rhs.into().0)
    }
}

impl<T: Scalar, O> AddAssign<O> for Degrees<T> where O: Into<Degrees<T>> {
    fn add_assign(&mut self, rhs: O) {
        self.0 += rhs.into().0;
    }
}

impl<T: Scalar, O> Add<O> for Radians<T> where O: Into<Radians<T>> {
    type Output = Self;

    fn add(self, rhs: O) -> Self {
        Radians(self.0 + rhs.into().0)
    }
}

impl<T: Scalar, O> AddAssign<O> for Radians<T> where O: Into<Radians<T>> {
    fn add_assign(&mut self, rhs: O) {
        self.0 += rhs.into().0;
    }
}

// Subtraction

impl<T: Scalar, O> Sub<O> for Degrees<T> where O: Into<Degrees<T>> {
    type Output = Self;

    fn sub(self, rhs: O) -> Self {
        Degrees(self.0 - rhs.into().0)
    }
}

impl<T: Scalar, O> SubAssign<O> for Degrees<T> where O: Into<Degrees<T>> {
    fn sub_assign(&mut self, rhs: O) {
        self.0 -= rhs.into().0;
    }
}

impl<T: Scalar, O> Sub<O> for Radians<T> where O: Into<Radians<T>> {
    type Output = Self;

    fn sub(self, rhs: O) -> Self {
        Radians(self.0 - rhs.into().0)
    }
}

impl<T: Scalar, O> SubAssign<O> for Radians<T> where O: Into<Radians<T>> {
    fn sub_assign(&mut self, rhs: O) {
        self.0 -= rhs.into().0;
    }
}

// Multiplication

impl<T: Scalar> Mul<T> for Degrees<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Degrees(self.0 * rhs)
    }
}

impl<T: Scalar> MulAssign<T> for Degrees<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.0 *= rhs;
    }
}

impl<T: Scalar> Mul<T> for Radians<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Radians(self.0 * rhs)
    }
}

impl<T: Scalar> MulAssign<T> for Radians<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.0 *= rhs;
    }
}

macro_rules! angle_pre_scale {
    ($angle:ident, $($t:ty),+) => {
        $(impl Mul<$angle <$t>> for $t {
            type Output = $angle <$t>;

            fn mul(self, rhs: $angle <$t>) -> $angle <$t> {
                $angle(self * rhs.0)
            }
        })
        *
    };
}

angle_pre_scale!(Radians, i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64);
angle_pre_scale!(Degrees, i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64);

// Division

impl<T: Scalar> Div<T> for Degrees<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Degrees(self.0 / rhs)
    }
}

impl<T: Scalar> DivAssign<T> for Degrees<T> {
    fn div_assign(&mut self, rhs: T) {
        self.0 /= rhs;
    }
}

impl<T: Scalar> Div<T> for Radians<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Radians(self.0 / rhs)
    }
}

impl<T: Scalar> DivAssign<T> for Radians<T> {
    fn div_assign(&mut self, rhs: T) {
        self.0 /= rhs;
    }
}

impl<T: Scalar> Div<Degrees<T>> for Degrees<T> {
    type Output = T;

    fn div(self, rhs: Degrees<T>) -> T {
        self.0 / rhs.0
    }
}

impl<T: FloatScalar> Div<Radians<T>> for Degrees<T> {
    type Output = T;

    fn div(self, rhs: Radians<T>) -> T {
        self.to_radians() / rhs
    }
}

impl<T: Scalar> Div<Radians<T>> for Radians<T> {
    type Output = T;

    fn div(self, rhs: Radians<T>) -> T {
        self.0 / rhs.0
    }
}

impl<T: FloatScalar> Div<Degrees<T>> for Radians<T> {
    type Output = T;

    fn div(self, rhs: Degrees<T>) -> T {
        self / rhs.to_radians()
    }
}

// Negation

impl<T: SignedScalar> Neg for Degrees<T> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Degrees(-self.0)
    }
}

impl<T: SignedScalar> Neg for Radians<T> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        Radians(-self.0)
    }
}

// Conversion

impl<T> From<T> for Radians<T> {
    fn from(v: T) -> Self {
        Radians(v)
    }
}

impl<T> From<T> for Degrees<T> {
    fn from(v: T) -> Self {
        Degrees(v)
    }
}

impl<T: FloatScalar> From<Degrees<T>> for Radians<T> {
    fn from(v: Degrees<T>) -> Radians<T> {
        v.to_radians()
    }
}

impl<T: FloatScalar> From<Radians<T>> for Degrees<T> {
    fn from(v: Radians<T>) -> Degrees<T> {
        v.to_degrees()
    }
}

// Comparison

impl<T: FloatScalar> PartialEq<Radians<T>> for Degrees<T> {
    fn eq(&self, other: &Radians<T>) -> bool {
        self.to_radians() == *other
    }
}

impl<T: FloatScalar> PartialEq<Degrees<T>> for Radians<T> {
    fn eq(&self, other: &Degrees<T>) -> bool {
        self.to_degrees() == *other
    }
}

impl<T: FloatScalar> PartialOrd<Radians<T>> for Degrees<T> {
    fn partial_cmp(&self, other: &Radians<T>) -> Option<Ordering> {
        self.to_radians().partial_cmp(other)
    }
}

impl<T: FloatScalar> PartialOrd<Degrees<T>> for Radians<T> {
    fn partial_cmp(&self, other: &Degrees<T>) -> Option<Ordering> {
        self.to_degrees().partial_cmp(other)
    }
}