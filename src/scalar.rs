use std::ops::{Add, AddAssign, Div, DivAssign, Mul, MulAssign, Neg, Rem, RemAssign, Sub, SubAssign};

pub trait Arithmetic: Sized + 'static
+ Add<Self, Output=Self>
+ Sub<Self, Output=Self>
+ Mul<Self, Output=Self>
+ Div<Self, Output=Self>
+ Rem<Self, Output=Self>
+ AddAssign<Self>
+ SubAssign<Self>
+ MulAssign<Self>
+ DivAssign<Self>
+ RemAssign<Self>
+ PartialEq<Self> {}

pub trait SignedArithmetic: Arithmetic + Neg<Output=Self> {}

pub trait Scalar: Arithmetic + ZeroInit + Copy {
    const ONE: Self;

    fn rem_euclid(self, rhs: Self) -> Self;
    fn div_euclid(self, rhs: Self) -> Self;
}

pub trait SignedScalar: Scalar + SignedArithmetic {
    const NEGATIVE_ONE: Self;

    fn abs(self) -> Self;
    fn signum(self) -> Self;
}

pub trait IntScalar: Scalar {
    fn pow(self, n: u32) -> Self;
}

pub trait OrdScalar: Scalar + PartialOrd<Self> {
    fn min(self, other: Self) -> Self;
    fn max(self, other: Self) -> Self;
    fn clamp(self, min: Self, max: Self) -> Self;
}

pub trait FloatScalar: SignedScalar + OrdScalar {
    const PI: Self;
    const E: Self;
    const POSITIVE_INFINITY: Self;
    const NEGATIVE_INFINITY: Self;
    const EPSILON: Self;

    fn sqrt(self) -> Self;
    fn recip(self) -> Self;
    fn powf(self, n: Self) -> Self;
    fn powi(self, n: i32) -> Self;

    fn to_radians(self) -> Self;
    fn to_degrees(self) -> Self;

    fn sin(self) -> Self;
    fn cos(self) -> Self;
    fn tan(self) -> Self;
    fn sin_cos(self) -> (Self, Self);

    fn asin(self) -> Self;
    fn acos(self) -> Self;
    fn atan(self) -> Self;
    fn atan2(self, other: Self) -> Self;

    fn is_nan(self) -> bool;
}

pub trait ZeroInit {
    const ZERO: Self;
}

impl<T: ZeroInit, const N: usize> ZeroInit for [T; N] {
    const ZERO: Self = [T::ZERO; N];
}

impl<A> Arithmetic for A where A: Sized + 'static
+ Add<A, Output=A>
+ Sub<A, Output=A>
+ Mul<A, Output=A>
+ Div<A, Output=A>
+ Rem<A, Output=A>
+ AddAssign<A>
+ SubAssign<A>
+ MulAssign<A>
+ DivAssign<A>
+ RemAssign<A>
+ PartialEq<A>
+ PartialOrd<A> {}

impl<A> SignedArithmetic for A where A: Arithmetic + Neg<Output=A> {}

macro_rules! signed_integral_scalar {
    ($($t:ty),*) => {
        $(
            impl SignedScalar for $t {
                const NEGATIVE_ONE: Self = -1;

                fn abs(self) -> Self {
                    <$t>::abs(self)
                }

                fn signum(self) -> Self {
                    <$t>::signum(self)
                }
            }
        )*

        integral_scalar!($($t),*);
    };
}

macro_rules! unsigned_integral_scalar {
    ($($t:ty),*) => {
        integral_scalar!($($t),*);
    };
}

macro_rules! integral_scalar {
    ($($t:ty),*) => {
        $(
            impl ZeroInit for $t {
                const ZERO: Self = 0;
            }

            impl Scalar for $t {
                const ONE: Self = 1;

                #[inline(always)]
                fn rem_euclid(self, rhs: Self) -> Self {
                    <$t>::rem_euclid(self, rhs)
                }

                #[inline(always)]
                fn div_euclid(self, rhs: Self) -> Self {
                    <$t>::div_euclid(self, rhs)
                }
            }

            impl IntScalar for $t {
                fn pow(self, n: u32) -> Self {
                    <$t>::pow(self, n)
                }
            }

            impl OrdScalar for $t {
                #[inline(always)]
                fn min(self, other: Self) -> Self {
                    <$t as Ord>::min(self, other)
                }

                #[inline(always)]
                fn max(self, other: Self) -> Self {
                    <$t as Ord>::max(self, other)
                }

                #[inline(always)]
                fn clamp(self, min: Self, max: Self) -> Self {
                    <$t as Ord>::clamp(self, min, max)
                }
            }
        )*
    };
}

macro_rules! real_scalar {
    ($($t:ty, $p:path),*) => {
        $(
            impl ZeroInit for $t {
                const ZERO: Self = 0.0;
            }

            impl Scalar for $t {
                const ONE: Self = 1.0;

                #[inline(always)]
                fn rem_euclid(self, rhs: Self) -> Self {
                    <$t>::rem_euclid(self, rhs)
                }

                #[inline(always)]
                fn div_euclid(self, rhs: Self) -> Self {
                    <$t>::div_euclid(self, rhs)
                }
            }

            impl SignedScalar for $t {
                const NEGATIVE_ONE: Self = -1.0;

                #[inline(always)]
                fn abs(self) -> Self {
                    <$t>::abs(self)
                }

                #[inline(always)]
                fn signum(self) -> Self {
                    <$t>::signum(self)
                }
            }

            impl OrdScalar for $t {
                #[inline(always)]
                fn min(self, other: Self) -> Self {
                    <$t>::min(self, other)
                }

                #[inline(always)]
                fn max(self, other: Self) -> Self {
                    <$t>::max(self, other)
                }

                #[inline(always)]
                fn clamp(self, min: Self, max: Self) -> Self {
                    <$t>::clamp(self, min, max)
                }
            }

            impl FloatScalar for $t {
                const PI: Self = { use $p as base; base::consts::PI };
                const E: Self = { use $p as base; base::consts::E };
                const POSITIVE_INFINITY: Self = <$t>::INFINITY;
                const NEGATIVE_INFINITY: Self = <$t>::NEG_INFINITY;
                const EPSILON: Self = <$t>::EPSILON;

                #[inline(always)]
                fn sqrt(self) -> Self {
                    <$t>::sqrt(self)
                }

                #[inline(always)]
                fn recip(self) -> Self {
                    <$t>::recip(self)
                }

                #[inline(always)]
                fn powf(self, n: Self) -> Self {
                    <$t>::powf(self, n)
                }

                #[inline(always)]
                fn powi(self, n: i32) -> Self {
                    <$t>::powi(self, n)
                }

                #[inline(always)]
                fn to_radians(self) -> Self {
                    <$t>::to_radians(self)
                }

                #[inline(always)]
                fn to_degrees(self) -> Self {
                    <$t>::to_degrees(self)
                }

                #[inline(always)]
                fn sin(self) -> Self {
                    <$t>::sin(self)
                }

                #[inline(always)]
                fn cos(self) -> Self {
                    <$t>::cos(self)
                }

                #[inline(always)]
                fn tan(self) -> Self {
                    <$t>::tan(self)
                }

                #[inline(always)]
                fn sin_cos(self) -> (Self, Self) {
                    <$t>::sin_cos(self)
                }

                #[inline(always)]
                fn asin(self) -> Self {
                    <$t>::asin(self)
                }

                #[inline(always)]
                fn acos(self) -> Self {
                    <$t>::acos(self)
                }

                #[inline(always)]
                fn atan(self) -> Self {
                    <$t>::atan(self)
                }

                #[inline(always)]
                fn atan2(self, other: Self) -> Self {
                    <$t>::atan2(self, other)
                }

                #[inline(always)]
                fn is_nan(self) -> bool {
                    <$t>::is_nan(self)
                }
            }
        )*
    };
}

unsigned_integral_scalar!(u8, u16, u32, u64, u128, usize);
signed_integral_scalar!(i8, i16, i32, i64, i128, isize);
real_scalar!(f32, core::f32, f64, core::f64);

pub trait CastAs<T> {
    fn cast_as(self) -> T;
}

macro_rules! cast_as {
    ($source:ty $(,$dest:ty)*) => {
        $(
            impl CastAs<$dest> for $source {
                #[inline(always)]
                fn cast_as(self) -> $dest {
                    self as $dest
                }
            }
        )*
    };
}

cast_as!(u8, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(u16, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(u32, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(u64, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(u128, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(usize, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(i8, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(i16, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(i32, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(i64, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(i128, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(isize, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(f32, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);
cast_as!(f64, u8, u16, u32, u64, u128, usize, i8, i16, i32, i64, i128, isize, f32, f64);

#[cfg(feature = "simd")]
pub use simd::*;

#[cfg(feature = "simd")]
pub mod simd {
    use std::simd::prelude::*;
    use std::simd::{SimdElement, LaneCount, SupportedLaneCount, StdFloat};
    use crate::array;
    use super::*;

    pub trait SimdScalar: Scalar + SimdElement {}

    impl<T: Scalar + SimdElement> SimdScalar for T {}

    pub trait SimdOps<const N: usize> where LaneCount<N>: SupportedLaneCount {
        type Element: SimdScalar;

        fn map<U: SimdScalar, F: FnMut(Self::Element) -> U>(self, f: F) -> Simd<U, N>;
        fn map_pair<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element) -> (U, V)>(self, f: F) -> (Simd<U, N>, Simd<V, N>);
        fn map_zip<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element, U) -> V>(self, other: Simd<U, N>, f: F) -> Simd<V, N>;
        fn reduce_sum(self) -> Self::Element;
        fn comp_lt(self, other: Self) -> bool;
        fn comp_le(self, other: Self) -> bool;
        fn comp_gt(self, other: Self) -> bool;
        fn comp_ge(self, other: Self) -> bool;
    }

    impl<T: SimdScalar, const N: usize> ZeroInit for Simd<T, N> where LaneCount<N>: SupportedLaneCount {
        const ZERO: Self = Simd::from_array([T::ZERO; N]);
    }

    macro_rules! signed_integral_scalar_simd {
        ($($t:ty),*) => {
            $(
                impl<const N: usize> SignedScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    const NEGATIVE_ONE: Self = Simd::<$t, N>::from_array([-1; N]);

                    fn abs(self) -> Self {
                        <Simd<$t, N> as SimdInt>::abs(self)
                    }

                    fn signum(self) -> Self {
                        <Simd<$t, N> as SimdInt>::signum(self)
                    }
                }

                impl<const N: usize> SimdOps<N> for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    type Element = $t;

                    fn map<U: SimdScalar, F: FnMut($t) -> U>(self, f: F) -> Simd<U, N> {
                        Simd::from_array(self.to_array().map(f))
                    }

                    fn map_pair<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element) -> (U, V)>(self, f: F) -> (Simd<U, N>, Simd<V, N>) {
                        let array = self.to_array().map(f);
                        (Simd::from_array(array::from_fn(|i| array[i].0)), Simd::from_array(array::from_fn(|i| array[i].1)))
                    }

                    fn map_zip<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element, U) -> V>(self, other: Simd<U, N>, mut f: F) -> Simd<V, N> {
                        Simd::from_array(array::from_fn(|i| f(self[i], other[i])))
                    }

                    #[inline(always)]
                    fn reduce_sum(self) -> Self::Element {
                        <Simd<$t, N> as SimdInt>::reduce_sum(self)
                    }

                    fn comp_lt(self, other: Self) -> bool {
                        self.simd_lt(other).all()
                    }

                    fn comp_le(self, other: Self) -> bool {
                        self.simd_le(other).all()
                    }

                    fn comp_gt(self, other: Self) -> bool {
                        self.simd_gt(other).all()
                    }

                    fn comp_ge(self, other: Self) -> bool {
                        self.simd_ge(other).all()
                    }
                }
            )*

            integral_scalar_simd!($($t),*);
        };
    }

    macro_rules! unsigned_integral_scalar_simd {
        ($($t:ty),*) => {
            $(
                impl<const N: usize> SimdOps<N> for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    type Element = $t;

                    fn map<U: SimdScalar, F: FnMut($t) -> U>(self, f: F) -> Simd<U, N> {
                        Simd::from_array(self.to_array().map(f))
                    }

                    fn map_pair<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element) -> (U, V)>(self, f: F) -> (Simd<U, N>, Simd<V, N>) {
                        let array = self.to_array().map(f);
                        (Simd::from_array(array::from_fn(|i| array[i].0)), Simd::from_array(array::from_fn(|i| array[i].1)))
                    }

                    fn map_zip<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element, U) -> V>(self, other: Simd<U, N>, mut f: F) -> Simd<V, N> {
                        Simd::from_array(array::from_fn(|i| f(self[i], other[i])))
                    }

                    fn reduce_sum(self) -> Self::Element {
                        <Simd<$t, N> as SimdUint>::reduce_sum(self)
                    }

                    fn comp_lt(self, other: Self) -> bool {
                        self.simd_lt(other).all()
                    }

                    fn comp_le(self, other: Self) -> bool {
                        self.simd_le(other).all()
                    }

                    fn comp_gt(self, other: Self) -> bool {
                        self.simd_gt(other).all()
                    }

                    fn comp_ge(self, other: Self) -> bool {
                        self.simd_ge(other).all()
                    }
                }
            )*

            integral_scalar_simd!($($t),*);
        };
    }

    macro_rules! integral_scalar_simd {
        ($($t:ty),*) => {
            $(
                impl<const N: usize> Scalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    const ONE: Self = Simd::<$t, N>::from_array([1; N]);

                    #[inline(always)]
                    fn rem_euclid(self, rhs: Self) -> Self {
                        self.map_zip(rhs, |a, b| a.rem_euclid(b))
                    }

                    #[inline(always)]
                    fn div_euclid(self, rhs: Self) -> Self {
                        self.map_zip(rhs, |a, b| a.div_euclid(b))
                    }
                }

                impl<const N: usize> IntScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    #[inline(always)]
                    fn pow(self, n: u32) -> Self {
                        self.map(|a| a.pow(n))
                    }
                }

                impl<const N: usize> OrdScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    #[inline(always)]
                    fn min(self, other: Self) -> Self {
                        Self::simd_min(self, other)
                    }

                    #[inline(always)]
                    fn max(self, other: Self) -> Self {
                        Self::simd_max(self, other)
                    }

                    #[inline(always)]
                    fn clamp(self, min: Self, max: Self) -> Self {
                        Self::simd_clamp(self, min, max)
                    }
                }
            )*
        };
    }

    macro_rules! real_scalar_simd {
        ($($t:ty, $p:path),*) => {
            $(
                impl<const N: usize> Scalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    const ONE: Self = Simd::<$t, N>::from_array([1.0; N]);

                    #[inline(always)]
                    fn rem_euclid(self, rhs: Self) -> Self {
                        self.map_zip(rhs, |a, b| a.rem_euclid(b))
                    }

                    #[inline(always)]
                    fn div_euclid(self, rhs: Self) -> Self {
                        self.map_zip(rhs, |a, b| a.div_euclid(b))
                    }
                }

                impl<const N: usize> SignedScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    const NEGATIVE_ONE: Self = Simd::<$t, N>::from_array([-1.0; N]);

                    #[inline(always)]
                    fn abs(self) -> Self {
                        <Simd<$t, N> as SimdFloat>::abs(self)
                    }

                    #[inline(always)]
                    fn signum(self) -> Self {
                        <Simd<$t, N> as SimdFloat>::signum(self)
                    }
                }

                impl<const N: usize> OrdScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    #[inline(always)]
                    fn min(self, other: Self) -> Self {
                        Simd::<$t, N>::simd_min(self, other)
                    }

                    #[inline(always)]
                    fn max(self, other: Self) -> Self {
                        Simd::<$t, N>::simd_max(self, other)
                    }

                    #[inline(always)]
                    fn clamp(self, min: Self, max: Self) -> Self {
                        Simd::<$t, N>::simd_clamp(self, min, max)
                    }
                }

                impl<const N: usize> FloatScalar for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    const PI: Self = Simd::<$t, N>::from_array([{ use $p as base; base::consts::PI }; N]);
                    const E: Self = Simd::<$t, N>::from_array([{ use $p as base; base::consts::E }; N]);
                    const POSITIVE_INFINITY: Self = Simd::<$t, N>::from_array([<$t>::INFINITY; N]);
                    const NEGATIVE_INFINITY: Self = Simd::<$t, N>::from_array([<$t>::NEG_INFINITY; N]);
                    const EPSILON: Self = Simd::<$t, N>::from_array([<$t>::EPSILON; N]);

                    #[inline(always)]
                    fn sqrt(self) -> Self {
                        <Simd::<$t, N> as StdFloat>::sqrt(self)
                    }

                    #[inline(always)]
                    fn recip(self) -> Self {
                        <Simd<$t, N> as SimdFloat>::recip(self)
                    }

                    #[inline(always)]
                    fn powf(self, n: Self) -> Self {
                        self.map_zip(n, |a, b| a.powf(b))
                    }

                    #[inline(always)]
                    fn powi(self, n: i32) -> Self {
                        self.map(|a| a.powi(n))
                    }

                    #[inline(always)]
                    fn to_radians(self) -> Self {
                        <Simd<$t, N> as SimdFloat>::to_radians(self)
                    }

                    #[inline(always)]
                    fn to_degrees(self) -> Self {
                        <Simd<$t, N> as SimdFloat>::to_degrees(self)
                    }

                    #[inline(always)]
                    fn sin(self) -> Self {
                        self.map(|v| v.sin())
                    }

                    #[inline(always)]
                    fn cos(self) -> Self {
                        self.map(|v| v.cos())
                    }

                    #[inline(always)]
                    fn tan(self) -> Self {
                        self.map(|v| v.tan())
                    }

                    #[inline(always)]
                    fn sin_cos(self) -> (Self, Self) {
                        self.map_pair(|v| v.sin_cos())
                    }

                    #[inline(always)]
                    fn asin(self) -> Self {
                        self.map(|v| v.asin())
                    }

                    #[inline(always)]
                    fn acos(self) -> Self {
                        self.map(|v| v.acos())
                    }

                    #[inline(always)]
                    fn atan(self) -> Self {
                        self.map(|v| v.atan())
                    }

                    #[inline(always)]
                    fn atan2(self, other: Self) -> Self {
                        self.map_zip(other, |a, b| a.atan2(b))
                    }

                    fn is_nan(self) -> bool {
                        SimdFloat::is_nan(self).any()
                    }
                }

                impl<const N: usize> SimdOps<N> for Simd<$t, N> where LaneCount<N>: SupportedLaneCount {
                    type Element = $t;

                    fn map<U: SimdScalar, F: FnMut($t) -> U>(self, f: F) -> Simd<U, N> {
                        Simd::from_array(self.to_array().map(f))
                    }

                    fn map_pair<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element) -> (U, V)>(self, f: F) -> (Simd<U, N>, Simd<V, N>) {
                        let array = self.to_array().map(f);
                        (Simd::from_array(array::from_fn(|i| array[i].0)), Simd::from_array(array::from_fn(|i| array[i].1)))
                    }

                    fn map_zip<U: SimdScalar, V: SimdScalar, F: FnMut(Self::Element, U) -> V>(self, other: Simd<U, N>, mut f: F) -> Simd<V, N> {
                        Simd::from_array(array::from_fn(|i| f(self[i], other[i])))
                    }

                    fn reduce_sum(self) -> Self::Element {
                        <Simd<$t, N> as SimdFloat>::reduce_sum(self)
                    }

                    fn comp_lt(self, other: Self) -> bool {
                        self.simd_lt(other).all()
                    }

                    fn comp_le(self, other: Self) -> bool {
                        self.simd_le(other).all()
                    }

                    fn comp_gt(self, other: Self) -> bool {
                        self.simd_gt(other).all()
                    }

                    fn comp_ge(self, other: Self) -> bool {
                        self.simd_ge(other).all()
                    }
                }
            )*
        };
    }

    unsigned_integral_scalar_simd!(u8, u16, u32, u64, usize);
    signed_integral_scalar_simd!(i8, i16, i32, i64, isize);
    real_scalar_simd!(f32, core::f32, f64, core::f64);
}