use std::fmt;
use std::marker::PhantomData;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{DeserializeSeed, Error, SeqAccess, Visitor};
use serde::ser::SerializeTupleStruct;
use crate::{Matrix, Vector};

/// A DeserializeSeed helper for implementing deserialize_in_place Visitors.
///
/// Wraps a mutable reference and calls deserialize_in_place on it.
pub struct InPlaceSeed<'a, T: 'a>(pub &'a mut T);

impl<'a, 'de, T> DeserializeSeed<'de> for InPlaceSeed<'a, T>
    where
        T: Deserialize<'de>,
{
    type Value = ();
    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: Deserializer<'de>,
    {
        T::deserialize_in_place(deserializer, self.0)
    }
}

impl<T: Serialize, const N: usize> Serialize for Vector<T, N> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let mut tuple = serializer.serialize_tuple_struct("Vector", N)?;
        for x in self.as_array() {
            tuple.serialize_field(x)?;
        }
        tuple.end()
    }
}

struct VectorVisitor<T: 'static, const N: usize> {
    marker: PhantomData<Vector<T, N>>,
}

struct VectorInPlaceVisitor<'a, T: 'static, const N: usize>(&'a mut Vector<T, N>);

impl<T, const N: usize> VectorVisitor<T, N> {
    fn new() -> Self {
        VectorVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, T, const N: usize> Visitor<'de> for VectorVisitor<T, N> where T: Deserialize<'de>, {
    type Value = Vector<T, N>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a vector of length {}", N))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de>, {
        Vector::try_from_fn(|i| seq.next_element()?.ok_or(Error::invalid_length(i, &self)))
    }
}

impl<'a, 'de, T, const N: usize> Visitor<'de> for VectorInPlaceVisitor<'a, T, N> where T: Deserialize<'de> {
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a vector of length {}", N))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de> {
        let mut fail_idx = None;
        for (idx, dest) in self.0.iter_mut().enumerate() {
            if seq.next_element_seed(InPlaceSeed(dest))?.is_none() {
                fail_idx = Some(idx);
                break;
            }
        }
        if let Some(idx) = fail_idx {
            return Err(Error::invalid_length(idx, &self));
        }
        Ok(())
    }
}

impl<'de, T, const N: usize> Deserialize<'de> for Vector<T, N> where T: Deserialize<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("Vector", N, VectorVisitor::<T, N>::new())
    }

    fn deserialize_in_place<D>(deserializer: D, place: &mut Self) -> Result<(), D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("Vector", N, VectorInPlaceVisitor(place))
    }
}

impl<T: Serialize, const R: usize, const C: usize> Serialize for Matrix<T, R, C> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let mut tuple = serializer.serialize_tuple_struct("Matrix", C)?;
        for x in self.as_column_vectors() {
            tuple.serialize_field(x)?;
        }
        tuple.end()
    }
}

struct MatrixVisitor<T: 'static, const R: usize, const C: usize> {
    marker: PhantomData<Matrix<T, R, C>>,
}

struct MatrixInPlaceVisitor<'a, T: 'static, const R: usize, const C: usize>(&'a mut Matrix<T, R, C>);

impl<T, const R: usize, const C: usize> MatrixVisitor<T, R, C> {
    fn new() -> Self {
        MatrixVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, T, const R: usize, const C: usize> Visitor<'de> for MatrixVisitor<T, R, C> where T: Deserialize<'de>, {
    type Value = Matrix<T, R, C>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a matrix of size ({}, {})", R, C))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de>, {
        Matrix::try_from_fn_column_vectors(|c| seq.next_element()?.ok_or(Error::invalid_length(c, &self)))
    }
}

impl<'a, 'de, T, const R: usize, const C: usize> Visitor<'de> for MatrixInPlaceVisitor<'a, T, R, C> where T: Deserialize<'de> {
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a matrix of size ({}, {})", R, C))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de> {
        let mut fail_idx = None;
        for (idx, dest) in self.0.as_column_vectors_mut().iter_mut().enumerate() {
            if seq.next_element_seed(InPlaceSeed(dest))?.is_none() {
                fail_idx = Some(idx);
                break;
            }
        }
        if let Some(idx) = fail_idx {
            return Err(Error::invalid_length(idx, &self));
        }
        Ok(())
    }
}

impl<'de, T, const R: usize, const C: usize> Deserialize<'de> for Matrix<T, R, C> where T: Deserialize<'de> {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("Matrix", C, MatrixVisitor::<T, R, C>::new())
    }

    fn deserialize_in_place<D>(deserializer: D, place: &mut Self) -> Result<(), D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("Matrix", C, MatrixInPlaceVisitor(place))
    }
}

mod tests {
    #[test]
    pub fn test() {
        let m: crate::Matrix<_, 3, 4> = crate::matrix![
            0.0, 1.0, 2.0, 3.0,
            4.0, 5.0, 6.0, 7.0,
            8.0, 9.0, 10.0, 11.0
        ];
        dbg!(m);
        let s = serde_json::to_string(&m).unwrap();
        println!("{}", s);
        let m_2: crate::Matrix<f64, 3, 4> = serde_json::from_str(&s).unwrap();
        dbg!(m_2);
        assert_eq!(m, m_2);

        let q = crate::Quaternion::new(1.0, crate::Vector3::new(2.0, 3.0, 4.0));
        dbg!(q);
        let s = serde_json::to_string(&q).unwrap();
        println!("{}", s);
        let q_2: crate::Quaternion<f64> = serde_json::from_str(&s).unwrap();
        dbg!(q_2);
        assert_eq!(q, q_2);
    }
}