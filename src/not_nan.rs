use std::borrow::Borrow;
use std::cmp::Ordering;
use std::fmt::{Debug, Display, Formatter};
use std::hash::{Hash, Hasher};
use std::ops::{Add, AddAssign, Deref, Div, DivAssign, Mul, MulAssign, Neg, Rem, RemAssign, Sub, SubAssign};
use thiserror::Error;
use crate::{CastAs, FloatScalar, OrdScalar, Scalar, SignedScalar, ZeroInit};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[derive(Error, Debug)]
#[error("Can not create NotNan from NaN")]
pub struct ValueIsNan;

unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for NotNan<T> {}
unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for NotNan<T> {}

#[derive(Copy, Clone, PartialOrd, PartialEq, Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
#[repr(transparent)]
pub struct NotNan<T>(T);

pub type F32 = NotNan<f32>;
pub type F64 = NotNan<f64>;

impl<T: FloatScalar> NotNan<T> {
    pub fn new(value: T) -> Result<NotNan<T>, ValueIsNan> {
        if value.is_nan() {
            Err(ValueIsNan)
        } else {
            Ok(NotNan(value))
        }
    }

    pub fn into_inner(self) -> T {
        self.0
    }
}

#[macro_export]
macro_rules! not_nan {
    ($val:literal) => {
        unsafe { $crate::NotNan::new($val).unwrap_unchecked() }
    };
}

impl<T: FloatScalar> ZeroInit for NotNan<T> {
    const ZERO: Self = NotNan(T::ZERO);
}

impl<T: FloatScalar> Add<Self> for NotNan<T> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        NotNan(self.0 + rhs.0) // Always valid
    }
}

impl<T: FloatScalar> AddAssign for NotNan<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0; // Always valid
    }
}

impl<T: FloatScalar> Sub<Self> for NotNan<T> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        NotNan(self.0 - rhs.0) // Always valid
    }
}

impl<T: FloatScalar> SubAssign for NotNan<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 -= rhs.0; // Always valid
    }
}

impl<T: FloatScalar> Mul<Self> for NotNan<T> {
    type Output = Self;

    fn mul(self, rhs: Self) -> Self::Output {
        NotNan(self.0 * rhs.0) // Always valid
    }
}

impl<T: FloatScalar> MulAssign for NotNan<T> {
    fn mul_assign(&mut self, rhs: Self) {
        self.0 *= rhs.0; // Always valid
    }
}

impl<T: FloatScalar> Div<Self> for NotNan<T> {
    type Output = Self;

    fn div(self, rhs: Self) -> Self::Output {
        NotNan::new(self.0 / rhs.0).unwrap() // Failable
    }
}

impl<T: FloatScalar> DivAssign for NotNan<T> {
    fn div_assign(&mut self, rhs: Self) {
        *self = *self / rhs;
    }
}

impl<T: FloatScalar> Rem<Self> for NotNan<T> {
    type Output = Self;

    fn rem(self, rhs: Self) -> Self::Output {
        NotNan::new(self.0 % rhs.0).unwrap() // Failable
    }
}

impl<T: FloatScalar> RemAssign for NotNan<T> {
    fn rem_assign(&mut self, rhs: Self) {
        *self = *self % rhs;
    }
}

impl<T: FloatScalar> Neg for NotNan<T> {
    type Output = Self;

    fn neg(self) -> Self::Output {
        NotNan(-self.0) // Always valid
    }
}

impl<T: PartialEq> Eq for NotNan<T> {}

impl<T: PartialOrd> Ord for NotNan<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        unsafe { self.0.partial_cmp(&other.0).unwrap_unchecked() }
    }
}

impl<T: FloatScalar> Scalar for NotNan<T> {
    const ONE: Self = NotNan(T::ONE);

    fn rem_euclid(self, rhs: Self) -> Self {
        NotNan::new(self.0.rem_euclid(rhs.0)).unwrap() // Failable
    }

    fn div_euclid(self, rhs: Self) -> Self {
        NotNan::new(self.0.div_euclid(rhs.0)).unwrap() // Failable
    }
}

impl<T: FloatScalar> SignedScalar for NotNan<T> {
    const NEGATIVE_ONE: Self = NotNan(T::NEGATIVE_ONE);

    fn abs(self) -> Self {
        NotNan(self.0.abs())
    }

    fn signum(self) -> Self {
        NotNan(self.0.signum())
    }
}

impl<T: FloatScalar> OrdScalar for NotNan<T> {
    fn min(self, other: Self) -> Self {
        NotNan(self.0.min(other.0))
    }

    fn max(self, other: Self) -> Self {
        NotNan(self.0.max(other.0))
    }

    fn clamp(self, min: Self, max: Self) -> Self {
        NotNan(self.0.clamp(min.0, max.0))
    }
}

impl<T: FloatScalar> FloatScalar for NotNan<T> {
    const PI: Self = NotNan(T::PI);
    const E: Self = NotNan(T::E);
    const POSITIVE_INFINITY: Self = NotNan(T::POSITIVE_INFINITY);
    const NEGATIVE_INFINITY: Self = NotNan(T::NEGATIVE_INFINITY);
    const EPSILON: Self = NotNan(T::EPSILON);

    fn sqrt(self) -> Self {
        NotNan::new(self.0.sqrt()).unwrap() // Failable
    }

    fn recip(self) -> Self {
        NotNan::new(self.0.recip()).unwrap() // Failable
    }

    fn powf(self, n: Self) -> Self {
        NotNan::new(self.0.powf(n.0)).unwrap() // Failable
    }

    fn powi(self, n: i32) -> Self {
        NotNan::new(self.0.powi(n)).unwrap() // Failable
    }

    fn to_radians(self) -> Self {
        NotNan(self.0.to_radians())
    }

    fn to_degrees(self) -> Self {
        NotNan(self.0.to_degrees())
    }

    fn sin(self) -> Self {
        NotNan::new(self.0.sin()).unwrap() // Failable
    }

    fn cos(self) -> Self {
        NotNan::new(self.0.cos()).unwrap() // Failable
    }

    fn tan(self) -> Self {
        NotNan::new(self.0.tan()).unwrap() // Failable
    }

    fn sin_cos(self) -> (Self, Self) {
        let (s, c) = self.0.sin_cos();
        (NotNan::new(s).unwrap(), NotNan::new(c).unwrap()) // Failable
    }

    fn asin(self) -> Self {
        NotNan::new(self.0.asin()).unwrap() // Failable
    }

    fn acos(self) -> Self {
        NotNan::new(self.0.acos()).unwrap() // Failable
    }

    fn atan(self) -> Self {
        NotNan::new(self.0.atan()).unwrap() // Failable
    }

    fn atan2(self, other: Self) -> Self {
        NotNan::new(self.0.atan2(other.0)).unwrap() // Failable
    }

    fn is_nan(self) -> bool {
        false
    }
}

impl<T: FloatScalar> AsRef<T> for NotNan<T> {
    fn as_ref(&self) -> &T {
        &self.0
    }
}

impl<T: FloatScalar> Borrow<T> for NotNan<T> {
    #[inline]
    fn borrow(&self) -> &T {
        &self.0
    }
}

impl<T: FloatScalar> Deref for NotNan<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<NotNan<f32>> for f32 {
    fn from(value: NotNan<f32>) -> Self {
        value.0
    }
}

impl From<NotNan<f64>> for f64 {
    fn from(value: NotNan<f64>) -> Self {
        value.0
    }
}

impl TryFrom<f32> for NotNan<f32> {
    type Error = ValueIsNan;

    fn try_from(value: f32) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

impl TryFrom<f64> for NotNan<f64> {
    type Error = ValueIsNan;

    fn try_from(value: f64) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

impl<T: FloatScalar, V: FloatScalar> CastAs<NotNan<V>> for NotNan<T> where T: CastAs<V> {
    fn cast_as(self) -> NotNan<V> {
        NotNan(self.0.cast_as())
    }
}

impl Hash for NotNan<f32> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        bytemuck::cast_ref::<_, u32>(&self.0).hash(state);
    }
}

impl Hash for NotNan<f64> {
    fn hash<H: Hasher>(&self, state: &mut H) {
        bytemuck::cast_ref::<_, u64>(&self.0).hash(state);
    }
}

impl<T: Display> Display for NotNan<T> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}