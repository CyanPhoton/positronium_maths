use std::ops::{Bound, Range, RangeFrom, RangeFull, RangeInclusive, RangeTo, RangeToInclusive};
use crate::IntScalar;

pub trait RangeBounds<I> {
    fn start_bound(&self) -> Bound<&I>;
    fn end_bound(&self) -> Bound<&I>;
    // Inclusive
    fn start(&self) -> I where I: IntScalar {
        match self.start_bound() {
            Bound::Included(i) => *i,
            Bound::Excluded(i) => *i + I::ONE,
            Bound::Unbounded => I::ZERO
        }
    }
    // Exclusive
    fn end(&self, total_len: I) -> I where I: IntScalar {
        match self.end_bound() {
            Bound::Included(i) => *i + I::ONE,
            Bound::Excluded(i) => *i,
            Bound::Unbounded => total_len
        }
    }
}

macro_rules! impl_range_bounds {
    ($var:ty $(,$l:lifetime)*) => {
        impl<$($l,)* I> RangeBounds<I> for $var {
            fn start_bound(&self) -> Bound<&I> {
                core::ops::RangeBounds::start_bound(self)
            }

            fn end_bound(&self) -> Bound<&I> {
                core::ops::RangeBounds::end_bound(self)
            }
        }
    };
}

impl_range_bounds!(Range<&I>);
impl_range_bounds!(RangeFrom<&I>);
impl_range_bounds!(RangeInclusive<&I>);
impl_range_bounds!(RangeTo<&I>);
impl_range_bounds!(RangeToInclusive<&I>);
impl_range_bounds!((Bound<&'a I>, Bound<&'a I>), 'a);

impl_range_bounds!((Bound<I>, Bound<I>));
impl_range_bounds!(Range<I>);
impl_range_bounds!(RangeFrom<I>);
impl_range_bounds!(RangeFull);
impl_range_bounds!(RangeInclusive<I>);
impl_range_bounds!(RangeTo<I>);
impl_range_bounds!(RangeToInclusive<I>);
