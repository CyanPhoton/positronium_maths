use crate::{Matrix3, Matrix4, FloatScalar, Quaternion, UnitQuaternion, UnitVector, UnitVector3, vector, Vector3, Vector4, VectorSlice};

impl<T: FloatScalar> Vector3<T> {
    pub fn rotate(self, q: UnitQuaternion<T>) -> Vector3<T> {
        (q * Quaternion::vector(self) * q.inverse()).xyz
    }

    pub fn rotate_inverse(self, inverse_q: UnitQuaternion<T>) -> Vector3<T> {
        (inverse_q.inverse() * Quaternion::vector(self) * inverse_q).xyz
    }

    pub fn rotate_assign(&mut self, q: UnitQuaternion<T>) {
        *self = self.rotate(q);
    }

    pub fn rotate_inverse_assign(&mut self, inverse_q: UnitQuaternion<T>) {
        *self = self.rotate_inverse(inverse_q);
    }
}

impl<T: FloatScalar> UnitVector3<T> {
    pub fn rotate(self, q: UnitQuaternion<T>) -> UnitVector3<T> {
        UnitVector((q * Quaternion::vector(self.0) * q.inverse()).xyz)
    }

    pub fn rotate_inverse(self, inverse_q: UnitQuaternion<T>) -> UnitVector3<T> {
        UnitVector((inverse_q.inverse() * Quaternion::vector(self.0) * inverse_q).xyz)
    }

    pub fn rotate_assign(&mut self, q: UnitQuaternion<T>) {
        *self = self.rotate(q);
    }

    pub fn rotate_inverse_assign(&mut self, inverse_q: UnitQuaternion<T>) {
        *self = self.rotate_inverse(inverse_q);
    }
}

impl<T: FloatScalar, const N: usize> VectorSlice<T, N> {
    // Slice length must be 3
    pub fn rotate_assign(&mut self, q: UnitQuaternion<T>) {
        self.set(self.vector::<3>().rotate(q));
    }

    pub fn rotate_inverse_assign(&mut self, inverse_q: UnitQuaternion<T>) {
        self.set(self.vector::<3>().rotate_inverse(inverse_q));
    }
}

impl<T: FloatScalar> Vector4<T> {
    pub fn space_rotate(self, q: UnitQuaternion<T>) -> Vector4<T> {
        self.space().rotate(q).push(self.w)
    }

    pub fn space_rotate_inverse(self, inverse_q: UnitQuaternion<T>) -> Vector4<T> {
        self.space().rotate_inverse(inverse_q).push(self.w)
    }

    pub fn space_rotate_assign(&mut self, q: UnitQuaternion<T>) {
        *self = self.space_rotate(q);
    }

    pub fn space_rotate_inverse_assign(&mut self, inverse_q: UnitQuaternion<T>) {
        *self = self.space_rotate_inverse(inverse_q);
    }
}


impl<T: FloatScalar> Matrix3<T> {
    pub fn rotate_from_quaternion(q: UnitQuaternion<T>) -> Matrix3<T> {
        // Using: https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Conversion_to_and_from_the_matrix_representation
        // But also see https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Quaternion-derived_rotation_matrix
        // or https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

        let two: T = T::ONE + T::ONE;

        let [a, b, c, d] = q.into_array();
        let [bs, cs, ds] = (q.xyz * two).into_array();
        let ab = a * bs;
        let ac = a * cs;
        let ad = a * ds;
        let bb = b * bs;
        let bc = b * cs;
        let bd = b * ds;
        let cc = c * cs;
        let cd = c * ds;
        let dd = d * ds;

        Matrix3::rows([
            [T::ONE - cc - dd, bc - ad, bd + ac],
            [bc + ad, T::ONE - bb - dd, cd - ab],
            [bd - ac, cd + ab, T::ONE - bb - cc]
        ])
    }

    pub fn rotate_to_quaternion(self) -> Quaternion<T> {
        // Using: https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion

        let two: T = T::ONE + T::ONE;

        let t = self.trace();
        if t >= T::ZERO {
            let r = (T::ONE + t).sqrt();
            let two_r = two * r;
            let s = two_r.recip();

            let scalar = r / two;
            let i = (self[2][1] - self[1][2]) * s;
            let j = (self[0][2] - self[2][0]) * s;
            let k = (self[1][0] - self[0][1]) * s;

            Quaternion::new(scalar, vector![i, j, k])
        } else {
            let largest = if self[0][0] >= self[1][1] {
                if self[0][0] >= self[2][2] {
                    0
                } else {
                    2
                }
            } else {
                if self[1][1] >= self[2][2] {
                    1
                } else {
                    2
                }
            };

            let n1 = largest;
            let n2 = (largest + 1) % 3;
            let n3 = (largest + 2) % 3;

            let t = self[n1][n1] - self[n2][n2] - self[n3][n3];
            let r = (T::ONE + t).sqrt();
            let two_r = two * r;
            let s = two_r.recip();

            let scalar = (self[n3][n2] - self[n2][n3]) * s;
            let mut vector = Vector3::zero();
            vector[n1] = r / two;
            vector[n2] = (self[n1][n2] + self[n2][n1]) * s;
            vector[n3] = (self[n3][n1] + self[n1][n3]) * s;

            Quaternion::new(scalar, vector)
        }
    }
}

impl<T: FloatScalar> Matrix4<T> {
    pub fn space_rotate_from_quaternion(q: UnitQuaternion<T>) -> Matrix4<T> {
        Matrix3::rotate_from_quaternion(q).to_homogeneous(T::ONE)
    }

    pub fn space_rotate_to_quaternion(self) -> Quaternion<T> {
        self[0..3][0..3].matrix::<3, 3>().rotate_to_quaternion()
    }
}

#[cfg(feature = "simd")]
impl<T: FloatScalar + crate::SimdScalar> crate::simd::SimdMatrix<T, 4, 4> {
    pub fn space_rotate_from_quaternion(q: UnitQuaternion<T>) -> crate::simd::SimdMatrix<T, 4, 4> {
        Matrix3::rotate_from_quaternion(q).to_homogeneous_simd(T::ONE)
    }
}

#[cfg(test)]
mod tests {
    #![allow(unused_variables)]

    use rand::distributions::Distribution;
    use rand::SeedableRng;

    use crate::{Matrix, Radians, UnitQuaternion, UnitVector, vector, Vector};

    #[test]
    pub fn test() {
        let mut rand = rand::rngs::StdRng::seed_from_u64(0);
        let theta_range = rand::distributions::Uniform::new(0.0f32, 2.0f32 * std::f32::consts::PI);
        let omega_range = rand::distributions::Uniform::new(0.0f32, 2.0f32 * std::f32::consts::PI);
        let phi_range = rand::distributions::Uniform::new_inclusive(-std::f32::consts::PI, std::f32::consts::PI);
        let scale_range = rand::distributions::Uniform::new_inclusive(0.5f32, 2.5f32);
        let vec_range = rand::distributions::Uniform::new_inclusive(-10.0f32, 10.0f32);

        for _ in 0..1_000_000 {
            let theta = theta_range.sample(&mut rand);
            let omega = omega_range.sample(&mut rand);
            let phi = phi_range.sample(&mut rand);
            let scale = scale_range.sample(&mut rand);

            let axis_z = phi.sin();
            let axis_xy = phi.cos();
            let axis_x = axis_xy * theta.cos();
            let axis_y = axis_xy * theta.sin();

            let v0 = Vector::<f32, 3>::from_fn(|_| vec_range.sample(&mut rand));

            let uq = UnitQuaternion::rotation(Radians::new(omega), vector![axis_x, axis_y, axis_z].unit());
            // let v1 = v0.rotate(uq * scale);

            let q = UnitQuaternion::rotation(Radians::new(omega), vector![axis_x, axis_y, axis_z].unit());

            let qm = Matrix::<f32, 3, 3, >::rotate_from_quaternion(q);
            let v2 = qm * v0;

            let q2 = qm.rotate_to_quaternion().unit();
            let v3 = v0.rotate(q2);

            let r1: UnitVector<f32, 3> = uq.rotation_axis().unwrap_or(UnitVector::<f32, 3>::unit_x());
            let mut r2: UnitVector<f32, 3> = q2.rotation_axis().unwrap_or(UnitVector::<f32, 3>::unit_x());

            let a1 = uq.rotation_angle().value();
            let mut a2 = q2.rotation_angle().value();

            if r1 * r2 < 0.0 {
                r2 = -r2;
                a2 = -a2;
                if a2 < 0.0 && a1 > 0.0 {
                    a2 += 2.0 * std::f32::consts::PI;
                }
            }

            let t1: Vector<_, 4> = r1.push(a1);
            let t2: Vector<_, 4> = r2.push(a2);

            // println!("{v0} -> {v1} vs {v2} vs {v3}");
            // println!("\t[{}] [{}] [{}] [{}]", v0.norm_squared(), v1.norm_squared(), v2.norm_squared(), v3.norm_squared());
            assert!(uq == q2 || (t2 - t1).norm_squared() < 0.000001, "Assert {} == {}", uq, q2);
        }
    }
}