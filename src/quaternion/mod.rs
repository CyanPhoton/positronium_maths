pub use quaternion::*;
pub use unit_quaternion::*;

pub mod quaternion;
pub mod unit_quaternion;
pub mod quaternion_transformations;

#[cfg(test)]
mod tests {
    use crate::{Degrees, UnitVector3f, Vector3, Vector3f};
    use crate::quaternion::{Quaternion, UnitQuaternion};

    #[test]
    pub fn quaternion_interpolation_tests() {
        {
            let a = Quaternion::identity().powf(2.0);
            assert_eq!(a, Quaternion::identity());

            assert_eq!(a.interpolate(a, 0.1245), a);
        }
        {
            let a = UnitQuaternion::identity().powf(2.0);
            assert_eq!(a, UnitQuaternion::identity());

            assert_eq!(a.interpolate(a, 0.1245), a);
        }
    }

    #[test]
    pub fn quaternion_rotate_test() {
        {
            let r = UnitQuaternion::rotation(Degrees::new(90.0), UnitVector3f::unit_x());

            dbg!(Vector3f::unit_x().rotate(r));
            dbg!(Vector3f::unit_y().rotate(r));
            dbg!(Vector3f::unit_z().rotate(r));
        }
    }

    #[test]
    pub fn quaternion_test() {
        {
            let axis = Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(165.0).to_radians();

            let q = Quaternion::rotation(angle, axis);

            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = -Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(165.0).to_radians();

            let q = Quaternion::rotation(angle, axis);

            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(-165.0).to_radians();

            let q = Quaternion::rotation(angle, axis);

            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = -Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(-165.0).to_radians();

            let q = Quaternion::rotation(angle, axis);

            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
    }

    #[test]
    pub fn unit_quaternion_test() {
        {
            let axis = Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(165.0).to_radians();

            let q = UnitQuaternion::rotation(angle, axis);

            dbg!((axis, angle.normalise(), q.rotation_axis(), q.rotation_angle().normalise()));
            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = -Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(165.0).to_radians();

            let q = UnitQuaternion::rotation(angle, axis);

            dbg!((axis, angle.normalise(), q.rotation_axis(), q.rotation_angle().normalise()));
            assert!((q.rotation_angle().normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(-165.0).to_radians();

            let q = UnitQuaternion::rotation(angle, axis).restrict();

            // NOTE: Negating rotation_axis() and rotation_angle() because with restrict(),
            // there is no way to give anything but a positive angle, and here we supply -ve,
            // which instead will result in the axis being negated.
            dbg!((axis, angle.normalise(), q.rotation_axis().map(|q| -q), (-q.rotation_angle()).normalise()));
            assert!(((-q.rotation_angle()).normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((-q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
        {
            let axis = -Vector3::new(1.0f64, 2.0, 3.0).unit();
            let angle = Degrees::new(-165.0).to_radians();

            let q = UnitQuaternion::rotation(angle, axis).restrict();

            // NOTE: Negating rotation_axis() and rotation_angle() because with restrict(),
            // there is no way to give anything but a positive angle, and here we supply -ve,
            // which instead will result in the axis being negated.
            dbg!((axis, angle.normalise(), q.rotation_axis().map(|q| -q), (-q.rotation_angle()).normalise()));
            assert!(((-q.rotation_angle()).normalise().value() - angle.normalise().value()).abs() < 1.0e-10);
            assert!((-q.rotation_axis().unwrap() - axis).norm() < 1.0e-10);
        }
    }
}