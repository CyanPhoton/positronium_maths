use std::fmt::{Debug, Display, Formatter};
use std::hash::Hash;
use std::ops::{Add, AddAssign, Deref, DerefMut, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

use crate::{CastAs, Radians, FloatScalar, Scalar, SignedScalar, UnitQuaternion, UnitVector3, Vector3, Vector4, ZeroInit, NotNan};

#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Quaternion<T: 'static>(Vector4<T>);

/// Safety: Is repr(transparent) and single field is Zeroable
unsafe impl<T: bytemuck::Zeroable> bytemuck::Zeroable for Quaternion<T> {}

/// Safety: Is repr(transparent) and single field is Pod
unsafe impl<T: bytemuck::Pod> bytemuck::Pod for Quaternion<T> {}

/// Safety: Is repr(transparent) and single field is NoUninit
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for Quaternion<NotNan<T>> {}

impl<T: ZeroInit + Copy> ZeroInit for Quaternion<T> {
    const ZERO: Self = Self(ZeroInit::ZERO);
}

impl<T> Quaternion<T> {
    pub const fn zero() -> Quaternion<T> where T: ZeroInit + Copy {
        Quaternion(Vector4::ZERO)
    }

    pub fn new(scalar: T, vector: Vector3<T>) -> Quaternion<T> {
        let [x, y, z] = vector.values;
        Quaternion(Vector4 {
            values: [x, y, z, scalar],
        })
    }

    /// Construct the array of [scalar component, imaginary component x,y,z]
    pub fn array([r, i, j, k]: [T; 4]) -> Self {
        Quaternion(Vector4::new(i, j, k, r))
    }

    pub fn tuple((r, i, j, k): (T, T, T, T)) -> Self {
        Quaternion(Vector4::new(i, j, k, r))
    }

    pub const fn scalar(scalar: T) -> Quaternion<T> where T: ZeroInit {
        Quaternion(Vector4::new(T::ZERO, T::ZERO, T::ZERO, scalar))
    }

    pub fn vector(vector: Vector3<T>) -> Quaternion<T> where T: ZeroInit {
        let [x, y, z] = vector.values;
        Quaternion(Vector4 {
            values: [x, y, z, T::ZERO],
        })
    }

    pub fn rotation<R>(angle: R, axis: UnitVector3<T>) -> Quaternion<T> where R: Into<Radians<T>>, T: FloatScalar {
        let radians = angle.into();
        let (s, c) = (radians / (T::ONE + T::ONE)).sin_cos();

        let axis = axis.into_vector();

        // Don't flip axis, flip angle instead
        Quaternion::new(c * s.signum(), axis * s.abs())
    }

    pub fn map<V, F: FnMut(T) -> V>(self, map: F) -> Quaternion<V> {
        Quaternion(self.0.map(map))
    }

    pub fn try_map<V, E, F: FnMut(T) -> Result<V, E>>(self, map: F) -> Result<Quaternion<V>, E> {
        Ok(Quaternion(self.0.try_map(map)?))
    }

    pub fn zip_map<U, V, F: FnMut(T, V) -> U>(self, other: Quaternion<V>, map: F) -> Quaternion<U> {
        Quaternion(self.0.zip_map(other.0, map))
    }

    pub fn cast_as<V>(self) -> Quaternion<V> where T: CastAs<V> {
        Quaternion(self.0.cast_as())
    }

    pub fn comp_into<V>(self) -> Quaternion<V> where T: Into<V> {
        self.map(|v| v.into())
    }

    pub fn comp_try_into<V, E>(self) -> Result<Quaternion<V>, E> where T: TryInto<V, Error = E> {
        self.try_map(|v| v.try_into())
    }

    /// Convert into the array [r, i, j, k]
    pub fn into_array(self) -> [T; 4] {
        let [i, j, k, r] = self.0.into_array();
        [r, i, j, k]
    }
}

impl<T: Scalar> Quaternion<T> {
    pub const fn identity() -> Quaternion<T> {
        Self::one()
    }

    pub const fn one() -> Quaternion<T> {
        Quaternion::scalar(T::ONE)
    }

    pub fn i() -> Quaternion<T> {
        Quaternion::vector(Vector3::unit_x())
    }

    pub fn j() -> Quaternion<T> {
        Quaternion::vector(Vector3::unit_y())
    }

    pub fn k() -> Quaternion<T> {
        Quaternion::vector(Vector3::unit_z())
    }

    pub fn inner_product(self, rhs: Self) -> T {
        self.0.inner_product(rhs.0)
    }

    pub fn norm_squared(self) -> T {
        self.inner_product(self)
    }

    pub fn norm(self) -> T where T: FloatScalar {
        self.norm_squared().sqrt()
    }

    pub fn rotation_angle(self) -> Radians<T> where T: FloatScalar {
        let c = self.r;
        let s = self.ijk.norm();

        Radians::atan2(s, c) * (T::ONE + T::ONE)
    }

    pub fn rotation_axis(self) -> Option<UnitVector3<T>> where T: FloatScalar {
        self.ijk.try_unit()
    }

    pub fn unit(self) -> UnitQuaternion<T> where T: FloatScalar {
        UnitQuaternion(self / self.norm())
    }

    pub fn try_unit(self) -> Option<UnitQuaternion<T>> where T: FloatScalar {
        if self != Self::zero() {
            Some(UnitQuaternion(self / self.norm()))
        } else {
            None
        }
    }

    pub fn unit_assign(&mut self) where T: FloatScalar {
        *self = *self / self.norm()
    }

    pub fn conjugate(self) -> Quaternion<T> where T: SignedScalar {
        Quaternion::new(self.r, -self.ijk)
    }

    pub fn conjugate_assign(&mut self) where T: SignedScalar {
        self.xyz = -self.xyz;
    }

    pub fn inverse(self) -> Quaternion<T> where T: FloatScalar {
        self.conjugate() / self.norm_squared()
    }

    pub fn inverse_assign(&mut self) where T: FloatScalar {
        *self = self.conjugate() / self.norm_squared();
    }

    pub fn powf(self, t: T) -> Quaternion<T> where T: FloatScalar {
        let Some(axis) = self.rotation_axis() else {
            return Quaternion::scalar(self.r.abs().powf(t));
        };

        let rot = self.rotation_angle();
        let scale = self.norm();

        Quaternion::rotation(rot * t, axis) * scale.powf(t)
    }

    /// Interpolates in the space of quaternions, may not be the shortest physical rotation
    pub fn interpolate(self, target: Quaternion<T>, fraction: T) -> Quaternion<T> where T: FloatScalar {
        let fraction = fraction.clamp(T::ZERO, T::ONE);
        (target * self.inverse()).powf(fraction) * self
    }

    /// Interpolate in the rotation space, will be the shortest physical rotation
    pub fn interpolate_rotation(self, target: Quaternion<T>, fraction: T) -> Quaternion<T> where T: FloatScalar {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    pub fn extrapolate(self, target: Quaternion<T>, fraction: T) -> Quaternion<T> where T: FloatScalar {
        (target * self.inverse()).powf(fraction) * self
    }

    pub fn extrapolate_rotation(self, target: Quaternion<T>, fraction: T) -> Quaternion<T> where T: FloatScalar {
        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict(self) -> Quaternion<T> where T: SignedScalar + PartialOrd {
        if self.r < T::ZERO {
            -self
        } else {
            self
        }
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict_assign(&mut self) where T: SignedScalar + PartialOrd {
        *self = self.restrict();
    }
}

impl<T> From<(T, Vector3<T>)> for Quaternion<T> {
    fn from(v: (T, Vector3<T>)) -> Self {
        Self::new(v.0, v.1)
    }
}

impl<T> From<Quaternion<T>> for (T, Vector3<T>) {
    fn from(v: Quaternion<T>) -> Self {
        let [r, i, k, j] = v.into_array();
        (r, Vector3::new(i, k, j))
    }
}

// Addition

impl<T: Scalar> Add for Quaternion<T> {
    type Output = Quaternion<T>;

    fn add(self, rhs: Self) -> Self::Output {
        Quaternion(self.0 + rhs.0)
    }
}

impl<T: Scalar> AddAssign for Quaternion<T> {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
    }
}

// Subtraction

impl<T: Scalar> Sub for Quaternion<T> {
    type Output = Quaternion<T>;

    fn sub(self, rhs: Self) -> Self::Output {
        Quaternion(self.0 - rhs.0)
    }
}

impl<T: Scalar> SubAssign for Quaternion<T> {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 -= rhs.0;
    }
}

// Quaternion x Quaternion multiplication

impl<T: SignedScalar> Mul<Quaternion<T>> for Quaternion<T> {
    type Output = Quaternion<T>;

    fn mul(self, rhs: Quaternion<T>) -> Self::Output {
        Quaternion(Vector4::new(
            self.0 * Vector4::new(rhs.r, rhs.k, -rhs.j, rhs.i),
            self.0 * Vector4::new(-rhs.k, rhs.r, rhs.i, rhs.j),
            self.0 * Vector4::new(rhs.j, -rhs.i, rhs.r, rhs.k),
            self.0 * Vector4::new(-rhs.i, -rhs.j, -rhs.k, rhs.r),
        ))
    }
}

impl<T: SignedScalar> MulAssign<Quaternion<T>> for Quaternion<T> {
    fn mul_assign(&mut self, rhs: Quaternion<T>) {
        *self = *self * rhs;
    }
}

// Multiplication

impl<T: Scalar> Mul<T> for Quaternion<T> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Quaternion(self.0 * rhs)
    }
}

impl<T: Scalar> MulAssign<T> for Quaternion<T> {
    fn mul_assign(&mut self, rhs: T) {
        self.0 *= rhs;
    }
}

macro_rules! vector_pre_scale {
($($t:ty),+) => {
    $(impl Mul<Quaternion<$t>> for $t {
        type Output = Quaternion<$t>;

        fn mul(self, rhs: Quaternion<$t>) -> Quaternion<$t> {
            Quaternion(self * rhs.0)
        }
    })
    *
};
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64);

// Quaternion / Quaternion division

impl<T: FloatScalar> Div<Quaternion<T>> for Quaternion<T> {
    type Output = Quaternion<T>;

    fn div(self, rhs: Quaternion<T>) -> Self::Output {
        self * rhs.inverse()
    }
}

// Division

impl<T: Scalar> Div<T> for Quaternion<T> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Quaternion(self.0 / rhs)
    }
}

impl<T: Scalar> DivAssign<T> for Quaternion<T> {
    fn div_assign(&mut self, rhs: T) {
        self.0 /= rhs;
    }
}

macro_rules! quaternion_pre_division {
($($t:ty),+) => {
    $(impl Div<Quaternion<$t>> for $t {
        type Output = Quaternion<$t>;

        fn div(self, rhs: Quaternion<$t>) -> Quaternion<$t> {
            Quaternion::scalar(self) / rhs
        }
    })
    *
};
}

quaternion_pre_division!(f32, f64);

// Negation

impl<T: SignedScalar> Neg for Quaternion<T> {
    type Output = Quaternion<T>;

    fn neg(self) -> Self::Output {
        Quaternion(-self.0)
    }
}

// Alias

#[repr(C)]
pub struct QuaternionComponentAlias<T> {
    pub i: T,
    pub j: T,
    pub k: T,
    pub r: T,
    #[doc(hidden)]
    _private: [u8; 0],
}

impl<T> Deref for Quaternion<T> {
    type Target = QuaternionComponentAlias<T>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T> DerefMut for Quaternion<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

#[repr(C)]
pub struct QuaternionIJKVectorAlias<T: 'static> {
    pub ijk: Vector3<T>,
    #[doc(hidden)]
    _r: T,
    #[doc(hidden)]
    _private: [u8; 0],
}

impl<T: 'static> Deref for QuaternionComponentAlias<T> {
    type Target = QuaternionIJKVectorAlias<T>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: 'static> DerefMut for QuaternionComponentAlias<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

#[repr(C)]
pub struct QuaternionXYZVectorAlias<T: 'static> {
    pub xyz: Vector3<T>,
    pub w: T,
    #[doc(hidden)]
    _private: [u8; 0],
}

impl<T> Deref for QuaternionIJKVectorAlias<T> {
    type Target = QuaternionXYZVectorAlias<T>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T> DerefMut for QuaternionIJKVectorAlias<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T> Debug for Quaternion<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?} + i{:?} + j{:?} + k{:?}", self.r, self.i, self.j, self.k)
    }
}

impl<T> Display for Quaternion<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} + i{} + j{} + k{}", self.r, self.i, self.j, self.k)
    }
}