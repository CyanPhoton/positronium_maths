use std::fmt::{Debug, Display, Formatter};
use std::ops::{Deref, Div, Mul, Neg};

use crate::{Radians, FloatScalar, Scalar, SignedScalar, UnitVector3, Vector3};
use crate::quaternion::Quaternion;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

/// Safety: Is repr(c) and single field is NoUninit, as there are forbidden bit states, but never any uninit
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for UnitQuaternion<T> {}

/// Safety: Is repr(c) and single field is NoUninit, as there are forbidden bit states, but never any uninit
unsafe impl<T: bytemuck::NoUninit> bytemuck::NoUninit for PackedUnitQuaternion<T> {}

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct PackedUnitQuaternion<T: 'static> {
    vector: Vector3<T>,
}

impl<T: Scalar> PackedUnitQuaternion<T> {
    #[track_caller]
    pub fn vector(vector: Vector3<T>) -> PackedUnitQuaternion<T> where T: PartialOrd {
        assert!(vector.norm_squared() <= T::ONE, "The vector component of a unit quaternion must have a norm <= 1, for there to exist a real component that makes it overall unit");

        PackedUnitQuaternion {
            vector,
        }
    }

    pub fn calculate_scalar(self) -> T where T: FloatScalar {
        let scalar_squared: T = T::ONE - self.vector.norm_squared();
        // This can only return +ve values,
        // This field corresponds to cos(theta/2), so all values in [-pi, pi] range
        // will get mapped to [-pi/2, pi/2] for which cos is >= 0, thus only +ve values are needed
        scalar_squared.sqrt()
    }

    pub fn unpack(self) -> UnitQuaternion<T> where T: FloatScalar {
        UnitQuaternion(Quaternion::new(self.calculate_scalar(), self.vector))
    }
}

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct UnitQuaternion<T: 'static>(pub(crate) Quaternion<T>);

impl<T: Scalar> UnitQuaternion<T> {
    pub fn rotation<R>(angle: R, axis: UnitVector3<T>) -> UnitQuaternion<T> where R: Into<Radians<T>>, T: FloatScalar {
        UnitQuaternion(Quaternion::rotation(angle, axis))
    }

    pub const fn identity() -> UnitQuaternion<T> {
        UnitQuaternion(Quaternion::identity())
    }

    pub const fn one() -> UnitQuaternion<T> {
        UnitQuaternion(Quaternion::one())
    }

    pub fn i() -> UnitQuaternion<T> {
        UnitQuaternion(Quaternion::i())
    }

    pub fn j() -> UnitQuaternion<T> {
        UnitQuaternion(Quaternion::j())
    }

    pub fn k() -> UnitQuaternion<T> {
        UnitQuaternion(Quaternion::k())
    }

    pub fn pack(self) -> PackedUnitQuaternion<T> where T: SignedScalar + PartialOrd {
        PackedUnitQuaternion {
            vector: if self.r < T::ZERO { -self.ijk } else { self.ijk },
        }
    }

    pub fn inner_product(self, rhs: Self) -> T {
        self.0.inner_product(rhs.0)
    }

    pub fn conjugate(self) -> UnitQuaternion<T> where T: SignedScalar {
        UnitQuaternion(self.0.conjugate())
    }

    pub fn conjugate_assign(&mut self) where T: SignedScalar {
        self.0.conjugate_assign();
    }

    pub fn inverse(self) -> UnitQuaternion<T> where T: SignedScalar {
        self.conjugate()
    }

    pub fn inverse_assign(&mut self) where T: SignedScalar {
        self.conjugate_assign();
    }

    pub fn powf(self, t: T) -> UnitQuaternion<T> where T: FloatScalar {
        let Some(axis) = self.rotation_axis() else {
            return UnitQuaternion::one();
        };
        let rot = self.rotation_angle();

        UnitQuaternion::rotation(rot * t, axis)
    }

    /// Interpolates in the space of unit quaternions with r >= 0, may not be the shortest physical rotation
    pub fn interpolate(self, target: UnitQuaternion<T>, fraction: T) -> UnitQuaternion<T> where T: FloatScalar {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        (target * self.inverse()).powf(fraction) * self
    }

    /// Interpolate in the rotation space, will be the shortest physical rotation
    pub fn interpolate_rotation(self, target: UnitQuaternion<T>, fraction: T) -> UnitQuaternion<T> where T: FloatScalar {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    pub fn extrapolate(self, target: UnitQuaternion<T>, fraction: T) -> UnitQuaternion<T> where T: FloatScalar {
        (target * self.inverse()).powf(fraction) * self
    }

    pub fn extrapolate_rotation(self, target: UnitQuaternion<T>, fraction: T) -> UnitQuaternion<T> where T: FloatScalar {
        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict(self) -> UnitQuaternion<T> where T: SignedScalar + PartialOrd {
        if self.r < T::ZERO {
            -self
        } else {
            self
        }
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict_assign(&mut self) where T: SignedScalar + PartialOrd {
        *self = self.restrict();
    }

    pub fn into_quaternion(self) -> Quaternion<T> {
        self.0
    }

    /// Convert into the array [r, i, j, k]
    pub fn into_array(self) -> [T; 4] {
        self.0.into_array()
    }
}

impl<T: FloatScalar> From<UnitQuaternion<T>> for Quaternion<T> {
    fn from(q: UnitQuaternion<T>) -> Self {
        q.into_quaternion()
    }
}

// UnitQuaternion x UnitQuaternion multiplication
// This is guaranteed to return another unit quaternion since |p * q| = |p| * |q|

impl<T: FloatScalar> Mul<UnitQuaternion<T>> for UnitQuaternion<T> {
    type Output = UnitQuaternion<T>;

    fn mul(self, rhs: UnitQuaternion<T>) -> Self::Output {
        UnitQuaternion(self.0 * rhs.0)
    }
}

impl<T: FloatScalar> Mul<Quaternion<T>> for UnitQuaternion<T> {
    type Output = Quaternion<T>;

    fn mul(self, rhs: Quaternion<T>) -> Self::Output {
        self.0 * rhs
    }
}

impl<T: FloatScalar> Mul<UnitQuaternion<T>> for Quaternion<T> {
    type Output = Quaternion<T>;

    fn mul(self, rhs: UnitQuaternion<T>) -> Self::Output {
        self * rhs.0
    }
}

// UnitQuaternion / UnitQuaternion division
// This is guaranteed to return another unit quaternion since |p / q| = |p| / |q|

impl<T: FloatScalar> Div<UnitQuaternion<T>> for UnitQuaternion<T> {
    type Output = UnitQuaternion<T>;

    fn div(self, rhs: UnitQuaternion<T>) -> Self::Output {
        self * rhs.inverse()
    }
}

impl<T: FloatScalar> Div<Quaternion<T>> for UnitQuaternion<T> {
    type Output = Quaternion<T>;

    fn div(self, rhs: Quaternion<T>) -> Self::Output {
        self.0 * rhs.inverse()
    }
}

impl<T: FloatScalar> Div<UnitQuaternion<T>> for Quaternion<T> {
    type Output = Quaternion<T>;

    fn div(self, rhs: UnitQuaternion<T>) -> Self::Output {
        self * rhs.inverse().0
    }
}

// -UnitQuaternion
// This is guaranteed to return another unit quaternion since | -1 * p | = | -1 | * | p | = | p |

impl<T: SignedScalar> Neg for UnitQuaternion<T> {
    type Output = UnitQuaternion<T>;

    fn neg(self) -> Self::Output {
        UnitQuaternion(-self.0)
    }
}


impl<T: Scalar> Deref for UnitQuaternion<T> {
    type Target = Quaternion<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

// Alias

impl<T: FloatScalar> Debug for UnitQuaternion<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: FloatScalar> Display for UnitQuaternion<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

#[repr(C)]
pub struct PackedUnitQuaternionComponentAlias<T> {
    pub i: T,
    pub j: T,
    pub k: T,
    #[doc(hidden)]
    _private: [u8; 0],
}

impl<T> Deref for PackedUnitQuaternion<T> {
    type Target = PackedUnitQuaternionComponentAlias<T>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T> Debug for PackedUnitQuaternion<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "? + i{:?} + j{:?} + k{:?}", self.vector.i, self.vector.j, self.vector.k)
    }
}

impl<T: FloatScalar> Display for PackedUnitQuaternion<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} + i{} + j{} + k{}", self.calculate_scalar(), self.vector.i, self.vector.j, self.vector.k)
    }
}