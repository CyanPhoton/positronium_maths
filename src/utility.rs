pub enum ConstTest<const TRUTH: bool> {}
pub trait True {}
impl True for ConstTest<true> {}
pub trait False {}
impl False for ConstTest<false> {}

pub mod array {
    use std::mem::{ManuallyDrop, MaybeUninit};
    use crate::ZeroInit;

    pub trait ConstArray<T, const N: usize> {
        const ARRAY: [T; N];
    }

    pub struct IndexArray<const N: usize>;

    impl<const N: usize> ConstArray<usize, N> for IndexArray<N> {
        const ARRAY: [usize; N] = {
            let mut result = [0; N];
            {
                let mut i = 0;
                while i < N {
                    result[i] = i;
                    i += 1;
                }
            }
            result
        };
    }

    pub struct OffsetIndexArray<const O: usize, const N: usize>;

    impl<const O: usize, const N: usize> ConstArray<usize, N> for OffsetIndexArray<O, N> {
        const ARRAY: [usize; N] = {
            let mut result = [0; N];
            {
                let mut i = 0;
                while i < N {
                    result[i] = i + O;
                    i += 1;
                }
            }
            result
        };
    }

    pub struct OffsetCyclicIndexArray<const O: usize, const N: usize>;

    impl<const O: usize, const N: usize> ConstArray<usize, N> for OffsetCyclicIndexArray<O, N> {
        const ARRAY: [usize; N] = {
            let mut result = [0; N];
            {
                let mut i = 0;
                while i < N {
                    result[i] = (i + O) % N;
                    i += 1;
                }
            }
            result
        };
    }

    #[inline(always)]
    pub fn from_fn<T, const N: usize, F: FnMut(usize) -> T>(mut f: F) -> [T; N] {
        let mut result: [MaybeUninit<T>; N] = unsafe {
            std::mem::transmute_copy(&MaybeUninit::<[T; N]>::uninit())
        };

        {
            let mut i = 0;
            while i < N {
                result[i].write(f(i));
                i += 1;
            }
        }

        unsafe {
            std::mem::transmute_copy(&result)
        }
    }

    #[inline(always)]
    pub fn try_from_fn<T, E, const N: usize, F: FnMut(usize) -> Result<T, E>>(mut f: F) -> Result<[T; N], E> {
        let mut result: [MaybeUninit<T>; N] = unsafe {
            std::mem::transmute_copy(&MaybeUninit::<[T; N]>::uninit())
        };

        {
            let mut i = 0;
            while i < N {
                match f(i) {
                    Ok(v) => {
                        result[i].write(v);
                    }
                    Err(e) => {
                        if std::mem::needs_drop::<T>() {
                            // In the cases of T needing drop, we should iterate back through the array and drop things
                            while i > 0 {
                                i -= 1;
                                unsafe {
                                    std::ptr::drop_in_place(result[i].as_mut_ptr());
                                }
                            }
                        }
                        return Err(e);
                    }
                }
                i += 1;
            }
        }

        unsafe {
            Ok(std::mem::transmute_copy(&result))
        }
    }

    #[derive(Debug)]
    pub struct InsufficientElements;

    #[inline(always)]
    pub fn from_iter<T, const N: usize, I: IntoIterator<Item=T>>(iter: I) -> Result<[T; N], InsufficientElements> {
        let mut iter = iter.into_iter();
        try_from_fn(|_| iter.next().ok_or(InsufficientElements))
    }

    #[inline(always)]
    pub const fn join<T, const N: usize, const M: usize>(lhs: [T; N], rhs: [T; M]) -> [T; N + M] {
        #[repr(C)]
        struct Pair<V, const N: usize, const M: usize> {
            lhs: [V; N],
            rhs: [V; M],
        }

        #[repr(C)]
        union Union<V, const N: usize, const M: usize> where [(); N + M]: {
            pair: ManuallyDrop<Pair<V, N, M>>,
            join: ManuallyDrop<[V; N + M]>
        }

        let union = Union::<T, N, M> {
            pair: ManuallyDrop::new(Pair {
                lhs,
                rhs,
            })
        };

        ManuallyDrop::into_inner(unsafe { union.join })
    }

    #[inline(always)]
    pub fn split<T, const N: usize, const M: usize>(array: [T; N]) -> ([T; M], [T; N - M]) {
        #[repr(C)]
        struct Pair<V, const N: usize, const M: usize> where [(); N - M]: {
            lhs: [V; M],
            rhs: [V; N - M],
        }

        #[repr(C)]
        union Union<V, const N: usize, const M: usize> where [(); N - M]: {
            pair: ManuallyDrop<Pair<V, N, M>>,
            join: ManuallyDrop<[V; N]>
        }

        let union = Union::<T, N, M> {
            join: ManuallyDrop::new(array)
        };

        let Pair { lhs, rhs } = ManuallyDrop::into_inner(unsafe { union.pair });
        (lhs, rhs)
    }

    #[inline(always)]
    pub const fn shuffle<T: ZeroInit + Copy, const N: usize, const M: usize>(array: &[T; N], indices: &[usize; M]) -> [T; M] {
        let mut result = [T::ZERO; M];
        {
            let mut i = 0;
            while i < M {
                result[i] = array[indices[i]];
                i += 1;
            }
        }
        result
    }

    #[inline(always)]
    pub fn map<T, U, M: FnMut(T) -> U, const N: usize>(array: [T; N], mut map: M) -> [U; N] {
        let array: [MaybeUninit<T>; N] = unsafe {
            std::mem::transmute_copy(&ManuallyDrop::new(array))
        };

        let mut result: [MaybeUninit<U>; N] = unsafe {
            std::mem::transmute_copy(&MaybeUninit::<[U; N]>::uninit())
        };

        for i in 0..N {
            let v = unsafe { array[i].assume_init_read() };
            result[i].write(map(v));
        }

        unsafe {
            std::mem::transmute_copy(&result)
        }
    }

    #[inline(always)]
    pub fn map_zip<T, V, U, M: FnMut(T, V) -> U, const N: usize>(first_array: [T; N], second_array: [V; N], mut map: M) -> [U; N] {
        let first_array: [MaybeUninit<T>; N] = unsafe {
            std::mem::transmute_copy(&ManuallyDrop::new(first_array))
        };
        let second_array: [MaybeUninit<V>; N] = unsafe {
            std::mem::transmute_copy(&ManuallyDrop::new(second_array))
        };

        let mut result: [MaybeUninit<U>; N] = unsafe {
            std::mem::transmute_copy(&MaybeUninit::<[U; N]>::uninit())
        };

        for i in 0..N {
            let u = unsafe { first_array[i].assume_init_read() };
            let v = unsafe { second_array[i].assume_init_read() };
            result[i].write(map(u, v));
        }

        unsafe {
            std::mem::transmute_copy(&result)
        }
    }

    #[inline(always)]
    pub fn try_map<T, U, E, M: FnMut(T) -> Result<U, E>, const N: usize>(array: [T; N], mut map: M) -> Result<[U; N], E> {
        let mut array: [MaybeUninit<T>; N] = unsafe {
            std::mem::transmute_copy(&ManuallyDrop::new(array))
        };

        let mut result: [MaybeUninit<U>; N] = unsafe {
            std::mem::transmute_copy(&MaybeUninit::<[U; N]>::uninit())
        };

        let mut i = 0;
        while i < N {
            let v = unsafe { array[i].assume_init_read() };
            match map(v) {
                Ok(u) => result[i].write(u),
                Err(e) => {
                    if std::mem::needs_drop::<T>() {
                        let mut i = i + 1;
                        while i < N {
                            unsafe {
                                std::ptr::drop_in_place(array[i].as_mut_ptr());
                            }
                            i += 1;
                        }
                    }
                    if std::mem::needs_drop::<U>() {
                        while i > 0 {
                            i -= 1;
                            unsafe {
                                std::ptr::drop_in_place(result[i].as_mut_ptr());
                            }
                        }
                    }
                    return Err(e);
                }
            };

            i += 1;
        }

        unsafe {
            Ok(std::mem::transmute_copy(&result))
        }
    }
}