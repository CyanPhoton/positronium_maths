use crate::{Degrees, OrdScalar, Radians, FloatScalar, SignedScalar};

impl<T: SignedScalar> Radians<T> {
    pub fn abs(self) -> Self {
        self.map(|v| v.abs())
    }
}

impl<T: OrdScalar> Radians<T> {
    pub fn min(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.min(r))
    }

    pub fn max(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.max(r))
    }

    pub fn clamp(self, lower: Self, upper: Self) -> Self {
        Self::new(self.value().clamp(lower.value(), upper.value()))
    }
}

impl<T: SignedScalar> Degrees<T> {
    pub fn abs(self) -> Self {
        self.map(|v| v.abs())
    }
}

impl<T: OrdScalar> Degrees<T> {
    pub fn min(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.min(r))
    }

    pub fn max(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.max(r))
    }

    pub fn clamp(self, lower: Self, upper: Self) -> Self {
        Self::new(self.value().clamp(lower.value(), upper.value()))
    }
}

macro_rules! impl_trig {
    ($($t:ty),*) => {
        $(
            impl<T: FloatScalar> $t {
                pub fn sin(self) -> T {
                    Radians::<T>::from(self).value().sin()
                }

                pub fn cos(self) -> T {
                    Radians::<T>::from(self).value().cos()
                }

                pub fn tan(self) -> T {
                    Radians::<T>::from(self).value().tan()
                }

                pub fn sin_cos(self) -> (T, T) {
                    Radians::<T>::from(self).value().sin_cos()
                }

                pub fn asin(value: T) -> $t {
                    <$t>::from(Radians::<T>::new(value.asin()))
                }

                pub fn acos(value: T) -> $t {
                    <$t>::from(Radians::<T>::new(value.acos()))
                }

                pub fn atan(value: T) -> $t {
                    <$t>::from(Radians::<T>::new(value.atan()))
                }

                pub fn atan2(numerator: T, denominator: T) -> $t {
                    <$t>::from(Radians::<T>::new(numerator.atan2(denominator)))
                }
            }
        )*
    };
}

impl_trig!(Radians<T>, Degrees<T>);

#[cfg(test)]
mod tests {
    use crate::{Degrees, Radians};

    #[test]
    fn test() {
        dbg!(Degrees::new(30.0).sin());
        dbg!(Degrees::new(30.0).to_radians().sin());
        dbg!(Degrees::asin(0.5));
        dbg!(Radians::asin(0.5).to_degrees());
    }
}