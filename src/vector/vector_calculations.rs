use std::ops::{DivAssign};

use super::Vector;
use crate::{ConstTest, Determinant, FloatScalar, Scalar, SignedScalar, True, UnitVector, Vector2, Vector3, VectorSlice};
use crate::Matrix;

impl<T: Scalar, const N: usize> Vector<T, N> {
    pub fn inner_product(self, rhs: Self) -> T {
        let mut result = T::ZERO;
        for i in 0..N {
            result += self[i] * rhs[i];
        }
        result
    }

    pub fn norm_squared(self) -> T {
        self.inner_product(self)
    }
}

impl<T: FloatScalar, const N: usize> Vector<T, N> {
    pub fn norm(self) -> T {
        self.norm_squared().sqrt()
    }

    pub fn unit(self) -> UnitVector<T, N> {
        UnitVector(self / self.norm())
    }

    pub fn try_unit(self) -> Option<UnitVector<T, N>> {
        if self != Self::zero() {
            Some(self.unit())
        } else {
            None
        }
    }

    pub fn unit_or_zero(self) -> Self {
        if self == Self::zero() {
            self
        } else {
            self / self.norm()
        }
    }

    pub fn unit_assign(&mut self) {
        *self = *self / self.norm()
    }

    pub fn unit_assign_or_zero(&mut self) {
        if *self != Self::zero() {
            *self = *self / self.norm()
        }
    }

    pub fn unit_with_norm(self) -> (UnitVector<T, N>, T) {
        let norm = self.norm();
        (UnitVector(self / norm), norm)
    }

    pub fn try_unit_with_norm(self) -> Option<(UnitVector<T, N>, T)> {
        if self != Self::zero() {
            Some(self.unit_with_norm())
        } else {
            None
        }
    }

    pub fn unit_or_zero_with_norm(self) -> (Self, T) {
        if self == Self::zero() {
            (self, T::ZERO)
        } else {
            let norm = self.norm();
            (self / norm, norm)
        }
    }

    pub fn unit_assign_with_norm(&mut self) -> T {
        let norm = self.norm();
        *self = *self / norm;
        norm
    }

    pub fn unit_assign_or_zero_with_norm(&mut self) -> T {
        if *self != Self::zero() {
            self.unit_assign_with_norm()
        } else {
            T::ZERO
        }
    }
}

impl<T: Scalar, const N: usize> VectorSlice<T, N> {
    #[track_caller]
    pub fn inner_product<const M: usize>(&self, rhs: &VectorSlice<T, M>) -> T {
        assert_eq!(self.size(), rhs.size(), "Inner product requires both vector slices have same size.");

        let mut result = T::ZERO;
        for i in 0..self.size() {
            result = result + (self[i] * rhs[i]);
        }
        result
    }

    pub fn norm_squared(&self) -> T {
        self.inner_product(self)
    }
}

impl<T: FloatScalar, const N: usize> VectorSlice<T, N>  {
    pub fn norm(&self) -> T {
        self.norm_squared().sqrt()
    }

    pub fn unit_assign(&mut self)  {
        self.div_assign(self.norm())
    }
}

impl<T: SignedScalar, const N: usize> Vector<T, N> where ConstTest<{ N > 0 }>: True, Matrix<T, { N - 1 }, { N - 1 }>: Determinant<T> {
    pub fn cross_product(vectors: [Self; N - 1]) -> Self {
        let m = Matrix::column_vectors(vectors);

        // The + size comes from the right edge of the matrix having the checkerboard pattern being alternated if matrix size is even or odd
        Vector::from_fn(|i| (if (i + N) % 2 == 0 { -T::ONE } else { T::ONE }) * m.cut_row(i).determinant())
    }
}

impl<T: SignedScalar> Vector3<T> {
    #[inline(always)]
    pub fn cross(self, rhs: Vector3<T>) -> Vector3<T> {
        Vector3::array([
            self.y * rhs.z - self.z * rhs.y,
            self.z * rhs.x - self.x * rhs.z,
            self.x * rhs.y - self.y * rhs.x
        ])
    }
}

impl<T: Scalar> Vector2<T> {
    pub fn cross_in_z(self, rhs: Vector2<T>) -> T {
        self.x * rhs.y - self.y * rhs.x
    }
}