use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::{Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, Deref, DerefMut, Div, DivAssign, Index, IndexMut, Mul, MulAssign, Neg, Sub, SubAssign};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};
use paste::paste;

use crate::{bounds::RangeBounds, CastAs, NotNan, Scalar, SignedScalar, utility::array, VectorSlice, ZeroInit};

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct Vector<T: 'static, const N: usize> {
    pub values: [T; N],
}

impl<T: Default, const N: usize> Default for Vector<T, N> {
    fn default() -> Self {
        Vector::from_fn(|_| T::default())
    }
}

/// Safety: Is repr(c) and single field is Zeroable
unsafe impl<T: bytemuck::Zeroable, const N: usize> bytemuck::Zeroable for Vector<T, N> {}

/// Safety: Is repr(c) and single field is Pod
unsafe impl<T: bytemuck::Pod, const N: usize> bytemuck::Pod for Vector<T, N> {}

/// Safety: Is repr(c) and single field is NoUninit
unsafe impl<T: bytemuck::NoUninit, const N: usize> bytemuck::NoUninit for Vector<NotNan<T>, N> {}

impl<T: ZeroInit + Copy, const N: usize> ZeroInit for Vector<T, N> {
    const ZERO: Self = Vector::splat(T::ZERO);
}

impl<T, const N: usize> Vector<T, N> {
    pub const fn zero() -> Self where T: ZeroInit + Copy {
        Self::ZERO
    }

    pub const fn splat(value: T) -> Self where T: Copy {
        Vector::array([value; N])
    }

    pub const fn array(values: [T; N]) -> Self {
        Vector { values }
    }

    pub fn slice(values: &[T]) -> Self where T: Copy {
        Vector::from_fn(|i| values[i])
    }

    #[track_caller]
    pub const fn unit_n(n: usize) -> Self where T: Scalar {
        assert!(n < N, "Invalid index specified");

        let mut v = Self::ZERO;
        v.values[n] = T::ONE;
        v
    }

    pub fn from_fn<F>(f: F) -> Self where F: FnMut(usize) -> T {
        Vector::array(array::from_fn(f))
    }

    pub fn try_from_fn<F, E>(f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<T, E> {
        Ok(Vector::array(array::try_from_fn(f)?))
    }

    pub fn chain<const M: usize>(self, other: Vector<T, M>) -> Vector<T, { N + M }> {
        Vector::array(array::join(self.values, other.values))
    }

    pub fn push(self, value: T) -> Vector<T, { N + 1 }> {
        Vector::array(array::join(self.values, [value]))
    }

    pub fn split<const M: usize>(self) -> (Vector<T, M>, Vector<T, { N - M }>) where [(); N - M]: {
        let (lhs, rhs) = array::split::<T, N, M>(self.values);
        (Vector::array(lhs), Vector::array(rhs))
    }

    pub fn pop(self) -> (Vector<T, { N - 1 }>, T) where [(); N - (N - 1)]: {
        let (lhs, rhs) = self.split::<{ N - 1 }>();
        let rhs = unsafe { rhs.into_iter().next().unwrap_unchecked() }; // (N - (N - 1)) == 1, so always safe
        (lhs, rhs)
    }

    #[must_use]
    pub fn swap(mut self, a: usize, b: usize) -> Self {
        self.values.swap(a, b);
        self
    }

    pub fn swap_assign(&mut self, a: usize, b: usize) {
        self.values.swap(a, b);
    }

    /// Can also use & operator
    pub fn comp_mul(self, rhs: Self) -> Self where T: Scalar {
        self & rhs
    }

    /// Can also use | operator
    pub fn comp_div(self, rhs: Self) -> Self where T: Scalar {
        self | rhs
    }

    pub fn map<V, F: FnMut(T) -> V>(self, mut map: F) -> Vector<V, N> {
        let mut iter = self.into_iter();
        Vector::from_fn(|_| map(unsafe { iter.next().unwrap_unchecked() })) // N = N so always enough elements
    }

    pub fn try_map<V, E, F: FnMut(T) -> Result<V, E>>(self, mut map: F) -> Result<Vector<V, N>, E> {
        let mut iter = self.into_iter();
        Vector::try_from_fn(|_| map(unsafe { iter.next().unwrap_unchecked() })) // N = N so always enough elements
    }

    pub fn zip_map<U, V, F: FnMut(T, V) -> U>(self, other: Vector<V, N>, mut map: F) -> Vector<U, N> {
        let mut self_iter = self.into_iter();
        let mut other_iter = other.into_iter();
        Vector::from_fn(|_| map(
            unsafe { self_iter.next().unwrap_unchecked() }, // N = N so always enough elements
            unsafe { other_iter.next().unwrap_unchecked() }, // N = N so always enough elements
        ))
    }

    pub fn cast_as<V>(self) -> Vector<V, N> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }

    pub fn comp_into<V>(self) -> Vector<V, N> where T: Into<V> {
        self.map(|v| v.into())
    }

    pub fn comp_try_into<V, E>(self) -> Result<Vector<V, N>, E> where T: TryInto<V, Error=E> {
        self.try_map(|v| v.try_into())
    }

    pub const fn as_slice(&self) -> &[T] {
        &self.values
    }

    pub fn as_slice_mut(&mut self) -> &mut [T] {
        &mut self.values
    }

    pub const fn as_array(&self) -> &[T; N] {
        &self.values
    }

    pub fn as_array_mut(&mut self) -> &mut [T; N] {
        &mut self.values
    }

    pub fn into_array(self) -> [T; N] {
        self.values
    }

    pub fn into_iter(self) -> impl Iterator<Item=T> {
        self.values.into_iter()
    }

    pub fn iter(&self) -> impl Iterator<Item=&T> {
        self.values.iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item=&mut T> {
        self.values.iter_mut()
    }
}

impl<T, const N: usize> From<[T; N]> for Vector<T, N> {
    fn from(values: [T; N]) -> Self {
        Vector::array(values)
    }
}

impl<T, const N: usize> From<Vector<T, N>> for [T; N] {
    fn from(values: Vector<T, N>) -> Self {
        values.into_array()
    }
}

macro_rules! from_tuple {
    ( $n0:literal, $t0:ident, $($n:literal, $t:ident, )* ) => {
        paste! {
            #[allow(non_snake_case)]
            impl<T> From<( $t0, $($t,)* )> for Vector<$t0, $n0> {
                fn from(v: ( $t0, $($t,)* )) -> Self {
                    let ( [< $t0 $n0 >], $( [< $t $n >]),* ) = v;
                    Vector::array([ [< $t0 $n0 >] $(, [< $t $n >])* ])
                }
            }

            #[allow(non_snake_case)]
            impl<T> From<Vector<$t0, $n0>> for ( $t0, $($t,)* ) {
                fn from(v: Vector<$t0, $n0>) -> Self {
                    // (  v[0], $( v[$n0 - $n]),*  )
                    let [ [< $t0 $n0 >] $(, [< $t $n >])* ] = v.into_array();
                    ( [< $t0 $n0 >], $( [< $t $n >]),* )
                }
            }

            #[allow(non_snake_case)]
            impl<T> Vector<$t0, $n0> {
                pub fn tuple(v: ( $t0, $($t,)* )) -> Self {
                    v.into()
                }
            }
        }

        from_tuple!( $( $n, $t, )* );
    };
    () => {}
}

from_tuple!(16, T, 15, T, 14, T, 13, T, 12, T, 11, T, 10, T, 9, T, 8, T, 7, T, 6, T, 5, T, 4, T, 3, T, 2, T, 1, T, );

impl<T, const N: usize> IntoIterator for Vector<T, N> {
    type Item = T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.into_iter()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a Vector<T, N> {
    type Item = &'a T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a mut Vector<T, N> {
    type Item = &'a mut T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<T, const N: usize> FromIterator<T> for Vector<T, N> {
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        Vector::array(array::from_iter(iter).unwrap())
    }
}

// Cross product

impl<T: SignedScalar> BitXor for Vector<T, 3> {
    type Output = Self;

    fn bitxor(self, rhs: Self) -> Self {
        self.cross(rhs)
    }
}

// Addition

impl<T: Scalar, const N: usize> Add for Vector<T, N> {
    type Output = Self;

    fn add(self, rhs: Self) -> Self {
        Vector::from_fn(|i| self[i] + rhs[i])
    }
}

impl<T: Scalar, const N: usize> AddAssign for Vector<T, N> {
    fn add_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self[i] += rhs[i];
        }
    }
}

// Subtraction

impl<T: Scalar, const N: usize> Sub for Vector<T, N> {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self {
        Vector::from_fn(|i| self[i] - rhs[i])
    }
}

impl<T: Scalar, const N: usize> SubAssign for Vector<T, N> {
    fn sub_assign(&mut self, rhs: Self) {
        for i in 0..N {
            self[i] -= rhs[i];
        }
    }
}

// Inner product

impl<T: Scalar, const N: usize> Mul for Vector<T, N> {
    type Output = T;

    fn mul(self, rhs: Self) -> T {
        self.inner_product(rhs)
    }
}

// Multiplication

impl<T: Scalar, const N: usize> Mul<T> for Vector<T, N> {
    type Output = Self;

    fn mul(self, rhs: T) -> Self {
        Vector::from_fn(|i| self[i] * rhs)
    }
}

impl<T: Scalar, const N: usize> MulAssign<T> for Vector<T, N> {
    fn mul_assign(&mut self, rhs: T) {
        for i in 0..N {
            self[i] *= rhs;
        }
    }
}

macro_rules! vector_pre_scale {
    ($($t:ty),+) => {
        $(impl<const N: usize> Mul<Vector<$t, N>> for $t {
            type Output = Vector<$t, N>;

            fn mul(self, rhs: Vector<$t, N>) -> Vector<$t, N> {
                Vector::from_fn(|i| self * rhs[i])
            }
        })
        *
    };
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64, NotNan<f32>, NotNan<f64>);

impl<T: Scalar, const N: usize> BitAnd<Vector<T, N>> for Vector<T, N> {
    type Output = Vector<T, N>;

    fn bitand(self, rhs: Vector<T, N>) -> Self::Output {
        self.zip_map(rhs, |l, r| l * r)
    }
}

impl<T: Scalar, const N: usize> BitAndAssign<Vector<T, N>> for Vector<T, N> {
    fn bitand_assign(&mut self, rhs: Vector<T, N>) {
        for i in 0..N {
            self[i] *= rhs[i];
        }
    }
}

// Division

impl<T: Scalar, const N: usize> Div<T> for Vector<T, N> {
    type Output = Self;

    fn div(self, rhs: T) -> Self {
        Vector::from_fn(|i| self[i] / rhs)
    }
}

impl<T: Scalar, const N: usize> DivAssign<T> for Vector<T, N> {
    fn div_assign(&mut self, rhs: T) {
        for i in 0..N {
            self[i] /= rhs;
        }
    }
}

impl<T: Scalar, const N: usize> BitOr<Vector<T, N>> for Vector<T, N> {
    type Output = Vector<T, N>;

    fn bitor(self, rhs: Vector<T, N>) -> Self::Output {
        self.zip_map(rhs, |l, r| l / r)
    }
}


impl<T: Scalar, const N: usize> BitOrAssign<Vector<T, N>> for Vector<T, N> {
    fn bitor_assign(&mut self, rhs: Vector<T, N>) {
        for i in 0..N {
            self[i] /= rhs[i];
        }
    }
}

macro_rules! vector_pre_recip {
    ($($t:ty),+) => {
        $(impl<const N: usize> BitOr<Vector<$t, N>> for $t {
            type Output = Vector<$t, N>;

            fn bitor(self, rhs: Vector<$t, N>) -> Vector<$t, N> {
                Vector::splat(self) | rhs
            }
        })
        *
    };
}

vector_pre_recip!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64, NotNan<f32>, NotNan<f64>);

// Negation

impl<T: Scalar, const N: usize> Neg for Vector<T, N> where T: SignedScalar {
    type Output = Vector<T, N>;

    fn neg(self) -> Self::Output {
        Vector::from_fn(|i| -self[i])
    }
}

// Indexing

impl<T, const N: usize> Index<usize> for Vector<T, N> {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        &self.values[index]
    }
}

impl<T, const N: usize> IndexMut<usize> for Vector<T, N> {
    fn index_mut(&mut self, index: usize) -> &mut T {
        &mut self.values[index]
    }
}

// Slicing

impl<T, I, const N: usize> Index<I> for Vector<T, N> where I: RangeBounds<usize> {
    type Output = VectorSlice<T, N>;

    #[track_caller]
    fn index(&self, index: I) -> &VectorSlice<T, N> {
        let start = index.start();
        let end = index.end(N);
        // let len = end - start;
        //
        // assert!(end <= N, "Index out of bounds");

        unsafe { core::mem::transmute(&self.values[start..end]) }
    }
}

impl<T, I, const N: usize> IndexMut<I> for Vector<T, N> where I: RangeBounds<usize> {
    #[track_caller]
    fn index_mut(&mut self, index: I) -> &mut VectorSlice<T, N> {
        let start = index.start();
        let end = index.end(N);
        // let len = end - start;
        //
        // assert!(end <= N, "Index out of bounds");

        unsafe { core::mem::transmute(&mut self.values[start..end]) }
    }
}

// Swizzling
impl<T, const N: usize> Vector<T, N> {
    pub fn swizzle<const M: usize>(self, indices: [usize; M]) -> Vector<T, M> where T: Copy {
        self >> indices
    }
}

impl<T, const N: usize, const M: usize> std::ops::Shr<[usize; M]> for Vector<T, N> where T: Copy {
    type Output = Vector<T, M>;

    fn shr(self, rhs: [usize; M]) -> Vector<T, M> {
        Vector::from_fn(|i| self[rhs[i]])
    }
}

macro_rules! vector_specialise {
    ($nick_base:ident, $size:literal, $( ($coord:ident, $coord_index:literal, $colour:ident, $uv:ident, $comp:ident)),+ ) => {
        paste! {
            pub type [<$nick_base $size>]<T> = Vector<T, $size>;

            pub mod [<vec $size>] {
                use super::*;

                impl<T> Vector<T, $size> {
                    pub const fn new($($coord: T),+) -> Self {
                        Vector::array([$($coord),+])
                    }
                }

                impl<T: Scalar> Vector<T, $size> {
                    $(
                    pub const fn [< unit_ $coord >]() -> Self {
                        Self::unit_n($coord_index)
                    }
                    )
                    +
                }

                #[repr(C)]
                #[doc(hidden)]
                pub struct [<VectorCoordAlias $size>]<T> {
                    $(pub $coord: T,
                    )+
                    #[doc(hidden)]
                    _private: [u8; 0],
                }

                impl<T> Deref for Vector<T, $size> {
                    type Target = [<VectorCoordAlias $size>]<T>;

                    fn deref(&self) -> &Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                impl<T> DerefMut for Vector<T, $size> {
                    fn deref_mut(&mut self) -> &mut Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                #[repr(C)]
                #[doc(hidden)]
                pub struct [<VectorColourAlias $size>]<T> {
                    $(
                    #[doc=concat!("Alias of .", stringify!($coord))]
                    pub $colour: T,
                    )+
                    #[doc(hidden)]
                    _private: [u8; 0],
                }

                impl<T> Deref for [<VectorCoordAlias $size>]<T> {
                    type Target = [<VectorColourAlias $size>]<T>;

                    fn deref(&self) -> &Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                impl<T> DerefMut for [<VectorCoordAlias $size>]<T> {
                    fn deref_mut(&mut self) -> &mut Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                #[repr(C)]
                #[doc(hidden)]
                pub struct [<VectorTextureAlias $size>]<T> {
                    $(
                    #[doc=concat!("Alias of .", stringify!($coord))]
                    pub $uv: T,
                    )+
                    #[doc(hidden)]
                    _private: [u8; 0],
                }

                impl<T> Deref for [<VectorColourAlias $size>]<T> {
                    type Target = [<VectorTextureAlias $size>]<T>;

                    fn deref(&self) -> &Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                impl<T> DerefMut for [<VectorColourAlias $size>]<T> {
                    fn deref_mut(&mut self) -> &mut Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                #[repr(C)]
                #[doc(hidden)]
                pub struct [<VectorCompAlias $size>]<T> {
                    $(
                    #[doc=concat!("Alias of .", stringify!($coord))]
                    pub $comp: T,
                    )+
                    #[doc(hidden)]
                    _private: [u8; 0],
                }

                impl<T> Deref for [<VectorTextureAlias $size>]<T> {
                    type Target = [<VectorCompAlias $size>]<T>;

                    fn deref(&self) -> &Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }

                impl<T> DerefMut for [<VectorTextureAlias $size>]<T> {
                    fn deref_mut(&mut self) -> &mut Self::Target {
                        unsafe { core::mem::transmute(self) }
                    }
                }
            }
        }
    };
}

vector_specialise!(Vector, 1, (x, 0, r, s, i));
vector_specialise!(Vector, 2, (x, 0, r, s, i), (y, 1, g, t, k));
vector_specialise!(Vector, 3, (x, 0, r, s, i), (y, 1, g, t, j), (z, 2, b, u, k));
vector_specialise!(Vector, 4, (x, 0, r, s, i), (y, 1, g, t, j), (z, 2, b, u, k), (w, 3, a, v, l));


impl<T> Vector1<T> {
    pub fn with_y(self, y: T) -> Vector2<T> {
        let [x] = self.values;
        Vector2::new(x, y)
    }
}

impl<T> Vector2<T> {
    pub fn with_z(self, z: T) -> Vector3<T> {
        let [x, y] = self.values;
        Vector3::new(x, y, z)
    }
}

impl<T> Vector3<T> {
    pub fn xy(self) -> Vector2<T> {
        let [x, y, _] = self.values;

        Vector {
            values: [x, y],
        }
    }

    #[cfg(feature = "simd")]
    pub fn xy_simd(self) ->  crate::simd::SimdVector<T, 2> where T: crate::SimdScalar {
        let [x, y, _] = self.values;

        crate::simd::SimdVector::<T, 2>::new(x, y)
    }

    pub fn with_w(self, w: T) -> Vector4<T> {
        let [x, y, z] = self.values;

        Vector {
            values: [x, y, z, w],
        }
    }

    #[cfg(feature = "simd")]
    pub const fn with_w_simd(self, w: T) -> crate::simd::SimdVector<T, 4> where T: crate::SimdScalar {
        let [x, y, z] = self.values;
        crate::simd::SimdVector::<T, 4>::new(x, y, z, w)
    }
}

impl<T> Vector4<T> {
    pub fn xy(self) -> Vector2<T> {
        let [x, y, _, _] = self.values;

        Vector {
            values: [x, y],
        }
    }

    #[cfg(feature = "simd")]
    pub fn xy_simd(self) ->  crate::simd::SimdVector<T, 2> where T: crate::SimdScalar {
        let [x, y, _, _] = self.values;

        crate::simd::SimdVector::<T, 2>::new(x, y)
    }

    pub fn xyz(self) -> Vector3<T> {
        let [x, y, z, _] = self.values;

        Vector {
            values: [x, y, z],
        }
    }
}

macro_rules! vector_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

vector_nicks!(Vector, 1, f, f32, d, f64, F, NotNan<f32>, D, NotNan<f64>, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(Vector, 2, f, f32, d, f64, F, NotNan<f32>, D, NotNan<f64>, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(Vector, 3, f, f32, d, f64, F, NotNan<f32>, D, NotNan<f64>, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(Vector, 4, f, f32, d, f64, F, NotNan<f32>, D, NotNan<f64>, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);

impl<T, const N: usize> Debug for Vector<T, N> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<")?;
        for (i, v) in self.iter().enumerate() {
            if i != 0 {
                write!(f, ", {:?}", v)?;
            } else {
                write!(f, "{:?}", v)?;
            }
        }
        write!(f, ">")
    }
}

impl<T, const N: usize> Display for Vector<T, N> where T: Display {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<")?;
        for (i, v) in self.iter().enumerate() {
            if i != 0 {
                write!(f, ", {}", v)?;
            } else {
                write!(f, "{}", v)?;
            }
        }
        write!(f, ">")
    }
}