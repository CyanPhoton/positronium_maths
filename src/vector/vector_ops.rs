use crate::{OrdScalar, Scalar, SignedScalar, Vector};

impl<T: Scalar, const N: usize> Vector<T, N> {
    pub fn comp_abs(self) -> Self where T: SignedScalar {
        self.map(|v| v.abs())
    }

    pub fn comp_sum(&self) -> T {
        let mut sum = T::ZERO;
        for i in 0..N {
            sum += self[i];
        }
        sum
    }

    pub fn all<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.values.iter().all(f)
    }

    pub fn zip_all<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Vector<V, N>, mut f: F) -> bool {
        self.values.iter().zip(other).all(|(l, r)| f(l, r))
    }

    pub fn any<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.values.iter().any(f)
    }

    pub fn zip_any<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Vector<V, N>, mut f: F) -> bool {
        self.values.iter().zip(other).any(|(l, r)| f(l, r))
    }
}

impl<T: OrdScalar, const N: usize> Vector<T, N> {
    pub fn comp_min(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.min(r))
    }

    pub fn comp_max(self, rhs: Self) -> Self {
        self.zip_map(rhs, |l, r| l.max(r))
    }

    pub fn comp_clamp(self, lower: Self, upper: Self) -> Self {
        Self::from_fn(|i| {
            self[i].clamp(lower[i], upper[i])
        })
    }

    pub fn comp_gt(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l > r)
    }

    pub fn comp_ge(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l >= r)
    }

    pub fn comp_lt(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l < r)
    }

    pub fn comp_le(&self, other: &Self) -> bool {
        self.zip_all(other, |l, r| l <= r)
    }
}