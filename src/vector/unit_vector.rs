use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::{Add, BitXor, Deref, Div, Index, Mul, Neg, Sub};

use paste::paste;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

use crate::{bounds::RangeBounds, Scalar, SignedScalar, Vector, VectorSlice, ZeroInit};

/// Safety: Is repr(c) and single field is NoUninit, as there are forbidden bit states, but never any uninit
unsafe impl<T: bytemuck::NoUninit, const N: usize> bytemuck::NoUninit for UnitVector<T, N> {}

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct UnitVector<T: 'static, const N: usize>(pub(crate) Vector<T, N>);

impl<T: Scalar, const N: usize> UnitVector<T, N> {
    #[track_caller]
    pub fn unit_n(n: usize) -> Self {
        assert!(n < N, "Invalid index specified");

        let mut v = Vector::<T, N>::ZERO;
        v[n] = T::ONE;
        UnitVector(v)
    }
}

impl<T, const N: usize> UnitVector<T, N> {
    pub fn into_vector(self) -> Vector<T, N> {
        self.0
    }

    pub fn into_array(self) -> [T; N] {
        self.0.values
    }

    pub fn into_iter(self) -> std::array::IntoIter<T, N> {
        self.0.values.into_iter()
    }
}

impl<T, const N: usize> From<UnitVector<T, N>> for Vector<T, N> {
    fn from(vector: UnitVector<T, N>) -> Self {
        vector.0
    }
}

impl<T, const N: usize> IntoIterator for UnitVector<T, N> {
    type Item = T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a UnitVector<T, N> {
    type Item = &'a T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a mut UnitVector<T, N> {
    type Item = &'a mut T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}

// Cross product

impl<T: SignedScalar> BitXor<UnitVector<T, 3>> for UnitVector<T, 3> {
    type Output = Vector<T, 3>;

    fn bitxor(self, rhs: UnitVector<T, 3>) -> Vector<T, 3> {
        self.0.cross(rhs.0)
    }
}

impl<T: SignedScalar> BitXor<Vector<T, 3>> for UnitVector<T, 3> {
    type Output = Vector<T, 3>;

    fn bitxor(self, rhs: Vector<T, 3>) -> Vector<T, 3> {
        self.0.cross(rhs)
    }
}

// Addition

impl<T: Scalar, const N: usize> Add<UnitVector<T, N>> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn add(self, rhs: UnitVector<T, N>) -> Vector<T, N> {
        self.0 + rhs.0
    }
}

impl<T: Scalar, const N: usize> Add<Vector<T, N>> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn add(self, rhs: Vector<T, N>) -> Vector<T, N> {
        self.0 + rhs
    }
}

// Subtraction

impl<T: Scalar, const N: usize> Sub<UnitVector<T, N>> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn sub(self, rhs: UnitVector<T, N>) -> Vector<T, N> {
        self.0 - rhs.0
    }
}

impl<T: Scalar, const N: usize> Sub<Vector<T, N>> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn sub(self, rhs: Vector<T, N>) -> Vector<T, N> {
        self.0 - rhs
    }
}

// Inner product

impl<T: Scalar, const N: usize> Mul<UnitVector<T, N>> for UnitVector<T, N> {
    type Output = T;

    fn mul(self, rhs: UnitVector<T, N>) -> T {
        self.0 * rhs.0
    }
}

impl<T: Scalar, const N: usize> Mul<Vector<T, N>> for UnitVector<T, N> {
    type Output = T;

    fn mul(self, rhs: Vector<T, N>) -> T {
        self.0 * rhs
    }
}

impl<T: Scalar, const N: usize> Mul<UnitVector<T, N>> for Vector<T, N> {
    type Output = T;

    fn mul(self, rhs: UnitVector<T, N>) -> T {
        self * rhs.0
    }
}

// Multiplication

impl<T: Scalar, const N: usize> Mul<T> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn mul(self, rhs: T) -> Vector<T, N> {
        self.0 * rhs
    }
}

macro_rules! vector_pre_scale {
    ($($t:ty),+) => {
        $(impl<const N: usize> Mul<UnitVector<$t, N>> for $t {
            type Output = Vector<$t, N>;

            fn mul(self, rhs: UnitVector<$t, N>) -> Vector<$t, N> {
                self * rhs.0
            }
        })
        *
    };
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, i128, u128, f32, f64);

// Division

impl<T: Scalar, const N: usize> Div<T> for UnitVector<T, N> {
    type Output = Vector<T, N>;

    fn div(self, rhs: T) -> Vector<T, N> {
        self.0 / rhs
    }
}

// Negation

impl<T: SignedScalar, const N: usize> Neg for UnitVector<T, N> {
    type Output = UnitVector<T, N>;

    fn neg(self) -> Self::Output {
        Self(-self.0)
    }
}

// Indexing

impl<T: Scalar, const N: usize> Index<usize> for UnitVector<T, N> {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        &self.0[index]
    }
}

// Slicing

impl<T: Scalar, I, const N: usize> Index<I> for UnitVector<T, N> where I: RangeBounds<usize> {
    type Output = VectorSlice<T, N>;

    #[track_caller]
    fn index(&self, index: I) -> &VectorSlice<T, N> {
        // let start = index.start();
        // let end = index.end(N);
        // let len = end - start;
        //
        // assert!(end <= N, "Index out of bounds");

        self.0.index(index)
    }
}

impl<T: Scalar, const N: usize> Deref for UnitVector<T, N> {
    type Target = Vector<T, N>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

macro_rules! vector_specialise {
    ($nick_base:ident, $size_ident:literal, $size:literal, $( ($coord:ident, $colour:ident, $uv:ident, $comp:ident)),+ ) => {
        paste! {
            pub type [<$nick_base $size_ident>]<T> = UnitVector<T, $size>;

            pub mod [<unit_vec $size_ident>] {
                use super::*;

                impl<T: Scalar> UnitVector<T, $size> {
                    $(
                    pub fn [< unit_ $coord >]() -> Self {
                        let mut v = Vector::<T, $size>::ZERO;
                        v.$coord = T::ONE;
                        UnitVector(v)
                    }
                    )
                    +
                }
            }
        }
    };
}

vector_specialise!(UnitVector, 1, 1usize, (x, r, s, i));
vector_specialise!(UnitVector, 2, 2usize, (x, r, s, i), (y, g, t, k));
vector_specialise!(UnitVector, 3, 3usize, (x, r, s, i), (y, g, t, j), (z, b, u, k));
vector_specialise!(UnitVector, 4, 4usize, (x, r, s, i), (y, g, t, j), (z, b, u, k), (w, a, v, l));

macro_rules! vector_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

vector_nicks!(UnitVector, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(UnitVector, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(UnitVector, 3, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(UnitVector, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);

impl<T, const N: usize> Debug for UnitVector<T, N> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<")?;
        for (i, v) in self.0.values.iter().enumerate() {
            if i != 0 {
                write!(f, ", {:?}", v)?;
            } else {
                write!(f, "{:?}", v)?;
            }
        }
        write!(f, ">")
    }
}

impl<T, const N: usize> Display for UnitVector<T, N> where T: Display {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<")?;
        for (i, v) in self.0.values.iter().enumerate() {
            if i != 0 {
                write!(f, ", {}", v)?;
            } else {
                write!(f, "{}", v)?;
            }
        }
        write!(f, ">")
    }
}