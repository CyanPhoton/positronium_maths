use std::fmt::{Debug, Display};
use std::ops::{AddAssign, DivAssign, Index, IndexMut, MulAssign, SubAssign};

use crate::bounds::RangeBounds;
use crate::Scalar;

use super::vector::Vector;

pub struct VectorSlice<T, const N: usize> {
    pub(super) values: [T],
}

impl<T, const N: usize> VectorSlice<T, N> {
    pub const fn size(&self) -> usize {
        self.values.len()
    }

    pub fn as_slice(&self) -> &[T] {
        &self.values[..]
    }

    pub fn as_slice_mut(&mut self) -> &mut [T] {
        &mut self.values[..]
    }

    pub fn set<const M: usize>(&mut self, value: Vector<T, M>) where T: Copy {
        self.as_slice_mut().copy_from_slice(value.as_slice());
    }

    pub fn set_slice<const M: usize>(&mut self, slice: &VectorSlice<T, M>) where T: Copy {
        self.as_slice_mut().copy_from_slice(slice.as_slice());
    }

    pub fn set_with<F, const M: usize>(&mut self, f: F) where F: FnOnce(&Self) -> Vector<T, M>, T: Copy + 'static {
        self.set(f(self))
    }

    pub fn vector<const M: usize>(&self) -> Vector<T, M> where T: Copy {
        self.into()
    }

    pub fn iter(&self) -> impl Iterator<Item=&T> {
        self.as_slice().iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item=&mut T> {
        self.as_slice_mut().iter_mut()
    }
}

impl<T: Copy, const N: usize, const M: usize> From<&VectorSlice<T, M>> for Vector<T, N> {
    #[track_caller]
    fn from(slice: &VectorSlice<T, M>) -> Self {
        assert_eq!(slice.size(), N, "The size of the source and destination of the conversion do not match, source: {} != dest: {}", slice.size(), N);

        slice.values.iter().copied().collect()
    }
}

// Iterators

impl<'a, T, const N: usize> IntoIterator for &'a VectorSlice<T, N> {
    type Item = &'a T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T, const N: usize> IntoIterator for &'a mut VectorSlice<T, N> {
    type Item = &'a mut T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

// Add

impl<T: Scalar, const N: usize, const M: usize> AddAssign<&VectorSlice<T, M>> for VectorSlice<T, N> {
    #[track_caller]
    fn add_assign(&mut self, rhs: &VectorSlice<T, M>) {
        assert_eq!(rhs.size(), self.size(), "Can not add vectors of different sizes");

        for i in 0..self.size() {
            self[i] += rhs[i];
        }
    }
}

impl<T: Scalar, const N: usize, const M: usize> AddAssign<Vector<T, M>> for VectorSlice<T, N> {
    fn add_assign(&mut self, rhs: Vector<T, M>) {
        self.add_assign(&rhs[..])
    }
}

// Subtract

impl<T: Scalar, const N: usize, const M: usize> SubAssign<&VectorSlice<T, M>> for VectorSlice<T, N> {
    #[track_caller]
    fn sub_assign(&mut self, rhs: &VectorSlice<T, M>) {
        assert_eq!(rhs.size(), self.size(), "Can not add vectors of different sizes");

        for i in 0..self.size() {
            self[i] -= rhs[i];
        }
    }
}

impl<T: Scalar, const N: usize, const M: usize> SubAssign<Vector<T, M>> for VectorSlice<T, N> {
    fn sub_assign(&mut self, rhs: Vector<T, M>) {
        self.sub_assign(&rhs[..])
    }
}

// Multiply

impl<T: Scalar, const N: usize> MulAssign<T> for VectorSlice<T, N> {
    fn mul_assign(&mut self, rhs: T) {
        for v in self.as_slice_mut() {
            *v *= rhs;
        }
    }
}

// Dividing

impl<T: Scalar, const N: usize> DivAssign<T> for VectorSlice<T, N> {
    fn div_assign(&mut self, rhs: T) {
        for v in self.as_slice_mut() {
            *v /= rhs;
        }
    }
}

// Indexing

impl<T, const N: usize> Index<usize> for VectorSlice<T, N> {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        &self.values[index]
    }
}

impl<T, const N: usize> IndexMut<usize> for VectorSlice<T, N> {
    fn index_mut(&mut self, index: usize) -> &mut T {
        &mut self.values[index]
    }
}

impl<T, I, const N: usize> Index<I> for VectorSlice<T, N> where I: RangeBounds<usize> {
    type Output = VectorSlice<T, N>;

    fn index(&self, index: I) -> &VectorSlice<T, N> {
        let start = index.start();
        let len = index.end(N) - start;

        unsafe { core::mem::transmute(core::slice::from_raw_parts(&self.values[start] as *const T, len)) }
    }
}

impl<T, I, const N: usize> IndexMut<I> for VectorSlice<T, N> where I: RangeBounds<usize> {
    fn index_mut(&mut self, index: I) -> &mut VectorSlice<T, N> {
        let start = index.start();
        let len = index.end(N) - start;

        unsafe { core::mem::transmute(core::slice::from_raw_parts_mut(&mut self.values[start] as *mut T, len)) }
    }
}

impl<T, const N: usize> Debug for VectorSlice<T, N> where T: Debug {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<...")?;
        for (i, v) in self.values.iter().enumerate() {
            if i != 0 {
                write!(f, ", {:?}", v)?;
            } else {
                write!(f, "{:?}", v)?;
            }
        }
        write!(f, "...>")
    }
}

impl<T, const N: usize> Display for VectorSlice<T, N> where T: Display {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "<...")?;
        for (i, v) in self.values.iter().enumerate() {
            if i != 0 {
                write!(f, ", {}", v)?;
            } else {
                write!(f, "{}", v)?;
            }
        }
        write!(f, "...>")
    }
}