pub use vector::*;
pub use unit_vector::*;
pub use vector_slice::*;

pub mod vector;
pub mod vector_ops;
pub mod unit_vector;
pub mod vector_slice;
pub mod vector_calculations;
pub mod geometry_calculations;

#[macro_export]
macro_rules! vector {
    ($elem:expr; $n:expr) => (
        $crate::Vector::array([$elem; $n])
    );
    ($($x:expr),+ $(,)?) => (
        $crate::Vector::array([$($x),+])
    );
}

#[macro_export]
macro_rules! swizzle {
    ($v:expr; $($i:ident),+) => {
        $crate::Vector::array([ $(  $v . $i  ),+ ])
    };
}

#[cfg(test)]
mod vector_tests {
    #![allow(dead_code)]

    use std::ops::{AddAssign, MulAssign};
    use crate::NotNan;

    use super::*;

    #[test]
    fn byte_test() {
        let a = NotNan::new(1.0_f32).unwrap();
        let a_b: u32 = bytemuck::cast(a);
        dbg!(a_b);

        let b = Vector4::splat(a);
        let b_b: [u32; 4] = bytemuck::cast(b);
        dbg!(b_b);
    }

    #[test]
    fn test() {
        let mut a = Vector::from([1.0, 2.0, 3.0, 4.0]);
        let b = Vector4::<f64>::new(5.0, 6.0, 7.0, -4.0);

        let _: Vector4f = b.cast_as();

        b.x;
        b.r;
        b.s;

        println!("a + b = {:?}", a + b);

        println!("a[0..3] = {:?}", &a[0..3]);
        println!("b[0..3] = {:?}", &b[0..3]);

        let c: Vector3<_> = (&a[0..3]).into();

        println!("c = {}", c);

        let d = Vector2::new(std::f64::consts::PI, std::f64::consts::E);
        let e = &a[1..=2];

        println!("{} + {} = {}", d, e, d + e.vector());

        let f = d + e.vector();

        println!("|{}| = {}", f, f.norm());
        println!("{}/|{}| = {}", f, f, f.unit());

        println!("old a = {}", a);
        let g = &mut a[1..=2];
        g.set(Vector2::new(0.0, 0.0));
        println!("new a = {}", a);
        let h = &mut a[1..=2];
        h.set_slice(&f[..]);
        println!("new new a = {}", a);

        println!("Test: {}", Vector2::from(&f[..]).norm());
        let mut f = f.unit().into_vector();
        println!("Test: {}", Vector2::from(&f[..]).norm());
        // f[1..].set(Vector1::from(&f[1..]).unit());
        // f[1..].set_with(|v| Vector1::from(v).unit());
        f[1..].unit_assign();
        assert_eq!(f[1..].norm(), 1.0);
        println!("Test: {}", f);

        println!("Test 2: {:?}", a.as_slice());
        println!("Test 2: {:?}", &a[..].as_slice());

        println!("old a = {}", a);
        (&mut a[1..3]).mul_assign(2.0);
        println!("new a = {}", a);
        let mut k = Vector2::new(1.0, -1.0) + (&mut a[1..3]).vector();
        println!("k = {}", k);

        let j = Vector2::new(1.0, -1.0);

        k[..].add_assign(j);
        k[..].add_assign(&j[..]);
        println!("k = {}", k);

        dbg!(UnitVector3::<f64>::unit_x() * Vector3::<f64>::unit_x());
        dbg!(Vector3::<f64>::unit_x() * UnitVector3::<f64>::unit_x());

        assert_eq!(Vector3::new(1.0, 0.0, 0.0).cross(Vector3::new(0.0, 1.0, 0.0)), Vector3::new(0.0, 0.0, 1.0));
        assert_eq!(Vector3::new(0.0, 1.0, 0.0).cross(Vector3::new(1.0, 0.0, 0.0)), Vector3::new(0.0, 0.0, -1.0));

        assert_eq!(Vector3f::unit_x() ^ Vector3f::unit_y(), Vector3f::unit_z());
        assert_eq!(Vector3f::cross_product([Vector3f::unit_x(), Vector3f::unit_y()]), Vector3f::unit_z());
        assert_eq!(Vector4f::cross_product([Vector4f::unit_x(), Vector4f::unit_y(), Vector4f::unit_z()]), Vector4f::unit_w());
        assert_eq!(Vector::<f32, 5>::cross_product([Vector::<f32, 5>::unit_n(0), Vector::<f32, 5>::unit_n(1), Vector::<f32, 5>::unit_n(2), Vector::<f32, 5>::unit_n(3)]), Vector::<f32, 5>::unit_n(4));
        assert_eq!(Vector::<f32, 6>::cross_product([Vector::<f32, 6>::unit_n(0), Vector::<f32, 6>::unit_n(1), Vector::<f32, 6>::unit_n(2), Vector::<f32, 6>::unit_n(3), Vector::<f32, 6>::unit_n(4)]), Vector::<f32, 6>::unit_n(5));

        let z = &mut a[1..3];
        dbg!(&z);
        z[0] = -z[0];
        dbg!(&z);

        // Swizzle test
        {
            let k = vector![1, 2, 3, 4];

            let j = k >> [3, 0];

            assert_eq!(j, vector![4, 1]);

            let l = swizzle!(k; w, x);

            assert_eq!(l, j);

            let r = swizzle!(k; r, r, r, r);
            assert_eq!(r, vector![1, 1, 1, 1]);
            assert_eq!(r, k >> [0, 0, 0, 0]);
        }

        {
            const A: Vector<i32, 4> = vector![1, 2, 3, 4];
            const B: Vector<i32, 4> = vector![-6, -5, -4, 3];
        }

        {
            let _g = vector![3; 4];
        }
    }
}