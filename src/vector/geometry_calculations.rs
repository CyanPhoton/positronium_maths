use crate::{FloatScalar, Scalar, UnitVector, Vector, Vector3, Vector4, ZeroInit};

impl<T: FloatScalar, const N: usize> Vector<T, N> {
    pub fn project_to_direction(self, direction: UnitVector<T, N>) -> Vector<T, N> {
        let direction = direction.into_vector();
        direction * (self * direction)
    }

    pub fn project_to_plane(self, plane_normal: UnitVector<T, N>) -> Vector<T, N> {
        self - self.project_to_direction(plane_normal)
    }

    pub fn reflect_on_plane(self, plane_normal: UnitVector<T, N>) -> Vector<T, N> {
        let two = T::ONE + T::ONE;
        self - self.project_to_direction(plane_normal) * two
    }
}

impl<T: Scalar> Vector4<T> {
    pub const fn zero_point() -> Vector4<T> {
        Vector4::new(T::ZERO, T::ZERO, T::ZERO, T::ONE)
    }

    pub const fn zero_dir() -> Vector4<T> {
        Vector4::ZERO
    }

    pub const fn space(self) -> Vector3<T> {
        let [x, y, z, _] = self.values;
        Vector3::new(x, y, z)
    }

    pub fn perspective_division(self) -> Vector3<T> {
        let (v, w) = self.pop();
        v / w
    }
}