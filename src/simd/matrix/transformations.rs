use std::fmt::Display;
use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{Arithmetic, ControlAxis, DepthDomain, DepthRange, ForwardDirection, Matrix3, NdcConfig, Radians, FloatScalar, simd::SimdMatrix, SimdScalar, simd::SimdVector, simd::SimdVector2, simd::SimdVector4, UnitVector3, Vector3};

impl<T: SimdScalar, const N: usize> SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    #[inline(always)]
    pub fn scale(factor: T) -> Self {
        Self::identity() * factor
    }

    #[inline(always)]
    pub fn scale_comp_vector(factors: SimdVector<T, N>) -> Self {
        Self::diagonal_vector(factors)
    }

    #[inline(always)]
    pub fn scale_comp(factors: [T; N]) -> Self {
        Self::diagonal(factors)
    }
}


impl<T: SimdScalar> SimdMatrix<T, 4, 4> {
    #[inline(always)]
    pub fn space_scale(factor: T) -> Self {
        Matrix3::scale(factor).to_homogeneous_simd(T::ONE)
    }

    #[inline(always)]
    pub fn space_scale_comp_vector(factors: Vector3<T>) -> Self {
        Matrix3::scale_comp_vector(factors).to_homogeneous_simd(T::ONE)
    }

    #[inline(always)]
    pub fn space_scale_comp(factors: [T; 3]) -> Self {
        Matrix3::scale_comp(factors).to_homogeneous_simd(T::ONE)
    }

    #[inline(always)]
    pub fn space_translate(translation: Vector3<T>) -> Self {
        Self::new(SimdVector4::unit_x(), SimdVector4::unit_y(), SimdVector4::unit_z(), translation.with_w_simd(T::ONE))
    }

    #[inline(always)]
    pub fn space_rotate_axis<R>(angle_radians: R, axis: UnitVector3<T>) -> Self where T: FloatScalar, R: Into<Radians<T>> {
        Matrix3::rotate_axis(angle_radians, axis).to_homogeneous_simd(T::ONE)
    }

    #[inline(always)]
    pub fn space_look_at(eye: Vector3<T>, target: Vector3<T>, up: UnitVector3<T>, ndc: NdcConfig) -> Self where T: FloatScalar + Display, Simd<T, 4>: Arithmetic {
        let signs = ndc.sign_vector::<T>();

        let dir = (target - eye).unit();
        let new_up = up.into_vector().project_to_plane(dir).unit().into_vector();
        let dir = dir.into_vector();
        let side = dir ^ new_up;

        let offset = Self::space_translate(-target); // Moves target to origin
        let rotate = Matrix3::row_vectors([side * signs.x, new_up * signs.y, dir * signs.z]).to_homogeneous_simd(T::ONE); // Rotates about origin
        let translate = Self::space_translate((Vector3::unit_z() * signs.z) * (target - eye).norm()); // Moves camera backwards

        translate * rotate * offset
    }

    #[inline(always)]
    pub fn projection<R>(camera_fov: R, camera_size: T, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar, R: Into<Radians<T>>, Simd<T, 2>: Arithmetic {
        let two = T::ONE + T::ONE;

        let fov = camera_fov.into();
        let fov_factor = (fov / two).tan();
        let half_size = camera_size.div(two);

        let (k_x, k_y) = match control_axis {
            ControlAxis::Vertical => {
                // k_y = k_0, k_x = k_1 = a^-1 = w_0 / w_1 = w_y / w_x = (W / H)^-1
                (aspect_ratio.recip(), T::ONE)
            }
            ControlAxis::Horizontal => {
                // k_x = k_0, k_y = k_1 = a^-1 = w_0 / w_1 = w_x / w_y = (W / H)
                (T::ONE, aspect_ratio)
            }
            ControlAxis::MinAxis => if aspect_ratio >= T::ONE {
                // (W / H) >= 1 => H <= W, so use Vertical Axis Rule
                (aspect_ratio.recip(), T::ONE)
            } else {
                // (W / H) < 1 => W < H, so use Horizontal Axis Rule
                (T::ONE, aspect_ratio)
            },
            ControlAxis::MaxAxis => if aspect_ratio >= T::ONE {
                // (W / H) >= 1 => W >= H, so use Horizontal Axis Rule
                (T::ONE, aspect_ratio)
            } else {
                // (W / H) < 1 => H > W, so use Vertical Axis Rule
                (aspect_ratio.recip(), T::ONE)
            }
        };

        let depth_factors = match depth_domain {
            DepthDomain::Finite { near, far } => {
                let denom = far - near;
                SimdMatrix::rows([
                    [depth_range.far * far - depth_range.near * near, depth_range.far - depth_range.near],
                    [near * far * (depth_range.near - depth_range.far), depth_range.near * far - depth_range.far * near],
                ]) / denom
            }
            DepthDomain::Infinite { near } => {
                SimdMatrix::rows([
                    [depth_range.far, T::ZERO],
                    [near * (depth_range.near - depth_range.far), depth_range.near]
                ])
            }
        };

        let s: T = forward_dir.sign::<T>();
        let z_components = depth_factors * SimdVector2::new(fov_factor, half_size);

        SimdMatrix::rows([
            [k_x, T::ZERO, T::ZERO, T::ZERO],
            [T::ZERO, k_y, T::ZERO, T::ZERO],
            [T::ZERO, T::ZERO, z_components.x, s * z_components.y],
            [T::ZERO, T::ZERO, s * fov_factor, half_size],
        ])
    }

    #[inline(always)]
    pub fn perspective_projection<R>(camera_fov: R, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar, R: Into<Radians<T>>, Simd<T, 2>: Arithmetic {
        Self::projection(camera_fov, T::ZERO, aspect_ratio, control_axis, depth_domain, forward_dir, depth_range)
    }

    #[inline(always)]
    pub fn orthogonal_projection(camera_size: T, aspect_ratio: T, control_axis: ControlAxis, depth_domain: DepthDomain<T>, forward_dir: ForwardDirection, depth_range: DepthRange<T>) -> Self where T: FloatScalar, Simd<T, 2>: Arithmetic {
        Self::projection(Radians::new(T::ZERO), camera_size, aspect_ratio, control_axis, depth_domain, forward_dir, depth_range)
    }
}