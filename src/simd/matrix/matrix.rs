use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::{Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, Deref, DerefMut, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};
use std::simd::{LaneCount, Simd, SupportedLaneCount};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};
use paste::paste;

use crate::{Arithmetic, CastAs, Matrix, SignedArithmetic, SignedScalar, simd::SimdVector, SimdScalar, Vector, ZeroInit};
use crate::utility::array;

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct SimdMatrix<T: SimdScalar, const R: usize, const C: usize> where LaneCount<R>: SupportedLaneCount {
    pub(super) columns: [SimdVector<T, R>; C],
}

impl<T: SimdScalar, const R: usize, const C: usize> Default for SimdMatrix<T, R, C> where T: Default, LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    fn default() -> Self {
        SimdMatrix::column_vectors([SimdVector::splat(T::default()); C])
    }
}

/// Safety: Is repr(c) and single field is Zeroable
unsafe impl<T: SimdScalar, const R: usize, const C: usize> bytemuck::Zeroable for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {}

/// Safety: Is repr(c) and single field is Pod
unsafe impl<T: SimdScalar, const R: usize, const C: usize> bytemuck::Pod for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {}

impl<T: SimdScalar, const R: usize, const C: usize> ZeroInit for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    const ZERO: Self = SimdMatrix::column_vectors([SimdVector::zero(); C]);
}

impl<T: SimdScalar, const R: usize, const C: usize> SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    pub const fn zero() -> Self {
        SimdMatrix::ZERO
    }

    #[inline(always)]
    pub const fn splat(value: T) -> Self {
        SimdMatrix::column_vectors([SimdVector::splat(value); C])
    }

    #[inline(always)]
    pub const fn column_vectors(columns: [SimdVector<T, R>; C]) -> Self {
        SimdMatrix {
            columns,
        }
    }

    #[inline(always)]
    pub fn row_vectors(rows: [SimdVector<T, C>; R]) -> Self where LaneCount<C>: SupportedLaneCount {
        SimdMatrix::column_vectors(rows).transpose()
    }

    #[inline(always)]
    pub fn columns(columns: [[T; R]; C]) -> Self {
        SimdMatrix::column_vectors(array::map(columns, SimdVector::array))
    }

    #[inline(always)]
    pub fn rows(rows: [[T; C]; R]) -> Self {
        SimdMatrix::from_fn(|r, c| rows[r][c])
    }

    #[inline(always)]
    pub fn array_column_wise(array: [T; R * C]) -> Self {
        let ptr = &array as *const _ as *const Self;
        // Safe due to #[repr(C)] and layout guarantees of arrays
        // Due to higher alignment of Simd, need to use unaligned read
        let result = unsafe { ptr.read_unaligned() };
        // forget is not needed since Scalar: Copy, so no Drop
        result
    }

    #[inline(always)]
    pub fn array_row_wise(array: [T; C * R]) -> Self {
        SimdMatrix::from_fn(|r, c| array[c + C * r])
    }

    #[inline(always)]
    pub fn slice_column_wise(values: &[T]) -> Self {
        SimdMatrix::from_fn(|r, c| values[r + R * c])
    }

    #[inline(always)]
    pub fn slice_row_wise(values: &[T]) -> Self {
        SimdMatrix::from_fn(|r, c| values[c + C * r])
    }

    #[inline(always)]
    pub fn from_fn<F>(mut f: F) -> Self where F: FnMut(usize, usize) -> T {
        SimdMatrix::column_vectors(array::from_fn(|c| SimdVector::from_fn(|r| f(r, c))))
    }

    #[inline(always)]
    pub fn try_from_fn<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize, usize) -> Result<T, E> {
        Ok(SimdMatrix::column_vectors(array::try_from_fn(|c| SimdVector::try_from_fn(|r| f(r, c)))?))
    }

    #[inline(always)]
    pub fn from_fn_columns<F>(mut f: F) -> Self where F: FnMut(usize) -> [T; R] {
        SimdMatrix::columns(array::from_fn(|c| f(c)))
    }

    #[inline(always)]
    pub fn try_from_fn_columns<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<[T; R], E> {
        Ok(SimdMatrix::columns(array::try_from_fn(|c| f(c))?))
    }

    #[inline(always)]
    pub fn from_fn_column_vectors<F>(mut f: F) -> Self where F: FnMut(usize) -> SimdVector<T, R> {
        SimdMatrix::column_vectors(array::from_fn(|c| f(c)))
    }

    #[inline(always)]
    pub fn try_from_fn_column_vectors<F, E>(mut f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<SimdVector<T, R>, E> {
        Ok(SimdMatrix::column_vectors(array::try_from_fn(|c| f(c))?))
    }

    #[inline(always)]
    pub fn from_iter_column_wise<I: IntoIterator<Item=T>>(iter: I) -> Self {
        let mut iter = iter.into_iter();
        SimdMatrix::column_vectors(array::from_fn(|_| SimdVector::from_fn(|_| iter.next().expect("Insufficient Elements"))))
    }

    #[inline(always)]
    pub fn from_iter_row_wise<I: IntoIterator<Item=T>>(iter: I) -> Self where LaneCount<C>: SupportedLaneCount {
        SimdMatrix::from_iter_column_wise(iter).transpose()
    }

    #[must_use]
    #[inline(always)]
    pub fn swap_rows(mut self, a: usize, b: usize) -> Self {
        for c in 0..C {
            self.columns[c].swap_assign(a, b);
        }

        self
    }

    pub fn swap_rows_assign(&mut self, a: usize, b: usize) {
        for c in 0..C {
            self.columns[c].swap_assign(a, b);
        }
    }

    #[must_use]
    #[inline(always)]
    pub fn swap_columns(mut self, a: usize, b: usize) -> Self {
        self.columns.swap(a, b);

        self
    }

    pub fn swap_columns_assign(&mut self, a: usize, b: usize) {
        self.columns.swap(a, b);
    }

    /// Can also use & operator
    #[inline(always)]
    pub fn comp_mul(self, rhs: Self) -> Self where Simd<T, R>: Arithmetic {
        self & rhs
    }

    /// Can also use | operator
    #[inline(always)]
    pub fn comp_div(self, rhs: Self) -> Self where Simd<T, R>: Arithmetic {
        self | rhs
    }

    #[inline(always)]
    pub fn map<V: SimdScalar, F: FnMut(T) -> V>(self, mut map: F) -> SimdMatrix<V, R, C> {
        SimdMatrix::from_fn(|r, c| map(self[r][c]))
    }

    #[inline(always)]
    pub fn try_map<V: SimdScalar, E, F: FnMut(T) -> Result<V, E>>(self, mut map: F) -> Result<SimdMatrix<V, R, C>, E> {
        SimdMatrix::try_from_fn(|r, c| map(self[r][c]))
    }

    #[inline(always)]
    pub fn zip_map<U: SimdScalar, V: SimdScalar, F: FnMut(T, V) -> U>(self, other: SimdMatrix<V, R, C>, mut map: F) -> SimdMatrix<U, R, C> {
        SimdMatrix::from_iter_column_wise(self.into_iter_column_wise().zip(other.into_iter_column_wise()).map(|(u, v)| map(u, v)))
    }

    #[inline(always)]
    pub fn cast_as<V: SimdScalar>(self) -> SimdMatrix<V, R, C> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }

    #[inline(always)]
    pub fn comp_into<V: SimdScalar>(self) -> SimdMatrix<V, R, C> where T: Into<V> {
        self.map(|v| v.into())
    }

    #[inline(always)]
    pub fn comp_try_into<V: SimdScalar, E>(self) -> Result<SimdMatrix<V, R, C>, E> where T: TryInto<V, Error=E> {
        self.try_map(|v| v.try_into())
    }

    #[inline(always)]
    pub fn as_columns(&self) -> &[[T; R]; C] {
        unsafe { std::mem::transmute(&self.columns) }
    }

    #[inline(always)]
    pub fn as_columns_mut(&mut self) -> &mut [[T; R]; C] {
        unsafe { std::mem::transmute(&mut self.columns) }
    }

    #[inline(always)]
    pub fn as_column_vectors(&self) -> &[SimdVector<T, R>; C] {
        &self.columns
    }

    #[inline(always)]
    pub fn as_column_vectors_mut(&mut self) -> &mut [SimdVector<T, R>; C] {
        &mut self.columns
    }

    #[inline(always)]
    pub fn as_array_column_wise(&self) -> &[T; R * C] {
        // Safe due to #[repr(C)] and layout guarantees of arrays
        unsafe { std::mem::transmute(self) }
    }

    #[inline(always)]
    pub fn as_array_column_wise_mut(&mut self) -> &mut [T; R * C] {
        // Safe due to #[repr(C)] and layout guarantees of arrays
        unsafe { std::mem::transmute(self) }
    }

    #[inline(always)]
    pub fn as_slice_column_wise(&self) -> &[T] {
        unsafe { core::slice::from_raw_parts(&self[0][0] as *const T, R * C) }
    }

    #[inline(always)]
    pub fn as_slice_column_wise_mut(&mut self) -> &mut [T] {
        unsafe { core::slice::from_raw_parts_mut(&mut self[0][0] as *mut T, R * C) }
    }

    #[inline(always)]
    pub fn into_arrays(self) -> [[T; R]; C] {
        array::map(self.columns, SimdVector::into_array)
    }

    #[inline(always)]
    pub fn into_array_column_wise(self) -> [T; R * C] {
        let ptr = &self as *const _ as *const [T; R * C];
        // Safe due to #[repr(C)] and layout guarantees of arrays
        let result = unsafe { ptr.read() };
        // forget is not needed since Scalar: Copy, so no Drop
        result
    }

    #[inline(always)]
    pub fn column_vector(&self, column: usize) -> SimdVector<T, R> {
        self.columns[column]
    }

    #[inline(always)]
    pub fn row_vector(&self, row: usize) -> Vector<T, C> {
        Vector::from_fn(|c| self[row][c])
    }

    #[inline(always)]
    pub fn into_column_vectors(self) -> [SimdVector<T, R>; C] {
        self.columns
    }

    #[inline(always)]
    pub fn into_row_vectors(self) -> [SimdVector<T, C>; R] where LaneCount<C>: SupportedLaneCount {
        self.transpose().columns
    }

    #[inline(always)]
    pub fn into_iter_column_wise(self) -> impl Iterator<Item=T> {
        self.columns.into_iter().flat_map(|column| column.into_iter())
    }

    #[inline(always)]
    pub fn iter_column_wise(&self) -> impl Iterator<Item=&T> {
        self.columns.iter().flat_map(|column| column.iter())
    }

    #[inline(always)]
    pub fn iter_column_wise_mut(&mut self) -> impl Iterator<Item=&mut T> {
        self.columns.iter_mut().flat_map(|column| column.iter_mut())
    }
}

impl<T: SimdScalar, const N: usize> SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    pub fn identity() -> Self {
        let mut identity = Self::ZERO;
        for i in 0..N {
            identity[i][i] = T::ONE;
        }
        identity
    }

    #[inline(always)]
    pub fn diagonal_vector(v: SimdVector<T, N>) -> Self {
        Self::diagonal(v.into_array())
    }

    #[inline(always)]
    pub fn diagonal(v: [T; N]) -> Self {
        Self::diagonal_iter(v)
    }

    #[inline(always)]
    pub fn diagonal_iter<I>(iter: I) -> Self where I: IntoIterator<Item=T> {
        let mut result = Self::ZERO;
        let mut iter = iter.into_iter();
        for i in 0..N {
            result[i][i] = iter.next().expect("Insufficient Elements");
        }
        result
    }
}

impl<T: SimdScalar, const R: usize> SimdMatrix<T, R, 1> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    pub fn into_vector(self) -> SimdVector<T, R> {
        self.columns[0]
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> From<Matrix<T, R, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    fn from(matrix: Matrix<T, R, C>) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| matrix.columns[c].into())
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> From<SimdMatrix<T, R, C>> for Matrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    fn from(matrix: SimdMatrix<T, R, C>) -> Self {
        Matrix::from_fn_column_vectors(|c| matrix.columns[c].into())
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> From<[[T; R]; C]> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    fn from(columns: [[T; R]; C]) -> Self {
        SimdMatrix::columns(columns)
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> From<[SimdVector<T, R>; C]> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    fn from(columns: [SimdVector<T, R>; C]) -> Self {
        SimdMatrix::column_vectors(columns)
    }
}

// Addition

impl<T: SimdScalar, const R: usize, const C: usize> Add for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn add(self, rhs: Self) -> Self::Output {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] + rhs.columns[c])
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> AddAssign for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn add_assign(&mut self, rhs: Self) {
        for c in 0..C {
            self.columns[c] += rhs.columns[c];
        }
    }
}

// Subtraction

impl<T: SimdScalar, const R: usize, const C: usize> Sub for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn sub(self, rhs: Self) -> Self::Output {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] - rhs.columns[c])
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> SubAssign for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn sub_assign(&mut self, rhs: Self) {
        for c in 0..C {
            self.columns[c] -= rhs.columns[c];
        }
    }
}

// Multiplication

impl<T: SimdScalar, const R: usize, const K: usize, const C: usize> Mul<SimdMatrix<T, K, C>> for SimdMatrix<T, R, K> where LaneCount<R>: SupportedLaneCount, LaneCount<K>: SupportedLaneCount, Simd<T, R>: Arithmetic, Simd<T, K>: Arithmetic {
    type Output = SimdMatrix<T, R, C>;

    #[inline(always)]
    fn mul(self, rhs: SimdMatrix<T, K, C>) -> Self::Output {
        let mut result = SimdMatrix::ZERO;
        for c in 0..C {
            result.columns[c] = self * rhs.columns[c];
        }
        result
    }
}

impl<T: SimdScalar, const N: usize> MulAssign<SimdMatrix<T, N, N>> for SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn mul_assign(&mut self, rhs: SimdMatrix<T, N, N>) {
        *self = *self * rhs;
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> Mul<SimdVector<T, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, LaneCount<C>: SupportedLaneCount, Simd<T, R>: Arithmetic, Simd<T, C>: Arithmetic {
    type Output = SimdVector<T, R>;

    #[inline(always)]
    fn mul(self, rhs: SimdVector<T, C>) -> Self::Output {
        let mut result = SimdVector::ZERO;
        for c in 0..C {
            result += self.columns[c] * rhs[c];
        }
        result
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> Mul<SimdMatrix<T, R, C>> for SimdVector<T, R> where LaneCount<R>: SupportedLaneCount, LaneCount<C>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = SimdVector<T, C>;

    #[inline(always)]
    fn mul(self, rhs: SimdMatrix<T, R, C>) -> Self::Output {
        SimdVector::from_fn(|c| self * rhs.columns[c])
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> Mul<T> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn mul(self, rhs: T) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] * rhs)
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> MulAssign<T> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn mul_assign(&mut self, rhs: T) {
        for c in 0..C {
            self.columns[c] *= rhs;
        }
    }
}

macro_rules! matrix_pre_scale {
    ($($t:ty),+) => {
        $(impl<const R: usize, const C: usize> Mul<SimdMatrix<$t, R, C>> for $t where LaneCount<R>: SupportedLaneCount, Simd<$t, R>: Arithmetic {
            type Output = SimdMatrix<$t, R, C>;

            #[inline(always)]
            fn mul(self, rhs: SimdMatrix<$t, R, C>) -> SimdMatrix<$t, R, C> {
                SimdMatrix::from_fn_column_vectors(|c| self * rhs.columns[c])
            }
        })
        *
    };
}

matrix_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

impl<T: SimdScalar, const R: usize, const C: usize> BitAnd<SimdMatrix<T, R, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = SimdMatrix<T, R, C>;

    #[inline(always)]
    fn bitand(self, rhs: SimdMatrix<T, R, C>) -> Self::Output {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] & rhs.columns[c])
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> BitAndAssign<SimdMatrix<T, R, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn bitand_assign(&mut self, rhs: SimdMatrix<T, R, C>) {
        for c in 0..C {
            self.columns[c] &= rhs.columns[c];
        }
    }
}

// Division

impl<T: SimdScalar, const R: usize, const C: usize> Div<T> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn div(self, rhs: T) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] / rhs)
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> DivAssign<T> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn div_assign(&mut self, rhs: T) {
        for c in 0..C {
            self.columns[c] /= rhs;
        }
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> BitOr<SimdMatrix<T, R, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    type Output = SimdMatrix<T, R, C>;

    #[inline(always)]
    fn bitor(self, rhs: SimdMatrix<T, R, C>) -> Self::Output {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c] | rhs.columns[c])
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> BitOrAssign<SimdMatrix<T, R, C>> for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: Arithmetic {
    fn bitor_assign(&mut self, rhs: SimdMatrix<T, R, C>) {
        for c in 0..C {
            self.columns[c] |= rhs.columns[c];
        }
    }
}

macro_rules! matrix_pre_scale {
    ($($t:ty),+) => {
        $(impl<const R: usize, const C: usize> BitOr<SimdMatrix<$t, R, C>> for $t where LaneCount<R>: SupportedLaneCount, Simd<$t, R>: Arithmetic {
            type Output = SimdMatrix<$t, R, C>;

            #[inline(always)]
            fn bitor(self, rhs: SimdMatrix<$t, R, C>) -> SimdMatrix<$t, R, C> {
                SimdMatrix::from_fn_column_vectors(|c| self * rhs.columns[c])
            }
        })
        *
    };
}

matrix_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

// Negation

impl<T: SimdScalar + SignedScalar, const R: usize, const C: usize> Neg for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: SignedArithmetic {
    type Output = Self;

    #[inline(always)]
    fn neg(self) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| -self.columns[c])
    }
}

// Deref

impl<T: SimdScalar, const R: usize, const C: usize> Deref for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    type Target = Matrix<T, R, C>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> DerefMut for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

#[cfg(test)]
mod partial_index_tests {
    use crate::*;
    use crate::simd::*;

    #[test]
    fn test() {
        let m = SimdMatrix4d::from_iter_column_wise((0..16).map(|i| i as f64));

        let _a = &m[0];
        let _b = &m[1];

        assert_eq!(0.0, m[0][0]);
        assert_eq!(1.0, m[1][0]);
        assert_eq!(2.0, m[2][0]);
        assert_eq!(3.0, m[3][0]);
        assert_eq!(4.0, m[0][1]);
        assert_eq!(8.0, m[0][2]);
        assert_eq!(12.0, m[0][3]);
        assert_eq!(13.0, m[1][3]);
        assert_eq!(14.0, m[2][3]);
        assert_eq!(15.0, m[3][3]);

        let mut n = SimdMatrix4d::from_iter_column_wise((0..16).map(|i| i as f64));

        n[3][2] = -0.5;
        n[0][1] = 7.5;

        assert_eq!(-0.5, n[3][2]);
        assert_eq!(7.5, n[0][1]);

        let t = &m[1][1..4];
        let _t: Matrix<_, 1, 3> = t.row_matrix();
        let _t: Vector<_, 3> = m[1][1..4].vector();

        let t = &m[1..4][1];
        let _t: Matrix<_, 3, 1> = t.column_matrix();
        let _t: Vector<_, 3> = m[1..4][1].vector();
    }
}

macro_rules! matrix_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            pub type [<$nick_base $size>]<T> = SimdMatrix<T, $size, $size>;

            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

matrix_nicks!(SimdMatrix, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, usize, isize, f32, f64);
matrix_nicks!(SimdMatrix, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, usize, isize, f32, f64);
matrix_nicks!(SimdMatrix, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, usize, isize, f32, f64);

macro_rules! matrix_nicks_rect {
    ($nick_base:ident, $row:literal, $col:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),* ; $($cols:literal),+ ) => {
        paste! {

            impl<T: SimdScalar> SimdMatrix<T, $row, $col> {
                #[inline(always)]
                pub fn new($( [< column_ $cols >]: SimdVector<T, $row> ),+) -> Self {
                    Self::column_vectors( [ $( [< column_ $cols >] ),+ ] )
                }
            }

            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col>]<T> = Matrix<T, $row, $col>;

            $(

            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col $suffix>] = [<$nick_base $row x $col>]<$var>;)
            *

            $(
            #[cfg(feature = "rectangle_matrix_nicks")]
            pub type [<$nick_base $row x $col $exp_var>] = [<$nick_base $row x $col>]<$exp_var>;)
            *
        }
    };
}

matrix_nicks_rect!(SimdMatrix, 1, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0);
matrix_nicks_rect!(SimdMatrix, 2, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0);
matrix_nicks_rect!(SimdMatrix, 4, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0);
matrix_nicks_rect!(SimdMatrix, 1, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1);
matrix_nicks_rect!(SimdMatrix, 2, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1);
matrix_nicks_rect!(SimdMatrix, 4, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1);
matrix_nicks_rect!(SimdMatrix, 1, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1, 2, 3);
matrix_nicks_rect!(SimdMatrix, 2, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1, 2, 3);
matrix_nicks_rect!(SimdMatrix, 4, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, f32, f64 ; 0, 1, 2, 3);

impl<T: SimdScalar, const R: usize, const C: usize> Debug for SimdMatrix<T, R, C> where T: Debug, LaneCount<R>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.deref())
    }
}

impl<T: SimdScalar, const R: usize, const C: usize> Display for SimdMatrix<T, R, C> where T: Display, LaneCount<R>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.deref())
    }
}