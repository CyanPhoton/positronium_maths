pub use matrix::*;

pub mod matrix;
pub mod matrix_calculations;
pub mod matrix_ops;
pub mod transformations;

#[macro_export]
macro_rules! simd_matrix {
    ($elem:expr; ($r:expr, $c:expr)) => (
        $crate::simd::SimdMatrix::<_, $r, $c>::array_row_wise([$elem; $r * $c])
    );
    ($($x:expr),+ $(,)?) => (
        $crate::simd::SimdMatrix::array_row_wise([$($x),+])
    );
}