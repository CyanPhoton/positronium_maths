use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{Arithmetic, array, Matrix, FloatScalar, SignedScalar, simd::SimdMatrix, SimdOps, SimdScalar, simd::SimdVector, ZeroInit};
use crate::matrix::{Cofactor, Invert};

impl<T: SimdScalar, const R: usize, const C: usize> SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, LaneCount<C>: SupportedLaneCount {
    #[inline(always)]
    pub fn transpose(self) -> SimdMatrix<T, C, R> {
        let mut vectors = array::map(self.columns, |c| c.values);
        let mut new_vectors = [Simd::ZERO; C];
        let (mut read, mut write) = (&mut vectors, &mut new_vectors);
        if R >= C {
            let steps = C.ilog2();

            for _ in 0..steps {
                for i in 0..C/2 {
                    let (a, b) = read[i].interleave(read[i + C/2]);
                    write[i*2] = a;
                    write[i*2 + 1] = b;
                }
                (read, write) = (write, read);
            }
        } else {
            let steps = R.ilog2();

            for _ in 0..steps {
                for i in 0..C/2 {
                    let (a, b) = read[i*2].deinterleave(read[i*2 + 1]);
                    write[i] = a;
                    write[i + C/2] = b;
                }
                (read, write) = (write, read);
            }
        }
        if (C.min(R)/2)%2 == 0 {
            // Safety: Ptr is parented to be valid and to point to a contiguous region of the same size,
            // unaligned is needed as going from 2x4 -> 4x2 requires higher alignment
            unsafe { std::ptr::read_unaligned(vectors.as_ptr() as *const _) }
        } else {
            // Safety: Ptr is parented to be valid and to point to a contiguous region of the same size,
            // unaligned is needed as going from 2x4 -> 4x2 requires higher alignment
            unsafe { std::ptr::read_unaligned(new_vectors.as_ptr() as *const _) }
        }
    }
}


impl<T: SimdScalar, const N: usize> SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount {
    pub fn transpose_assign(&mut self) {
        *self = self.transpose();
    }

    pub fn trace(&self) -> T where Simd<T, N>: SimdOps<N, Element=T> {
        SimdVector::comp_sum(&SimdVector::from_fn(|i| self[i][i]))
    }

    #[inline(always)]
    pub fn powi(self, n: isize) -> Option<SimdMatrix<T, N, N>> where T: FloatScalar, Self: Invert, Simd<T, N>: Arithmetic {
        let a = n.unsigned_abs();
        if n >= 0 {
            Some(self.powu(a))
        } else {
            self.invert().map(|m| m.powu(a))
        }
    }

    #[inline(always)]
    pub fn powi_unchecked(self, n: isize) -> SimdMatrix<T, N, N> where T: FloatScalar, Self: Invert, Simd<T, N>: Arithmetic {
        let a = n.unsigned_abs();
        if n >= 0 {
            self.powu(a)
        } else {
            self.invert_unchecked().powu(a)
        }
    }

    // Uses successive squaring to process in O(log2(n)) matrix multiplications
    #[inline(always)]
    pub fn powu(self, mut n: usize) -> SimdMatrix<T, N, N> where Simd<T, N>: Arithmetic {
        let mut current_total = SimdMatrix::<T, N, N>::identity();
        let mut current_ss = self;

        while n > 0 {
            if n & 1 == 1 {
                current_total = current_total * current_ss;
            }
            current_ss = current_ss * current_ss;
            n >>= 1;
        }

        current_total
    }
}

// TODO: SIMD det calculation
// TODO: SIMD cofactor calculation
// TODO: SIMD inverse calculation
// TODO: SIMD gauss jordan?
// consider: https://lxjk.github.io/2017/09/03/Fast-4x4-Matrix-Inverse-with-SSE-SIMD-Explained.html


impl<T: SimdScalar + SignedScalar, const N: usize> Cofactor for SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount, Matrix<T, N, N>: Cofactor {
    fn cofactor(&self) -> Self {
        Matrix::cofactor(self).into()
    }
}

impl<T: SimdScalar + FloatScalar, const N: usize> Invert for SimdMatrix<T, N, N> where LaneCount<N>: SupportedLaneCount, Matrix<T, N, N>: Invert {
    fn invert(&self) -> Option<Self> {
        Matrix::invert(self).map(|m| m.into())
    }

    fn invert_unchecked(&self) -> Self {
        Matrix::invert_unchecked(self).into()
    }
}

impl<T: SimdScalar + FloatScalar, const N: usize> SimdMatrix<T, N, N> where Self: Invert, LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    pub fn invert(&self) -> Option<Self> {
        <Self as Invert>::invert(self)
    }

    #[inline(always)]
    pub fn invert_unchecked(&self) -> Self {
        <Self as Invert>::invert_unchecked(self)
    }
}

impl<T: SimdScalar + FloatScalar, const R: usize, const C: usize> SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    pub fn super_augmented_gauss_jordan<const M: usize>(self, matrix: SimdMatrix<T, R, M>) -> (Self, SimdMatrix<T, R, M>) {
        let (a, b) = Matrix::super_augmented_gauss_jordan(self.into(), matrix.into());
        (a.into(), b.into())
    }
}