use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{OrdScalar, SignedScalar, simd::SimdMatrix, SimdScalar, SimdOps};

impl<T: SimdScalar, const R: usize, const C: usize> SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    #[inline(always)]
    pub fn comp_abs(self) -> Self where T: SignedScalar, Simd<T, R>: SignedScalar {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c].comp_abs())
    }

    pub fn comp_sum(&self) -> T where Simd<T, R>: SimdOps<R, Element=T> {
        let mut sum = T::ZERO;
        for c in 0..C {
            sum += self.columns[c].comp_sum();
        }
        sum
    }

    pub fn all<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter_column_wise().all(f)
    }

    pub fn zip_all<V: SimdScalar, F: FnMut(&T, &V) -> bool>(&self, other: &SimdMatrix<V, R, C>, mut f: F) -> bool {
        self.iter_column_wise().zip(other.iter_column_wise()).all(|(l, r)| f(l, r))
    }

    pub fn any<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter_column_wise().any(f)
    }

    pub fn zip_any<V: SimdScalar, F: FnMut(&T, &V) -> bool>(&self, other: &SimdMatrix<V, R, C>, mut f: F) -> bool {
        self.iter_column_wise().zip(other.iter_column_wise()).any(|(l, r)| f(l, r))
    }
}

impl<T: SimdScalar + OrdScalar, const R: usize, const C: usize> SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount, Simd<T, R>: SimdOps<R, Element=T> + OrdScalar{
    #[inline(always)]
    pub fn comp_min(self, rhs: Self) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c].comp_min(rhs.columns[c]))
    }

    #[inline(always)]
    pub fn comp_max(self, rhs: Self) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c].comp_max(rhs.columns[c]))
    }

    #[inline(always)]
    pub fn comp_clamp(self, lower: Self, upper: Self) -> Self {
        SimdMatrix::from_fn_column_vectors(|c| self.columns[c].comp_clamp(lower.columns[c], upper.columns[c]))
    }

    pub fn comp_gt(&self, other: &Self) -> bool {
        (0..C).all(|c| self.columns[c].comp_gt(&other.columns[c]))
    }

    pub fn comp_ge(&self, other: &Self) -> bool {
        (0..C).all(|c| self.columns[c].comp_ge(&other.columns[c]))
    }

    pub fn comp_lt(&self, other: &Self) -> bool {
        (0..C).all(|c| self.columns[c].comp_lt(&other.columns[c]))
    }

    pub fn comp_le(&self, other: &Self) -> bool {
        (0..C).all(|c| self.columns[c].comp_le(&other.columns[c]))
    }
}