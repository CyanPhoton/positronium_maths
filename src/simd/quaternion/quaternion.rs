use std::fmt::{Debug, Display, Formatter};
use std::hash::Hash;
use std::ops::{Add, AddAssign, Deref, DerefMut, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};
use std::simd::Simd;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};

use crate::{Arithmetic, CastAs, Quaternion, Radians, FloatScalar, SignedArithmetic, SignedScalar, SimdScalar, simd::SimdVector4, UnitVector3, Vector3, ZeroInit};
use crate::simd::quaternion::SimdUnitQuaternion;

#[repr(transparent)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash, Default)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct SimdQuaternion<T: SimdScalar>(pub(crate) SimdVector4<T>);

/// Safety: Is repr(transparent) and single field is Zeroable
unsafe impl<T: SimdScalar> bytemuck::Zeroable for SimdQuaternion<T> {}

/// Safety: Is repr(transparent) and single field is Pod
unsafe impl<T: SimdScalar> bytemuck::Pod for SimdQuaternion<T> {}

impl<T: SimdScalar> ZeroInit for SimdQuaternion<T> {
    const ZERO: Self = Self(ZeroInit::ZERO);
}

impl<T: SimdScalar> SimdQuaternion<T> {
    #[inline(always)]
    pub const fn zero() -> SimdQuaternion<T> {
        SimdQuaternion(SimdVector4::zero())
    }

    #[inline(always)]
    pub const fn new(scalar: T, vector: Vector3<T>) -> SimdQuaternion<T> {
        SimdQuaternion(vector.with_w_simd(scalar))
    }

    /// Construct the array of [scalar component, imaginary component x,y,z]
    #[inline(always)]
    pub const fn array([r, i, j, k]: [T; 4]) -> Self {
        SimdQuaternion(SimdVector4::new(i, j, k, r))
    }

    #[inline(always)]
    pub const fn tuple((r, i, j, k): (T, T, T, T)) -> Self {
        SimdQuaternion(SimdVector4::new(i, j, k, r))
    }

    #[inline(always)]
    pub const fn scalar(scalar: T) -> SimdQuaternion<T> {
        SimdQuaternion(SimdVector4::new(T::ZERO, T::ZERO, T::ZERO, scalar))
    }

    #[inline(always)]
    pub const fn vector(vector: Vector3<T>) -> SimdQuaternion<T> {
        SimdQuaternion(vector.with_w_simd(T::ZERO))
    }

    #[inline(always)]
    pub fn rotation<R>(angle: R, axis: UnitVector3<T>) -> SimdQuaternion<T> where R: Into<Radians<T>>, T: FloatScalar {
        let radians = angle.into();
        let (s, c) = (radians / (T::ONE + T::ONE)).sin_cos();

        let axis = axis.into_vector();

        // Don't flip axis, flip angle instead
        SimdQuaternion::new(c * s.signum(), axis * s.abs())
    }

    #[inline(always)]
    pub const fn identity() -> SimdQuaternion<T> {
        Self::one()
    }

    #[inline(always)]
    pub const fn one() -> SimdQuaternion<T> {
        SimdQuaternion::scalar(T::ONE)
    }

    #[inline(always)]
    pub const fn i() -> SimdQuaternion<T> {
        SimdQuaternion::vector(Vector3::unit_x())
    }

    #[inline(always)]
    pub const fn j() -> SimdQuaternion<T> {
        SimdQuaternion::vector(Vector3::unit_y())
    }

    #[inline(always)]
    pub const fn k() -> SimdQuaternion<T> {
        SimdQuaternion::vector(Vector3::unit_z())
    }

    #[inline(always)]
    pub fn inner_product(self, rhs: Self) -> T where Simd<T, 4>: Arithmetic {
        self.0.inner_product(rhs.0)
    }

    #[inline(always)]
    pub fn norm_squared(self) -> T where Simd<T, 4>: Arithmetic {
        self.inner_product(self)
    }

    #[inline(always)]
    pub fn norm(self) -> T where T: FloatScalar, Simd<T, 4>: Arithmetic {
        self.norm_squared().sqrt()
    }

    #[inline(always)]
    pub fn rotation_angle(self) -> Radians<T> where T: FloatScalar {
        let c = self.r;
        let s = self.ijk.norm();

        Radians::atan2(s, c) * (T::ONE + T::ONE)
    }

    #[inline(always)]
    pub fn rotation_axis(self) -> Option<UnitVector3<T>> where T: FloatScalar {
        self.ijk.try_unit()
    }

    #[inline(always)]
    pub fn unit(self) -> SimdUnitQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        SimdUnitQuaternion(self / self.norm())
    }

    #[inline(always)]
    pub fn try_unit(self) -> Option<SimdUnitQuaternion<T>> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        if self != Self::zero() {
            Some(SimdUnitQuaternion(self / self.norm()))
        } else {
            None
        }
    }

    pub fn unit_assign(&mut self) where T: FloatScalar, Simd<T, 4>: Arithmetic {
        *self = *self / self.norm()
    }

    #[inline(always)]
    pub fn conjugate(self) -> SimdQuaternion<T> where T: SignedScalar {
        SimdQuaternion::new(self.r, -self.ijk)
    }

    pub fn conjugate_assign(&mut self) where T: SignedScalar {
        self.xyz = -self.xyz;
    }

    #[inline(always)]
    pub fn inverse(self) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        self.conjugate() / self.norm_squared()
    }

    pub fn inverse_assign(&mut self) where T: FloatScalar, Simd<T, 4>: Arithmetic {
        *self = self.conjugate() / self.norm_squared();
    }

    #[inline(always)]
    pub fn powf(self, t: T) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        let Some(axis) = self.rotation_axis() else {
            return SimdQuaternion::scalar(self.r.abs().powf(t));
        };

        let rot = self.rotation_angle();
        let scale = self.norm();

        SimdQuaternion::rotation(rot * t, axis) * scale.powf(t)
    }

    /// Interpolates in the space of quaternions, may not be the shortest physical rotation
    #[inline(always)]
    pub fn interpolate(self, target: SimdQuaternion<T>, fraction: T) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        let fraction = fraction.clamp(T::ZERO, T::ONE);
        (target * self.inverse()).powf(fraction) * self
    }

    /// Interpolate in the rotation space, will be the shortest physical rotation
    #[inline(always)]
    pub fn interpolate_rotation(self, target: SimdQuaternion<T>, fraction: T) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: SignedArithmetic {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    #[inline(always)]
    pub fn extrapolate(self, target: SimdQuaternion<T>, fraction: T) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        (target * self.inverse()).powf(fraction) * self
    }

    #[inline(always)]
    pub fn extrapolate_rotation(self, target: SimdQuaternion<T>, fraction: T) -> SimdQuaternion<T> where T: FloatScalar, Simd<T, 4>: SignedArithmetic {
        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    #[inline(always)]
    pub fn restrict(self) -> SimdQuaternion<T> where T: SignedScalar + PartialOrd, Simd<T, 4>: SignedArithmetic {
        if self.r < T::ZERO {
            -self
        } else {
            self
        }
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict_assign(&mut self) where T: SignedScalar + PartialOrd, Simd<T, 4>: SignedArithmetic {
        *self = self.restrict();
    }

    #[inline(always)]
    pub fn map<V: SimdScalar, F: FnMut(T) -> V>(self, map: F) -> SimdQuaternion<V> {
        SimdQuaternion(self.0.map(map))
    }

    #[inline(always)]
    pub fn try_map<V: SimdScalar, E, F: FnMut(T) -> Result<V, E>>(self, map: F) -> Result<SimdQuaternion<V>, E> {
        Ok(SimdQuaternion(self.0.try_map(map)?))
    }

    #[inline(always)]
    pub fn zip_map<U: SimdScalar, V: SimdScalar, F: FnMut(T, V) -> U>(self, other: SimdQuaternion<V>, map: F) -> SimdQuaternion<U> {
        SimdQuaternion(self.0.zip_map(other.0, map))
    }

    #[inline(always)]
    pub fn cast_as<V: SimdScalar>(self) -> SimdQuaternion<V> where T: CastAs<V> {
        SimdQuaternion(self.0.cast_as())
    }

    #[inline(always)]
    pub fn comp_into<V: SimdScalar>(self) -> SimdQuaternion<V> where T: Into<V> {
        self.map(|v| v.into())
    }

    #[inline(always)]
    pub fn comp_try_into<V: SimdScalar, E>(self) -> Result<SimdQuaternion<V>, E> where T: TryInto<V, Error = E> {
        self.try_map(|v| v.try_into())
    }

    /// Convert into the array [r, i, j, k]
    #[inline(always)]
    pub fn into_array(self) -> [T; 4] {
        let [i, j, k, r] = self.0.into_array();
        [r, i, j, k]
    }
}

impl<T: SimdScalar> From<Quaternion<T>> for SimdQuaternion<T> {
    #[inline(always)]
    fn from(q: Quaternion<T>) -> Self {
        SimdQuaternion::array(q.into_array())
    }
}

impl<T: SimdScalar> From<SimdQuaternion<T>> for Quaternion<T> {
    #[inline(always)]
    fn from(q: SimdQuaternion<T>) -> Self {
        Quaternion::array(q.into_array())
    }
}

impl<T: SimdScalar> From<(T, Vector3<T>)> for SimdQuaternion<T> {
    #[inline(always)]
    fn from(v: (T, Vector3<T>)) -> Self {
        Self::new(v.0, v.1)
    }
}

impl<T: SimdScalar> From<SimdQuaternion<T>> for (T, Vector3<T>) {
    #[inline(always)]
    fn from(v: SimdQuaternion<T>) -> Self {
        (v.r, v.ijk)
    }
}

// Addition

impl<T: SimdScalar> Add for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn add(self, rhs: Self) -> Self::Output {
        SimdQuaternion(self.0 + rhs.0)
    }
}

impl<T: SimdScalar> AddAssign for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    fn add_assign(&mut self, rhs: Self) {
        self.0 += rhs.0;
    }
}

// Subtraction

impl<T: SimdScalar> Sub for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn sub(self, rhs: Self) -> Self::Output {
        SimdQuaternion(self.0 - rhs.0)
    }
}

impl<T: SimdScalar> SubAssign for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    fn sub_assign(&mut self, rhs: Self) {
        self.0 -= rhs.0;
    }
}

// Quaternion x Quaternion multiplication

impl<T: SimdScalar + SignedScalar> Mul<SimdQuaternion<T>> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn mul(self, rhs: SimdQuaternion<T>) -> Self::Output {
        SimdQuaternion(SimdVector4::new(
            self.0 * SimdVector4::new(rhs.r, rhs.k, -rhs.j, rhs.i),
            self.0 * SimdVector4::new(-rhs.k, rhs.r, rhs.i, rhs.j),
            self.0 * SimdVector4::new(rhs.j, -rhs.i, rhs.r, rhs.k),
            self.0 * SimdVector4::new(-rhs.i, -rhs.j, -rhs.k, rhs.r),
        ))
    }
}

impl<T: SimdScalar + SignedScalar> MulAssign<SimdQuaternion<T>> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    fn mul_assign(&mut self, rhs: SimdQuaternion<T>) {
        *self = *self * rhs;
    }
}

// Multiplication

impl<T: SimdScalar> Mul<T> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn mul(self, rhs: T) -> Self {
        SimdQuaternion(self.0 * rhs)
    }
}

impl<T: SimdScalar> MulAssign<T> for SimdQuaternion<T> where  Simd<T, 4>: Arithmetic {
    fn mul_assign(&mut self, rhs: T) {
        self.0 *= rhs;
    }
}

macro_rules! vector_pre_scale {
($($t:ty),+) => {
    $(impl Mul<SimdQuaternion<$t>> for $t {
        type Output = SimdQuaternion<$t>;

        #[inline(always)]
        fn mul(self, rhs: SimdQuaternion<$t>) -> SimdQuaternion<$t> {
            SimdQuaternion(self * rhs.0)
        }
    })
    *
};
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

// Quaternion / Quaternion division

impl<T: SimdScalar + FloatScalar> Div<SimdQuaternion<T>> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn div(self, rhs: SimdQuaternion<T>) -> Self::Output {
        self * rhs.inverse()
    }
}

// Division

impl<T: SimdScalar> Div<T> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn div(self, rhs: T) -> Self {
        SimdQuaternion(self.0 / rhs)
    }
}

impl<T: SimdScalar> DivAssign<T> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    fn div_assign(&mut self, rhs: T) {
        self.0 /= rhs;
    }
}

macro_rules! quaternion_pre_division {
($($t:ty),+) => {
    $(impl Div<SimdQuaternion<$t>> for $t {
        type Output = SimdQuaternion<$t>;

        #[inline(always)]
        fn div(self, rhs: SimdQuaternion<$t>) -> SimdQuaternion<$t> {
            SimdQuaternion::scalar(self) / rhs
        }
    })
    *
};
}

quaternion_pre_division!(f32, f64);

// Negation

impl<T: SimdScalar + SignedScalar> Neg for SimdQuaternion<T> where Simd<T, 4>: SignedArithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        SimdQuaternion(-self.0)
    }
}

// Alias


impl<T: SimdScalar> Deref for SimdQuaternion<T> {
    type Target = Quaternion<T>;

    fn deref(&self) -> &Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: SimdScalar> DerefMut for SimdQuaternion<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: SimdScalar> Debug for SimdQuaternion<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.deref())
    }
}

impl<T: SimdScalar> Display for SimdQuaternion<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.deref())
    }
}