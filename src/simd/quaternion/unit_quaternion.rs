use std::fmt::{Debug, Display, Formatter};
use std::ops::{Deref, Div, Mul, Neg};
use std::simd::Simd;

use crate::{Arithmetic, Radians, FloatScalar, SignedArithmetic, SignedScalar, SimdScalar, UnitQuaternion, UnitVector3};

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{Decode, Encode};
use crate::simd::quaternion::SimdQuaternion;

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct SimdUnitQuaternion<T: SimdScalar>(pub(crate) SimdQuaternion<T>);

impl<T: SimdScalar> SimdUnitQuaternion<T> {
    #[inline(always)]
    pub fn rotation<R>(angle: R, axis: UnitVector3<T>) -> SimdUnitQuaternion<T> where R: Into<Radians<T>>, T: FloatScalar {
        SimdUnitQuaternion(SimdQuaternion::rotation(angle, axis))
    }

    #[inline(always)]
    pub const fn identity() -> SimdUnitQuaternion<T> {
        SimdUnitQuaternion(SimdQuaternion::identity())
    }

    #[inline(always)]
    pub const fn one() -> SimdUnitQuaternion<T> {
        SimdUnitQuaternion(SimdQuaternion::one())
    }

    #[inline(always)]
    pub const fn i() -> SimdUnitQuaternion<T> {
        SimdUnitQuaternion(SimdQuaternion::i())
    }

    #[inline(always)]
    pub const fn j() -> SimdUnitQuaternion<T> {
        SimdUnitQuaternion(SimdQuaternion::j())
    }

    #[inline(always)]
    pub const fn k() -> SimdUnitQuaternion<T> {
        SimdUnitQuaternion(SimdQuaternion::k())
    }

    #[inline(always)]
    pub fn inner_product(self, rhs: Self) -> T where Simd<T, 4>: Arithmetic {
        self.0.inner_product(rhs.0)
    }

    #[inline(always)]
    pub fn conjugate(self) -> SimdUnitQuaternion<T> where T: SignedScalar {
        SimdUnitQuaternion(self.0.conjugate())
    }

    pub fn conjugate_assign(&mut self) where T: SignedScalar {
        self.0.conjugate_assign();
    }

    #[inline(always)]
    pub fn inverse(self) -> SimdUnitQuaternion<T> where T: SignedScalar {
        self.conjugate()
    }

    pub fn inverse_assign(&mut self) where T: SignedScalar {
        self.conjugate_assign();
    }

    #[inline(always)]
    pub fn powf(self, t: T) -> SimdUnitQuaternion<T> where T: FloatScalar {
        let Some(axis) = self.rotation_axis() else {
            return SimdUnitQuaternion::one();
        };
        let rot = self.rotation_angle();

        SimdUnitQuaternion::rotation(rot * t, axis)
    }

    /// Interpolates in the space of unit quaternions with r >= 0, may not be the shortest physical rotation
    #[inline(always)]
    pub fn interpolate(self, target: SimdUnitQuaternion<T>, fraction: T) -> SimdUnitQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        (target * self.inverse()).powf(fraction) * self
    }

    /// Interpolate in the rotation space, will be the shortest physical rotation
    #[inline(always)]
    pub fn interpolate_rotation(self, target: SimdUnitQuaternion<T>, fraction: T) -> SimdUnitQuaternion<T> where T: FloatScalar, Simd<T, 4>: SignedArithmetic {
        let fraction = fraction.clamp(T::ZERO, T::ONE);

        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    #[inline(always)]
    pub fn extrapolate(self, target: SimdUnitQuaternion<T>, fraction: T) -> SimdUnitQuaternion<T> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        (target * self.inverse()).powf(fraction) * self
    }

    #[inline(always)]
    pub fn extrapolate_rotation(self, target: SimdUnitQuaternion<T>, fraction: T) -> SimdUnitQuaternion<T> where T: FloatScalar, Simd<T, 4>: SignedArithmetic {
        let source = if self.inner_product(target) < T::ZERO {
            -self
        } else {
            self
        };

        (target * source.inverse()).powf(fraction) * source
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    #[inline(always)]
    pub fn restrict(self) -> SimdUnitQuaternion<T> where T: SignedScalar + PartialOrd, Simd<T, 4>: SignedArithmetic {
        if self.r < T::ZERO {
            -self
        } else {
            self
        }
    }

    /// If needed negate the quaternion to restrict it to the r >= domain
    pub fn restrict_assign(&mut self) where T: SignedScalar + PartialOrd, Simd<T, 4>: SignedArithmetic {
        *self = self.restrict();
    }

    #[inline(always)]
    pub fn into_quaternion(self) -> SimdQuaternion<T> {
        self.0
    }

    /// Convert into the array [r, i, j, k]
    #[inline(always)]
    pub fn into_array(self) -> [T; 4] {
        self.0.into_array()
    }
}

impl<T: SimdScalar> From<UnitQuaternion<T>> for SimdUnitQuaternion<T> {
    #[inline(always)]
    fn from(q: UnitQuaternion<T>) -> Self {
        SimdUnitQuaternion(q.0.into())
    }
}

impl<T: SimdScalar> From<SimdUnitQuaternion<T>> for UnitQuaternion<T> {
    #[inline(always)]
    fn from(q: SimdUnitQuaternion<T>) -> Self {
        UnitQuaternion(q.0.into())
    }
}

impl<T: SimdScalar + FloatScalar> From<SimdUnitQuaternion<T>> for SimdQuaternion<T> {
    #[inline(always)]
    fn from(q: SimdUnitQuaternion<T>) -> Self {
        q.into_quaternion()
    }
}

// UnitQuaternion x UnitQuaternion multiplication
// This is guaranteed to return another unit quaternion since |p * q| = |p| * |q|

impl<T: SimdScalar + FloatScalar> Mul<SimdUnitQuaternion<T>> for SimdUnitQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdUnitQuaternion<T>;

    #[inline(always)]
    fn mul(self, rhs: SimdUnitQuaternion<T>) -> Self::Output {
        SimdUnitQuaternion(self.0 * rhs.0)
    }
}

impl<T: SimdScalar + FloatScalar> Mul<SimdQuaternion<T>> for SimdUnitQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn mul(self, rhs: SimdQuaternion<T>) -> Self::Output {
        self.0 * rhs
    }
}

impl<T: SimdScalar + FloatScalar> Mul<SimdUnitQuaternion<T>> for SimdQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdQuaternion<T>;

    #[inline(always)]
    fn mul(self, rhs: SimdUnitQuaternion<T>) -> Self::Output {
        self * rhs.0
    }
}


// UnitQuaternion / UnitQuaternion division
// This is guaranteed to return another unit quaternion since |p / q| = |p| / |q|

impl<T: SimdScalar + FloatScalar> Div<SimdUnitQuaternion<T>> for SimdUnitQuaternion<T> where Simd<T, 4>: Arithmetic {
    type Output = SimdUnitQuaternion<T>;

    #[inline(always)]
    fn div(self, rhs: SimdUnitQuaternion<T>) -> Self::Output {
        self * rhs.inverse()
    }
}

// -UnitQuaternion
// This is guaranteed to return another unit quaternion since | -1 * p | = | -1 | * | p | = | p |

impl<T: SimdScalar + SignedScalar> Neg for SimdUnitQuaternion<T> where Simd<T, 4>: SignedArithmetic {
    type Output = SimdUnitQuaternion<T>;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        SimdUnitQuaternion(-self.0)
    }
}

// Alias

impl<T: SimdScalar> Deref for SimdUnitQuaternion<T> {
    type Target = SimdQuaternion<T>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl<T: SimdScalar + FloatScalar> Debug for SimdUnitQuaternion<T> where T: Debug {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}

impl<T: SimdScalar + FloatScalar> Display for SimdUnitQuaternion<T> where T: Display {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}