use std::simd::Simd;
use crate::{Arithmetic, FloatScalar, simd::SimdMatrix, SimdScalar, simd::SimdUnitQuaternion, simd::SimdVector4, UnitVector, UnitVector3, Vector3, Vector4, VectorSlice};
use crate::simd::quaternion::SimdQuaternion;

impl<T: SimdScalar + FloatScalar> Vector3<T> where Simd<T, 4>: Arithmetic {
    #[inline(always)]
    pub fn simd_rotate(self, q: SimdUnitQuaternion<T>) -> Vector3<T> {
        (q * SimdQuaternion::vector(self) * q.inverse()).xyz
    }

    #[inline(always)]
    pub fn simd_rotate_inverse(self, inverse_q: SimdUnitQuaternion<T>) -> Vector3<T> {
        (inverse_q.inverse() * SimdQuaternion::vector(self) * inverse_q).xyz
    }

    pub fn simd_rotate_assign(&mut self, q: SimdUnitQuaternion<T>) {
        *self = self.simd_rotate(q);
    }

    pub fn simd_rotate_inverse_assign(&mut self, inverse_q: SimdUnitQuaternion<T>) {
        *self = self.simd_rotate_inverse(inverse_q);
    }
}

impl<T: SimdScalar + FloatScalar> UnitVector3<T> where Simd<T, 4>: Arithmetic {
    #[inline(always)]
    pub fn simd_rotate(self, q: SimdUnitQuaternion<T>) -> UnitVector3<T> {
        UnitVector((q * SimdQuaternion::vector(self.0) * q.inverse()).xyz)
    }

    #[inline(always)]
    pub fn simd_rotate_inverse(self, inverse_q: SimdUnitQuaternion<T>) -> UnitVector3<T> {
        UnitVector((inverse_q.inverse() * SimdQuaternion::vector(self.0) * inverse_q).xyz)
    }

    pub fn simd_rotate_assign(&mut self, q: SimdUnitQuaternion<T>) {
        *self = self.simd_rotate(q);
    }

    pub fn simd_rotate_inverse_assign(&mut self, inverse_q: SimdUnitQuaternion<T>) {
        *self = self.simd_rotate_inverse(inverse_q);
    }
}

impl<T: SimdScalar + FloatScalar, const N: usize> VectorSlice<T, N> where Simd<T, 4>: Arithmetic {
    // Slice length must be 3
    pub fn simd_rotate_assign(&mut self, q: SimdUnitQuaternion<T>) {
        self.set(self.vector::<3>().simd_rotate(q));
    }

    pub fn simd_rotate_inverse_assign(&mut self, inverse_q: SimdUnitQuaternion<T>) {
        self.set(self.vector::<3>().simd_rotate_inverse(inverse_q));
    }
}

impl<T: SimdScalar + FloatScalar> Vector4<T> where Simd<T, 4>: Arithmetic {
    #[inline(always)]
    pub fn simd_space_rotate(self, q: SimdUnitQuaternion<T>) -> Vector4<T> {
        self.space().simd_rotate(q).push(self.w)
    }

    #[inline(always)]
    pub fn simd_space_rotate_inverse(self, inverse_q: SimdUnitQuaternion<T>) -> Vector4<T> {
        self.space().simd_rotate_inverse(inverse_q).push(self.w)
    }

    pub fn simd_space_rotate_assign(&mut self, q: SimdUnitQuaternion<T>) {
        *self = self.simd_space_rotate(q);
    }

    pub fn simd_space_rotate_inverse_assign(&mut self, inverse_q: SimdUnitQuaternion<T>) {
        *self = self.simd_space_rotate_inverse(inverse_q);
    }
}

impl<T: SimdScalar + FloatScalar> SimdVector4<T> where Simd<T, 4>: Arithmetic {
    #[inline(always)]
    pub fn space_rotate<Q>(mut self, q: Q) -> SimdVector4<T> where Q: Into<SimdQuaternion<T>> {
        let mut w = T::ZERO;
        std::mem::swap(&mut w, &mut self.w);
        let q = q.into();
        let mut r = q * SimdQuaternion(self) * q.inverse();
        std::mem::swap(&mut w, &mut r.w);
        r.0
    }

    #[inline(always)]
    pub fn space_rotate_inverse<Q>(mut self, inverse_q: Q) -> SimdVector4<T> where Q: Into<SimdQuaternion<T>> {
        let mut w = T::ZERO;
        std::mem::swap(&mut w, &mut self.w);
        let inverse_q = inverse_q.into();
        let mut r = inverse_q.inverse() * SimdQuaternion(self) * inverse_q;
        std::mem::swap(&mut w, &mut r.w);
        r.0
    }

    pub fn space_rotate_assign<Q>(&mut self, q: Q) where Q: Into<SimdQuaternion<T>> {
        *self = self.space_rotate(q);
    }

    pub fn space_rotate_inverse_assign<Q>(&mut self, inverse_q: Q) where Q: Into<SimdQuaternion<T>> {
        *self = self.space_rotate_inverse(inverse_q);
    }
}


// impl<T: FloatScalar> Matrix3<T> {
//     pub fn rotate_from_quaternion<Q>(q: Q) -> Matrix3<T> where Q: Into<Quaternion<T>> {
//         // Using: https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Conversion_to_and_from_the_matrix_representation
//         // But also see https://en.wikipedia.org/wiki/Quaternions_and_spatial_rotation#Quaternion-derived_rotation_matrix
//         // or https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
//
//         let q = q.into();
//         let two: T = T::ONE + T::ONE;
//         let s = two / q.norm_squared();
//
//         let a = q.r;
//         let [b, c, d] = q.xyz.into_array();
//         let [bs, cs, ds] = (q.xyz * s).into_array();
//         let ab = a * bs;
//         let ac = a * cs;
//         let ad = a * ds;
//         let bb = b * bs;
//         let bc = b * cs;
//         let bd = b * ds;
//         let cc = c * cs;
//         let cd = c * ds;
//         let dd = d * ds;
//
//         Matrix3::rows([
//             [T::ONE - cc - dd, bc - ad, bd + ac],
//             [bc + ad, T::ONE - bb - dd, cd - ab],
//             [bd - ac, cd + ab, T::ONE - bb - cc]
//         ])
//     }
//
//     pub fn rotate_to_quaternion(self) -> Quaternion<T> {
//         // Using: https://en.wikipedia.org/wiki/Rotation_matrix#Quaternion
//
//         let two: T = T::ONE + T::ONE;
//
//         let t = self.trace();
//         if t >= T::ZERO {
//             let r = (T::ONE + t).sqrt();
//             let two_r = two * r;
//             let s = two_r.recip();
//
//             let scalar = r / two;
//             let i = (self[2][1] - self[1][2]) * s;
//             let j = (self[0][2] - self[2][0]) * s;
//             let k = (self[1][0] - self[0][1]) * s;
//
//             Quaternion::new(scalar, vector![i, j, k])
//         } else {
//             let largest = if self[0][0] >= self[1][1] {
//                 if self[0][0] >= self[2][2] {
//                     0
//                 } else {
//                     2
//                 }
//             } else {
//                 if self[1][1] >= self[2][2] {
//                     1
//                 } else {
//                     2
//                 }
//             };
//
//             let n1 = largest;
//             let n2 = (largest + 1) % 3;
//             let n3 = (largest + 2) % 3;
//
//             let t = self[n1][n1] - self[n2][n2] - self[n3][n3];
//             let r = (T::ONE + t).sqrt();
//             let two_r = two * r;
//             let s = two_r.recip();
//
//             let scalar = (self[n3][n2] - self[n2][n3]) * s;
//             let mut vector = Vector3::zero();
//             vector[n1] = r / two;
//             vector[n2] = (self[n1][n2] + self[n2][n1]) * s;
//             vector[n3] = (self[n3][n1] + self[n1][n3]) * s;
//
//             Quaternion::new(scalar, vector)
//         }
//     }
// }
//

impl<T: FloatScalar + SimdScalar> SimdMatrix<T, 4, 4> {
    #[inline(always)]
    pub fn space_rotate_to_quaternion(self) -> crate::Quaternion<T> {
        self[0..3][0..3].matrix::<3, 3>().rotate_to_quaternion()
    }
}