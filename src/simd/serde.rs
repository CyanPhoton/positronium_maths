use std::fmt;
use std::marker::PhantomData;
use std::simd::{LaneCount, SupportedLaneCount};
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde::de::{DeserializeSeed, Error, SeqAccess, Visitor};
use serde::ser::SerializeTupleStruct;
use crate::{simd::SimdMatrix, SimdScalar, simd::SimdVector};

/// A DeserializeSeed helper for implementing deserialize_in_place Visitors.
///
/// Wraps a mutable reference and calls deserialize_in_place on it.
pub struct InPlaceSeed<'a, T: 'a>(pub &'a mut T);

impl<'a, 'de, T> DeserializeSeed<'de> for InPlaceSeed<'a, T>
    where
        T: Deserialize<'de>,
{
    type Value = ();
    fn deserialize<D>(self, deserializer: D) -> Result<Self::Value, D::Error>
        where
            D: Deserializer<'de>,
    {
        T::deserialize_in_place(deserializer, self.0)
    }
}

impl<T: SimdScalar + Serialize, const N: usize> Serialize for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let mut tuple = serializer.serialize_tuple_struct("SimdVector", N)?;
        for x in self.as_array() {
            tuple.serialize_field(x)?;
        }
        tuple.end()
    }
}

struct VectorVisitor<T: SimdScalar, const N: usize> where LaneCount<N>: SupportedLaneCount {
    marker: PhantomData<SimdVector<T, N>>,
}

struct VectorInPlaceVisitor<'a, T: SimdScalar + 'a, const N: usize>(&'a mut SimdVector<T, N>) where LaneCount<N>: SupportedLaneCount;

impl<T: SimdScalar, const N: usize> VectorVisitor<T, N> where LaneCount<N>: SupportedLaneCount {
    fn new() -> Self {
        VectorVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, T: SimdScalar, const N: usize> Visitor<'de> for VectorVisitor<T, N> where T: Deserialize<'de>, LaneCount<N>: SupportedLaneCount {
    type Value = SimdVector<T, N>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a simd vector of length {}", N))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de>, {
        SimdVector::try_from_fn(|i| seq.next_element()?.ok_or(Error::invalid_length(i, &self)))
    }
}

impl<'a, 'de, T: SimdScalar, const N: usize> Visitor<'de> for VectorInPlaceVisitor<'a, T, N> where T: Deserialize<'de>, LaneCount<N>: SupportedLaneCount{
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a simd vector of length {}", N))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de> {
        let mut fail_idx = None;
        for (idx, dest) in self.0.iter_mut().enumerate() {
            if seq.next_element_seed(InPlaceSeed(dest))?.is_none() {
                fail_idx = Some(idx);
                break;
            }
        }
        if let Some(idx) = fail_idx {
            return Err(Error::invalid_length(idx, &self));
        }
        Ok(())
    }
}

impl<'de, T: SimdScalar, const N: usize> Deserialize<'de> for SimdVector<T, N> where T: Deserialize<'de>, LaneCount<N>: SupportedLaneCount {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("SimdVector", N, VectorVisitor::<T, N>::new())
    }

    fn deserialize_in_place<D>(deserializer: D, place: &mut Self) -> Result<(), D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("SimdVector", N, VectorInPlaceVisitor(place))
    }
}

impl<T: SimdScalar + Serialize, const R: usize, const C: usize> Serialize for SimdMatrix<T, R, C> where LaneCount<R>: SupportedLaneCount {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error> where S: Serializer {
        let mut tuple = serializer.serialize_tuple_struct("SimdMatrix", C)?;
        for x in self.as_column_vectors() {
            tuple.serialize_field(x)?;
        }
        tuple.end()
    }
}

struct SimdMatrixVisitor<T: SimdScalar, const R: usize, const C: usize> where LaneCount<R>: SupportedLaneCount {
    marker: PhantomData<SimdMatrix<T, R, C>>,
}

struct SimdMatrixInPlaceVisitor<'a, T: SimdScalar + 'a, const R: usize, const C: usize>(&'a mut SimdMatrix<T, R, C>) where LaneCount<R>: SupportedLaneCount;

impl<T: SimdScalar, const R: usize, const C: usize> SimdMatrixVisitor<T, R, C> where LaneCount<R>: SupportedLaneCount {
    fn new() -> Self {
        SimdMatrixVisitor {
            marker: PhantomData,
        }
    }
}

impl<'de, T: SimdScalar, const R: usize, const C: usize> Visitor<'de> for SimdMatrixVisitor<T, R, C> where T: Deserialize<'de>, LaneCount<R>: SupportedLaneCount {
    type Value = SimdMatrix<T, R, C>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a matrix of size ({}, {})", R, C))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de>, {
        SimdMatrix::try_from_fn_column_vectors(|c| seq.next_element()?.ok_or(Error::invalid_length(c, &self)))
    }
}

impl<'a, 'de, T: SimdScalar, const R: usize, const C: usize> Visitor<'de> for SimdMatrixInPlaceVisitor<'a, T, R, C> where T: Deserialize<'de>, LaneCount<R>: SupportedLaneCount {
    type Value = ();

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str(&format!("a matrix of size ({}, {})", R, C))
    }

    #[inline]
    fn visit_seq<A>(self, mut seq: A) -> Result<Self::Value, A::Error> where A: SeqAccess<'de> {
        let mut fail_idx = None;
        for (idx, dest) in self.0.as_column_vectors_mut().iter_mut().enumerate() {
            if seq.next_element_seed(InPlaceSeed(dest))?.is_none() {
                fail_idx = Some(idx);
                break;
            }
        }
        if let Some(idx) = fail_idx {
            return Err(Error::invalid_length(idx, &self));
        }
        Ok(())
    }
}

impl<'de, T: SimdScalar, const R: usize, const C: usize> Deserialize<'de> for SimdMatrix<T, R, C> where T: Deserialize<'de>, LaneCount<R>: SupportedLaneCount {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("SimdMatrix", C, SimdMatrixVisitor::<T, R, C>::new())
    }

    fn deserialize_in_place<D>(deserializer: D, place: &mut Self) -> Result<(), D::Error> where D: Deserializer<'de> {
        deserializer.deserialize_tuple_struct("SimdMatrix", C, SimdMatrixInPlaceVisitor(place))
    }
}

mod tests {
    #[test]
    pub fn test() {
        dbg!();
        let m: crate::simd::SimdMatrix<_, 4, 3> = crate::simd_matrix![
            0.0, 1.0, 2.0,
            3.0, 4.0, 5.0,
            6.0, 7.0, 8.0,
            9.0, 10.0, 11.0
        ];
        dbg!(m);
        let s = serde_json::to_string(&m).unwrap();
        println!("{}", s);
        let m_2: crate::simd::SimdMatrix<f64, 4, 3> = serde_json::from_str(&s).unwrap();
        dbg!(m_2);
        assert_eq!(m, m_2);

        let q = crate::simd::SimdQuaternion::new(1.0, crate::Vector3::new(2.0, 3.0, 4.0));
        dbg!(q);
        let s = serde_json::to_string(&q).unwrap();
        println!("{}", s);
        let q_2: crate::simd::SimdQuaternion<f64> = serde_json::from_str(&s).unwrap();
        dbg!(q_2);
        assert_eq!(q, q_2);
    }
}