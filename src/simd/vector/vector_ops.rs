use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{OrdScalar, Scalar, SignedScalar, SimdOps, SimdScalar, simd::SimdVector, Vector};

impl<T: SimdScalar, const N: usize> SimdVector<T, N> where Simd<T, N>: Scalar, LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    pub fn comp_abs(self) -> Self where Simd<T, N>: SignedScalar {
        Self {
            values: self.values.abs(),
        }
    }

    pub fn all<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter().all(f)
    }

    pub fn zip_all<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Vector<V, N>, mut f: F) -> bool {
        self.iter().zip(other).all(|(l, r)| f(l, r))
    }

    pub fn any<F: FnMut(&T) -> bool>(&self, f: F) -> bool {
        self.iter().any(f)
    }

    pub fn zip_any<V: Scalar, F: FnMut(&T, &V) -> bool>(&self, other: &Vector<V, N>, mut f: F) -> bool {
        self.iter().zip(other).any(|(l, r)| f(l, r))
    }
}

impl<T: SimdScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: SimdOps<N, Element=T> {
    #[inline(always)]
    pub fn comp_sum(&self) -> T {
        self.values.reduce_sum()
    }
}

impl<T: SimdScalar + OrdScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: SimdOps<N, Element=T> + OrdScalar {
    #[inline(always)]
    pub fn comp_min(self, rhs: Self) -> Self {
        Self {
            values: self.values.min(rhs.values),
        }
    }

    #[inline(always)]
    pub fn comp_max(self, rhs: Self) -> Self {
        Self {
            values: self.values.max(rhs.values),
        }
    }

    #[inline(always)]
    pub fn comp_clamp(self, lower: Self, upper: Self) -> Self {
        Self {
            values: self.values.clamp(lower.values, upper.values),
        }
    }

    pub fn comp_gt(&self, other: &Self) -> bool {
        self.values.comp_gt(other.values)
    }

    pub fn comp_ge(&self, other: &Self) -> bool {
        self.values.comp_ge(other.values)
    }

    pub fn comp_lt(&self, other: &Self) -> bool {
        self.values.comp_lt(other.values)
    }

    pub fn comp_le(&self, other: &Self) -> bool {
        self.values.comp_le(other.values)
    }
}