use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{Arithmetic, FloatScalar, SimdScalar, simd::SimdUnitVector, simd::SimdVector, ZeroInit};

impl<T: SimdScalar + FloatScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    #[inline(always)]
    pub fn project_to_direction(self, direction: SimdUnitVector<T, N>) -> SimdVector<T, N> {
        let direction = direction.into_vector();
        direction * (self * direction)
    }

    #[inline(always)]
    pub fn project_to_plane(self, plane_normal: SimdUnitVector<T, N>) -> SimdVector<T, N> {
        self - self.project_to_direction(plane_normal)
    }

    #[inline(always)]
    pub fn reflect_on_plane(self, plane_normal: SimdUnitVector<T, N>) -> SimdVector<T, N> {
        let two = T::ONE + T::ONE;
        self - self.project_to_direction(plane_normal) * two
    }
}

impl<T: SimdScalar> SimdVector<T, 4> {
    #[inline(always)]
    pub const fn zero_point() -> SimdVector<T, 4> {
        SimdVector::<T, 4>::new(T::ZERO, T::ZERO, T::ZERO, T::ONE)
    }

    #[inline(always)]
    pub const fn zero_dir() -> SimdVector<T, 4> {
        SimdVector::ZERO
    }

    #[inline(always)]
    pub fn perspective_division(self) -> SimdVector<T, 4> where T: FloatScalar, Simd<T, 4>: Arithmetic {
        self / self.w
    }
}