pub use vector::*;
pub use unit_vector::*;

pub mod vector;
pub mod vector_ops;
pub mod vector_calculations;
pub mod geometry_calculations;
pub mod unit_vector;

#[macro_export]
macro_rules! simd_vector {
    ($elem:expr; $n:expr) => (
        $crate::simd::SimdVector::array([$elem; $n])
    );
    ($($x:expr),+ $(,)?) => (
        $crate::simd::SimdVector::array([$($x),+])
    );
}

#[macro_export]
macro_rules! simd_vector_swizzle {
    ($v:expr; $($i:ident),+) => {
        {
            struct Indices;

            #[allow(non_upper_case_globals)]
            #[allow(dead_code)]
            impl Indices {
                pub const x: usize = 0;
                pub const y: usize = 1;
                pub const z: usize = 2;
                pub const w: usize = 3;

                pub const r: usize = 0;
                pub const g: usize = 1;
                pub const b: usize = 2;
                pub const a: usize = 3;

                pub const s: usize = 0;
                pub const t: usize = 1;
                pub const u: usize = 2;
                pub const v: usize = 3;

                pub const i: usize = 0;
                pub const j: usize = 1;
                pub const k: usize = 2;
                pub const l: usize = 3;
            }

            $crate::simd::SimdVector::simd(::std::simd::simd_swizzle!($v.into_simd(), [$( Indices::$i ),*]))
        }
    };
}