use std::fmt::{Debug, Display};
use std::ops::{Add, AddAssign, BitAnd, BitAndAssign, BitOr, BitOrAssign, BitXor, Deref, DerefMut, Div, DivAssign, Mul, MulAssign, Neg, Sub, SubAssign};
use std::simd::{LaneCount, Simd, SupportedLaneCount};
use crate::{Arithmetic, array, CastAs, SignedArithmetic, SignedScalar, SimdScalar, Vector, ZeroInit, NotNan};

#[cfg(feature = "bincode")]
use bincode::{
    Decode,
    Encode,
    BorrowDecode,
    de::{BorrowDecoder, Decoder},
    enc::Encoder,
    error::{DecodeError, EncodeError}
};

use paste::paste;

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
// #[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct SimdVector<T: SimdScalar, const N: usize> where LaneCount<N>: SupportedLaneCount {
    pub(crate) values: Simd<T, N>,
}

#[cfg(feature = "bincode")]
impl<T: SimdScalar + Encode, const N: usize> Encode for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        Encode::encode(self.as_array(), encoder)
    }
}

#[cfg(feature = "bincode")]
impl<T: SimdScalar + Decode, const N: usize> Decode for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        Ok(Self::array(Decode::decode(decoder)?))
    }
}

#[cfg(feature = "bincode")]
impl<'de, T: SimdScalar + BorrowDecode<'de>, const N: usize> BorrowDecode<'de> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn borrow_decode<D: BorrowDecoder<'de>>(decoder: &mut D) -> Result<Self, DecodeError> {
        Ok(Self::array(BorrowDecode::borrow_decode(decoder)?))
    }
}

impl<T: SimdScalar, const N: usize> Default for SimdVector<T, N> where T: Default, LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn default() -> Self {
        SimdVector::array([T::default(); N])
    }
}

/// Safety: Is repr(c) and single field is Zeroable
unsafe impl<T: SimdScalar, const N: usize> bytemuck::Zeroable for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {}

/// Safety: Is repr(c) and single field is Pod
unsafe impl<T: SimdScalar, const N: usize> bytemuck::Pod for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {}

impl<T: SimdScalar, const N: usize> ZeroInit for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    const ZERO: Self = SimdVector::splat(T::ZERO);
}

impl<T: SimdScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    pub const fn zero() -> Self {
        Self::ZERO
    }

    #[inline(always)]
    pub const fn splat(value: T) -> Self {
        SimdVector::array([value; N])
    }

    #[inline(always)]
    pub const fn array(values: [T; N]) -> Self {
        SimdVector { values: Simd::from_array(values) }
    }

    #[inline(always)]
    pub const fn simd(simd: Simd<T, N>) -> Self {
        SimdVector {
            values: simd,
        }
    }

    pub fn slice(values: &[T]) -> Self {
        SimdVector::from_fn(|i| values[i])
    }

    #[track_caller]
    #[inline(always)]
    pub const fn unit_n(n: usize) -> Self {
        assert!(n < N, "Invalid index specified");

        let mut array = [T::ZERO; N];
        array[n] = T::ONE;
        SimdVector::array(array)
    }

    #[inline(always)]
    pub fn from_fn<F>(f: F) -> Self where F: FnMut(usize) -> T {
        SimdVector::array(array::from_fn(f))
    }

    #[inline(always)]
    pub fn try_from_fn<F, E>(f: F) -> Result<Self, E> where F: FnMut(usize) -> Result<T, E> {
        Ok(SimdVector::array(array::try_from_fn(f)?))
    }

    #[inline(always)]
    pub fn chain<const M: usize>(self, other: SimdVector<T, M>) -> SimdVector<T, { N + M }> where LaneCount<M>: SupportedLaneCount, LaneCount<{ N + M }>: SupportedLaneCount {
        SimdVector::array(array::join(self.values.to_array(), other.values.to_array()))
    }

    #[inline(always)]
    pub fn split<const M: usize>(self) -> (SimdVector<T, M>, SimdVector<T, { N - M }>) where LaneCount<M>: SupportedLaneCount, LaneCount<{ N - M }>: SupportedLaneCount {
        let (lhs, rhs) = array::split::<T, N, M>(self.values.to_array());
        (SimdVector::array(lhs), SimdVector::array(rhs))
    }

    #[must_use]
    #[inline(always)]
    pub fn swap(mut self, a: usize, b: usize) -> Self {
        self.values.as_mut_array().swap(a, b);
        self
    }

    pub fn swap_assign(&mut self, a: usize, b: usize) {
        self.values.as_mut_array().swap(a, b);
    }

    /// Can also use & operator
    #[inline(always)]
    pub fn comp_mul(self, rhs: Self) -> Self where Simd<T, N>: Arithmetic {
        self & rhs
    }

    /// Can also use | operator
    #[inline(always)]
    pub fn comp_div(self, rhs: Self) -> Self where Simd<T, N>: Arithmetic {
        self | rhs
    }

    #[inline(always)]
    pub fn map<V: SimdScalar, F: FnMut(T) -> V>(self, mut map: F) -> SimdVector<V, N> {
        SimdVector::from_fn(|i| map(self[i]))
    }

    #[inline(always)]
    pub fn try_map<V: SimdScalar, E, F: FnMut(T) -> Result<V, E>>(self, mut map: F) -> Result<SimdVector<V, N>, E> {
        SimdVector::try_from_fn(|i| map(self[i]))
    }

    #[inline(always)]
    pub fn zip_map<U: SimdScalar, V: SimdScalar, F: FnMut(T, V) -> U>(self, other: SimdVector<V, N>, mut map: F) -> SimdVector<U, N> {
        SimdVector::from_fn(|i| map(self[i], other[i]))
    }

    #[inline(always)]
    pub fn cast_as<V: SimdScalar>(self) -> SimdVector<V, N> where T: CastAs<V> {
        self.map(|v| v.cast_as())
    }

    #[inline(always)]
    pub fn comp_into<V: SimdScalar>(self) -> SimdVector<V, N> where T: Into<V> {
        self.map(|v| v.into())
    }

    #[inline(always)]
    pub fn comp_try_into<V: SimdScalar, E>(self) -> Result<SimdVector<V, N>, E> where T: TryInto<V, Error = E> {
        self.try_map(|v| v.try_into())
    }

    pub const fn as_slice(&self) -> &[T] {
        self.values.as_array()
    }

    pub fn as_slice_mut(&mut self) -> &mut [T] {
        &mut self.values.as_mut_array()[..]
    }

    pub const fn as_array(&self) -> &[T; N] {
        self.values.as_array()
    }

    pub fn as_array_mut(&mut self) -> &mut [T; N] {
        self.values.as_mut_array()
    }

    #[inline(always)]
    pub const fn into_array(self) -> [T; N] {
        self.values.to_array()
    }

    #[inline(always)]
    pub const fn into_simd(self) -> Simd<T, N> {
        self.values
    }

    #[inline(always)]
    pub fn into_iter(self) -> impl Iterator<Item=T> {
        self.values.to_array().into_iter()
    }

    pub fn iter(&self) -> impl Iterator<Item=&T> {
        self.values.as_array().iter()
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item=&mut T> {
        self.values.as_mut_array().iter_mut()
    }
}

impl<T: SimdScalar, const N: usize> From<Vector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(vec: Vector<T, N>) -> Self {
        SimdVector::array(vec.into_array())
    }
}

impl<T: SimdScalar, const N: usize> From<SimdVector<T, N>> for Vector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(vec: SimdVector<T, N>) -> Self {
        Vector::array(vec.into_array())
    }
}

impl<T: SimdScalar, const N: usize> From<[T; N]> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(values: [T; N]) -> Self {
        SimdVector::array(values)
    }
}

impl<T: SimdScalar, const N: usize> From<SimdVector<T, N>> for [T; N] where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(values: SimdVector<T, N>) -> Self {
        values.into_array()
    }
}

macro_rules! from_tuple {
    ( $n0:literal, $t0:ident, $($n:literal, $t:ident, )* ) => {
        paste! {
            #[allow(non_snake_case)]
            impl<T: SimdScalar> From<( $t0, $($t,)* )> for SimdVector<$t0, $n0> {
                #[inline(always)]
                fn from(v: ( $t0, $($t,)* )) -> Self {
                    let ( [< $t0 $n0 >], $( [< $t $n >]),* ) = v;
                    SimdVector::array([ [< $t0 $n0 >] $(, [< $t $n >])* ])
                }
            }

            #[allow(non_snake_case)]
            impl<T: SimdScalar> From<SimdVector<$t0, $n0>> for ( $t0, $($t,)* ) {
                #[inline(always)]
                fn from(v: SimdVector<$t0, $n0>) -> Self {
                    // (  v[0], $( v[$n0 - $n]),*  )
                    let [ [< $t0 $n0 >] $(, [< $t $n >])* ] = v.into_array();
                    ( [< $t0 $n0 >], $( [< $t $n >]),* )
                }
            }

            #[allow(non_snake_case)]
            impl<T: SimdScalar> SimdVector<$t0, $n0> {
                #[inline(always)]
                pub fn tuple(v: ( $t0, $($t,)* )) -> Self {
                    v.into()
                }
            }
        }
    };
    () => {}
}

from_tuple!(16, T, 15, T, 14, T, 13, T, 12, T, 11, T, 10, T, 9, T, 8, T, 7, T, 6, T, 5, T, 4, T, 3, T, 2, T, 1, T, );
from_tuple!(8, T, 7, T, 6, T, 5, T, 4, T, 3, T, 2, T, 1, T, );
from_tuple!(4, T, 3, T, 2, T, 1, T, );
from_tuple!(2, T, 1, T, );
from_tuple!(1, T, );

impl<T: SimdScalar, const N: usize> IntoIterator for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.into_iter()
    }
}

impl<'a, T: SimdScalar, const N: usize> IntoIterator for &'a SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = &'a T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, T: SimdScalar, const N: usize> IntoIterator for &'a mut SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = &'a mut T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

impl<T: SimdScalar, const N: usize> FromIterator<T> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from_iter<I: IntoIterator<Item=T>>(iter: I) -> Self {
        SimdVector::array(array::from_iter(iter).unwrap())
    }
}

// Cross product

impl<T: SimdScalar> BitXor for SimdVector<T, 4> where Simd<T, 4>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn bitxor(self, rhs: Self) -> Self {
        self.cross_space(rhs)
    }
}

// Addition

impl<T: SimdScalar, const N: usize> Add for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn add(self, rhs: Self) -> Self {
        SimdVector {
            values: self.values + rhs.values,
        }
    }
}

impl<T: SimdScalar, const N: usize> AddAssign for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn add_assign(&mut self, rhs: Self) {
        self.values += rhs.values;
    }
}

// Subtraction

impl<T: SimdScalar, const N: usize> Sub for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn sub(self, rhs: Self) -> Self {
        SimdVector {
            values: self.values - rhs.values,
        }
    }
}

impl<T: SimdScalar, const N: usize> SubAssign for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn sub_assign(&mut self, rhs: Self) {
        self.values -= rhs.values;
    }
}

// Inner product

impl<T: SimdScalar, const N: usize> Mul for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = T;

    #[inline(always)]
    fn mul(self, rhs: Self) -> T {
        self.inner_product(rhs)
    }
}

// Multiplication

impl<T: SimdScalar, const N: usize> Mul<T> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn mul(self, rhs: T) -> Self {
        self & SimdVector::splat(rhs)
    }
}

impl<T: SimdScalar, const N: usize> MulAssign<T> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn mul_assign(&mut self, rhs: T) {
        self.values *= SimdVector::splat(rhs).values;
    }
}

macro_rules! vector_pre_scale {
    ($($t:ty),+) => {
        $(impl<const N: usize> Mul<SimdVector<$t, N>> for $t where LaneCount<N>: SupportedLaneCount, Simd<$t, N>: Arithmetic {
            type Output = SimdVector<$t, N>;

            #[inline(always)]
            fn mul(self, rhs: SimdVector<$t, N>) -> SimdVector<$t, N> {
                SimdVector::splat(self) & rhs
            }
        })
        *
    };
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

impl<T: SimdScalar, const N: usize> BitAnd<SimdVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn bitand(self, rhs: SimdVector<T, N>) -> Self::Output {
        SimdVector {
            values: self.values * rhs.values,
        }
    }
}

impl<T: SimdScalar, const N: usize> BitAndAssign<SimdVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn bitand_assign(&mut self, rhs: SimdVector<T, N>) {
        self.values *= rhs.values;
    }
}


macro_rules! vector_pre_recip {
    ($($t:ty),+) => {
        $(impl<const N: usize> BitOr<SimdVector<$t, N>> for $t where LaneCount<N>: SupportedLaneCount, Simd<$t, N>: Arithmetic {
            type Output = SimdVector<$t, N>;

            fn bitor(self, rhs: SimdVector<$t, N>) -> SimdVector<$t, N> {
                SimdVector::splat(self) | rhs
            }
        })
        *
    };
}

vector_pre_recip!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

// Division

impl<T: SimdScalar, const N: usize> Div<T> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = Self;

    #[inline(always)]
    fn div(self, rhs: T) -> Self {
        self | SimdVector::splat(rhs)
    }
}

impl<T: SimdScalar, const N: usize> DivAssign<T> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn div_assign(&mut self, rhs: T) {
        self.values /= SimdVector::splat(rhs).values;
    }
}

impl<T: SimdScalar, const N: usize> BitOr<SimdVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn bitor(self, rhs: SimdVector<T, N>) -> Self::Output {
        SimdVector {
            values: self.values / rhs.values,
        }
    }
}

impl<T: SimdScalar, const N: usize> BitOrAssign<SimdVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    fn bitor_assign(&mut self, rhs: SimdVector<T, N>) {
        self.values /= rhs.values;
    }
}

// Negation

impl<T: SimdScalar, const N: usize> Neg for SimdVector<T, N> where T: SignedScalar, LaneCount<N>: SupportedLaneCount, Simd<T, N>: SignedArithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        SimdVector {
            values: -self.values,
        }
    }
}

// Deref to Vector

impl<T: SimdScalar, const N: usize> Deref for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Target = Vector<T, N>;

    fn deref(&self) -> &Self::Target {
        // Safety, SimdVector has the same shape as Vector, just with potentially higher alignment,
        // so transmuting a ref this direction is always sound.
        unsafe { std::mem::transmute(self) }
    }
}

impl<T: SimdScalar, const N: usize> DerefMut for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // Safety, SimdVector has the same shape as Vector, just with potentially higher alignment,
        // so transmuting a ref this direction is always sound.
        unsafe { std::mem::transmute(self) }
    }
}

// Swizzling
// TODO
// impl<T: SimdScalar, const N: usize> SimdVector<T, N> {
//     pub fn swizzle<const M: usize>(self, indices: [usize; M]) -> SimdVector<T, M> where T: Copy {
//         self >> indices
//     }
// }
//
// impl<T: SimdScalar, const N: usize, const M: usize> std::ops::Shr<[usize; M]> for SimdVector<T, N> where T: Copy {
//     type Output = SimdVector<T, M>;
//
//     fn shr(self, rhs: [usize; M]) -> SimdVector<T, M> {
//         SimdVector::from_fn(|i| self[rhs[i]])
//     }
// }

macro_rules! vector_specialise {
    ($nick_base:ident, $size:literal, $( ($coord:ident, $coord_index:literal)),+ ) => {
        paste! {
            pub type [<$nick_base $size>]<T> = SimdVector<T, $size>;

            pub mod [<vec $size>] {
                use super::*;

                impl<T: SimdScalar> SimdVector<T, $size> where LaneCount<$size>: SupportedLaneCount {
                    #[inline(always)]
                    pub const fn new($($coord: T),+) -> Self {
                        SimdVector::array([$($coord),+])
                    }

                    $(
                    #[inline(always)]
                    pub const fn [< unit_ $coord >]() -> Self {
                        Self::unit_n($coord_index)
                    }
                    )
                    +
                }
            }
        }
    };
}

vector_specialise!(SimdVector, 1, (x, 0));
vector_specialise!(SimdVector, 2, (x, 0), (y, 1));
vector_specialise!(SimdVector, 4, (x, 0), (y, 1), (z, 2), (w, 3));

macro_rules! vector_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

vector_nicks!(SimdVector, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(SimdVector, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(SimdVector, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);

impl<T: SimdScalar, const N: usize> Debug for SimdVector<T, N> where T: Debug, LaneCount<N>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.deref())
    }
}

impl<T: SimdScalar, const N: usize> Display for SimdVector<T, N> where T: Display, LaneCount<N>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.deref())
    }
}
