use std::simd::{LaneCount, Simd, SupportedLaneCount};

use crate::{Arithmetic, FloatScalar, simd_vector_swizzle, SimdScalar, simd::SimdUnitVector, simd::SimdVector};

impl<T: SimdScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    #[inline(always)]
    pub fn inner_product(self, rhs: Self) -> T {
        (self & rhs).comp_sum()
    }

    #[inline(always)]
    pub fn norm_squared(self) -> T {
        self.inner_product(self)
    }
}

impl<T: SimdScalar + FloatScalar, const N: usize> SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    #[inline(always)]
    pub fn norm(self) -> T {
        self.norm_squared().sqrt()
    }

    #[inline(always)]
    pub fn unit(self) -> SimdUnitVector<T, N> {
        SimdUnitVector(self / self.norm())
    }

    #[inline(always)]
    pub fn try_unit(self) -> Option<SimdUnitVector<T, N>> {
        if self != Self::zero() {
            Some(self.unit())
        } else {
            None
        }
    }

    #[inline(always)]
    pub fn unit_or_zero(self) -> Self {
        if self == Self::zero() {
            self
        } else {
            self / self.norm()
        }
    }

    #[inline(always)]
    pub fn unit_assign(&mut self) {
        *self = *self / self.norm()
    }

    #[inline(always)]
    pub fn unit_assign_or_zero(&mut self) {
        if *self != Self::zero() {
            *self = *self / self.norm()
        }
    }

    #[inline(always)]
    pub fn unit_with_norm(self) -> (SimdUnitVector<T, N>, T) {
        let norm = self.norm();
        (SimdUnitVector(self / norm), norm)
    }

    #[inline(always)]
    pub fn try_unit_with_norm(self) -> Option<(SimdUnitVector<T, N>, T)> {
        if self != Self::zero() {
            Some(self.unit_with_norm())
        } else {
            None
        }
    }

    #[inline(always)]
    pub fn unit_or_zero_with_norm(self) -> (Self, T) {
        if self == Self::zero() {
            (self, T::ZERO)
        } else {
            let norm = self.norm();
            (self / norm, norm)
        }
    }

    pub fn unit_assign_with_norm(&mut self) -> T {
        let norm = self.norm();
        *self = *self / norm;
        norm
    }

    pub fn unit_assign_or_zero_with_norm(&mut self) -> T {
        if *self != Self::zero() {
            self.unit_assign_with_norm()
        } else {
            T::ZERO
        }
    }
}

impl<T: SimdScalar> SimdVector<T, 4> where Simd<T, 4>: Arithmetic {
    /// Performs a cross product of the xyz components of two Vector4.
    /// The resulting w component will always be 0
    #[inline(always)]
    pub fn cross_space(self, rhs: Self) -> Self {
        // In long:
        // self.x -> self.y
        // self.y -> self.z
        // self.z -> self.x
        let a = simd_vector_swizzle!(self; y, z, x, w);
        // rhs.x -> rhs.z
        // rhs.y -> rhs.x
        // rhs.z -> rhs.y
        let b = simd_vector_swizzle!(rhs; z, x, y, w);

        // let c = simd_swizzle!(self.values; z, x, y, w);
        // let d = simd_swizzle!(rhs.values; y, z, x, w);

        // a.hadamard_product(b) - a.hadamard_product(rhs)

        // self.y * rhs.z
        // self.z * rhs.x
        // self.x * rhs.y
        let c = a & b;

        // self.y * rhs.x
        // self.z * rhs.y
        // self.x * rhs.z
        let d = a & rhs;

        // self.y * rhs.x (x) -> (y) self.z * rhs.y
        // self.z * rhs.y (y) -> (z) self.x * rhs.z
        // self.x * rhs.z (z) -> (x) self.y * rhs.x
        let e = simd_vector_swizzle!(d; y, z, x, w);

        // self.y * rhs.z - self.z * rhs.y,
        // self.z * rhs.x - self.x * rhs.z,
        // self.x * rhs.y - self.y * rhs.x
        c - e
    }
}

impl<T: SimdScalar> SimdVector<T, 2> {
    #[inline(always)]
    pub fn cross_in_z(self, rhs: SimdVector<T, 2>) -> T {
        self.x * rhs.y - self.y * rhs.x
    }
}