use std::fmt::{Debug, Display};
use std::hash::Hash;
use std::ops::{Add, BitXor, Deref, Div, Index, Mul, Neg, Sub};
use std::simd::{LaneCount, Simd, SupportedLaneCount};

use paste::paste;

#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};

#[cfg(feature = "bincode")]
use bincode::{
    BorrowDecode,
    Decode,
    Encode,
    de::{BorrowDecoder, Decoder},
    enc::Encoder,
    error::{DecodeError, EncodeError}
};

use crate::{SignedScalar, simd::SimdVector, ZeroInit, SimdScalar, Arithmetic, SignedArithmetic, UnitVector};

#[repr(C)]
#[derive(Copy, Clone, Ord, PartialOrd, Eq, PartialEq, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
// #[cfg_attr(feature = "bincode", derive(Encode, Decode))]
pub struct SimdUnitVector<T: SimdScalar, const N: usize>(pub(crate) SimdVector<T, N>) where LaneCount<N>: SupportedLaneCount;

#[cfg(feature = "bincode")]
impl<T: SimdScalar + Encode, const N: usize> Encode for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn encode<E: Encoder>(&self, encoder: &mut E) -> Result<(), EncodeError> {
        Encode::encode(&self.0, encoder)
    }
}

#[cfg(feature = "bincode")]
impl<T: SimdScalar + Decode, const N: usize> Decode for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn decode<D: Decoder>(decoder: &mut D) -> Result<Self, DecodeError> {
        Ok(Self(Decode::decode(decoder)?))
    }
}

#[cfg(feature = "bincode")]
impl<'de, T: SimdScalar + BorrowDecode<'de>, const N: usize> BorrowDecode<'de> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    fn borrow_decode<D: BorrowDecoder<'de>>(decoder: &mut D) -> Result<Self, DecodeError> {
        Ok(Self(BorrowDecode::borrow_decode(decoder)?))
    }
}

impl<T: SimdScalar, const N: usize> SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[track_caller]
    #[inline(always)]
    pub fn unit_n(n: usize) -> Self {
        assert!(n < N, "Invalid index specified");

        let mut v = SimdVector::<T, N>::ZERO;
        v[n] = T::ONE;
        SimdUnitVector(v)
    }

    #[inline(always)]
    pub fn into_vector(self) -> SimdVector<T, N> {
        self.0
    }

    #[inline(always)]
    pub fn into_array(self) -> [T; N] {
        self.0.into_array()
    }

    #[inline(always)]
    pub fn into_iter(self) -> impl Iterator<Item = T> {
        self.0.into_iter()
    }
}

impl<T: SimdScalar, const N: usize> From<UnitVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(vec: UnitVector<T, N>) -> Self {
        SimdUnitVector(vec.0.into())
    }
}

impl<T: SimdScalar, const N: usize> From<SimdUnitVector<T, N>> for UnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(vec: SimdUnitVector<T, N>) -> Self {
        UnitVector(vec.0.into())
    }
}

impl<T: SimdScalar, const N: usize> From<SimdUnitVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount {
    #[inline(always)]
    fn from(vector: SimdUnitVector<T, N>) -> Self {
        vector.0
    }
}

impl<T: SimdScalar, const N: usize> IntoIterator for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.0.into_iter()
    }
}

impl<'a, T: SimdScalar, const N: usize> IntoIterator for &'a SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = &'a T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.0.iter()
    }
}

impl<'a, T: SimdScalar, const N: usize> IntoIterator for &'a mut SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Item = &'a mut T;
    type IntoIter = impl Iterator<Item=Self::Item>;

    #[inline(always)]
    fn into_iter(self) -> Self::IntoIter {
        self.0.iter_mut()
    }
}

// Cross product

impl<T: SimdScalar + SignedScalar> BitXor<SimdUnitVector<T, 4>> for SimdUnitVector<T, 4> where LaneCount<4>: SupportedLaneCount, Simd<T, 4>: Arithmetic {
    type Output = SimdVector<T, 4>;

    #[inline(always)]
    fn bitxor(self, rhs: SimdUnitVector<T, 4>) -> SimdVector<T, 4> {
        self.0.cross_space(rhs.0)
    }
}

impl<T: SimdScalar + SignedScalar> BitXor<SimdVector<T, 4>> for SimdUnitVector<T, 4> where LaneCount<4>: SupportedLaneCount, Simd<T, 4>: Arithmetic {
    type Output = SimdVector<T, 4>;

    #[inline(always)]
    fn bitxor(self, rhs: SimdVector<T, 4>) -> SimdVector<T, 4> {
        self.0.cross_space(rhs)
    }
}

// Addition

impl<T: SimdScalar, const N: usize> Add<SimdUnitVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn add(self, rhs: SimdUnitVector<T, N>) -> SimdVector<T, N> {
        self.0 + rhs.0
    }
}

impl<T: SimdScalar, const N: usize> Add<SimdVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn add(self, rhs: SimdVector<T, N>) -> SimdVector<T, N> {
        self.0 + rhs
    }
}

// Subtraction

impl<T: SimdScalar, const N: usize> Sub<SimdUnitVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn sub(self, rhs: SimdUnitVector<T, N>) -> SimdVector<T, N> {
        self.0 - rhs.0
    }
}

impl<T: SimdScalar, const N: usize> Sub<SimdVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn sub(self, rhs: SimdVector<T, N>) -> SimdVector<T, N> {
        self.0 - rhs
    }
}

// Inner product

impl<T: SimdScalar, const N: usize> Mul<SimdUnitVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = T;

    #[inline(always)]
    fn mul(self, rhs: SimdUnitVector<T, N>) -> T {
        self.0 * rhs.0
    }
}

impl<T: SimdScalar, const N: usize> Mul<SimdVector<T, N>> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = T;

    #[inline(always)]
    fn mul(self, rhs: SimdVector<T, N>) -> T {
        self.0 * rhs
    }
}

impl<T: SimdScalar, const N: usize> Mul<SimdUnitVector<T, N>> for SimdVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = T;

    #[inline(always)]
    fn mul(self, rhs: SimdUnitVector<T, N>) -> T {
        self * rhs.0
    }
}

// Multiplication

impl<T: SimdScalar, const N: usize> Mul<T> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn mul(self, rhs: T) -> SimdVector<T, N> {
        self.0 * rhs
    }
}

macro_rules! vector_pre_scale {
    ($($t:ty),+) => {
        $(impl<const N: usize> Mul<SimdUnitVector<$t, N>> for $t where LaneCount<N>: SupportedLaneCount {
            type Output = SimdVector<$t, N>;

            #[inline(always)]
            fn mul(self, rhs: SimdUnitVector<$t, N>) -> SimdVector<$t, N> {
                self * rhs.0
            }
        })
        *
    };
}

vector_pre_scale!(i8, u8, i16, u16, i32, u32, i64, u64, f32, f64);

// Division

impl<T: SimdScalar, const N: usize> Div<T> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: Arithmetic {
    type Output = SimdVector<T, N>;

    #[inline(always)]
    fn div(self, rhs: T) -> SimdVector<T, N> {
        self.0 / rhs
    }
}

// Negation

impl<T: SimdScalar + SignedScalar, const N: usize> Neg for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount, Simd<T, N>: SignedArithmetic {
    type Output = SimdUnitVector<T, N>;

    #[inline(always)]
    fn neg(self) -> Self::Output {
        Self(-self.0)
    }
}

// Indexing

impl<T: SimdScalar, const N: usize> Index<usize> for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Output = T;

    fn index(&self, index: usize) -> &T {
        &self.0[index]
    }
}

// Deref

impl<T: SimdScalar, const N: usize> Deref for SimdUnitVector<T, N> where LaneCount<N>: SupportedLaneCount {
    type Target = SimdVector<T, N>;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

macro_rules! vector_specialise {
    ($nick_base:ident, $size_ident:literal, $size:literal, $( ($coord:ident, $colour:ident, $uv:ident, $comp:ident)),+ ) => {
        paste! {
            pub type [<$nick_base $size_ident>]<T> = SimdUnitVector<T, $size>;

            pub mod [<unit_vec $size_ident>] {
                use super::*;

                impl<T: SimdScalar> SimdUnitVector<T, $size> {
                    $(
                    #[inline(always)]
                    pub fn [< unit_ $coord >]() -> Self {
                        let mut v = SimdVector::<T, $size>::ZERO;
                        v.$coord = T::ONE;
                        SimdUnitVector(v)
                    }
                    )
                    +
                }
            }
        }
    };
}

vector_specialise!(SimdUnitVector, 1, 1usize, (x, r, s, i));
vector_specialise!(SimdUnitVector, 2, 2usize, (x, r, s, i), (y, g, t, k));
vector_specialise!(SimdUnitVector, 4, 4usize, (x, r, s, i), (y, g, t, j), (z, b, u, k), (w, a, v, l));

macro_rules! vector_nicks {
    ($nick_base:ident, $size:literal, $($suffix:ident, $var:ty),* ; $($exp_var:ty),*) => {
        paste! {
            $(pub type [<$nick_base $size $suffix>] = [<$nick_base $size>]<$var>;)
            *
            $(pub type [<$nick_base $size $exp_var>] = [<$nick_base $size>]<$exp_var>;)
            *
        }
    };
}

vector_nicks!(SimdUnitVector, 1, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(SimdUnitVector, 2, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);
vector_nicks!(SimdUnitVector, 4, f, f32, d, f64, u, u32, i, i32, b, bool ; u8, i8, u16, i16, u32, i32, u64, i64, u128, i128, usize, isize, f32, f64);

impl<T: SimdScalar, const N: usize> Debug for SimdUnitVector<T, N> where T: Debug, LaneCount<N>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}

impl<T: SimdScalar, const N: usize> Display for SimdUnitVector<T, N> where T: Display, LaneCount<N>: SupportedLaneCount {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}