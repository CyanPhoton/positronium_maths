#![feature(impl_trait_in_assoc_type)]
#![feature(generic_const_exprs)]
#![cfg_attr(feature = "simd", feature(portable_simd))]

extern crate core;

pub use struct_field_align;
pub use struct_field_align::*;

pub use scalar::*;
pub use not_nan::*;
pub use complex::*;
pub use angles::*;
pub use matrix::*;
pub use quaternion::*;
pub use vector::*;
pub use utility::*;

pub mod scalar;
pub mod not_nan;
pub mod bounds;
pub mod vector;
pub mod angles;
pub mod angle_ops;
pub mod matrix;
pub mod complex;
pub mod quaternion;
pub mod ray;
pub mod utility;

#[cfg(feature = "simd")]
pub mod simd;

#[cfg(feature = "serde")]
pub mod serde;

#[cfg(all(feature = "bincode", test))]
mod bincode_tests {
    #[test]
    fn test() {
        let c = bincode::config::standard();

        let m: crate::Matrix<_, 3, 4> = crate::matrix![
            0.0, 1.0, 2.0, 3.0,
            4.0, 5.0, 6.0, 7.0,
            8.0, 9.0, 10.0, 11.0
        ];
        dbg!(m);
        let s = bincode::encode_to_vec(&m, c).unwrap();
        println!("{:?}", s);
        let m_2: crate::Matrix<f64, 3, 4> = bincode::decode_from_slice(&s[..], c).unwrap().0;
        dbg!(m_2);
        assert_eq!(m, m_2);

        let q = crate::Quaternion::new(1.0, crate::Vector3::new(2.0, 3.0, 4.0));
        dbg!(q);
        let s = bincode::encode_to_vec(&q, c).unwrap();
        println!("{:?}", s);
        let q_2: crate::Quaternion<f64> = bincode::decode_from_slice(&s[..], c).unwrap().0;
        dbg!(q_2);
        assert_eq!(q, q_2);
    }
}